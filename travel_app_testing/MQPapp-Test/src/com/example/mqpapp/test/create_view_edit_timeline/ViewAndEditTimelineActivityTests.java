package com.example.mqpapp.test.create_view_edit_timeline;

import android.app.ActionBar;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.example.mqpapp.HelpScreenActivity;
import com.example.mqpapp.R;
import com.example.mqpapp.create_view_edit_entry.CreateNewEntryActivity;
import com.example.mqpapp.create_view_edit_entry.ViewAndEditEntryActivity;
import com.example.mqpapp.create_view_edit_timeline.ViewAllTimelinesActivity;
import com.example.mqpapp.create_view_edit_timeline.ViewAndEditTimelineActivity;
import com.robotium.solo.Solo;

public class ViewAndEditTimelineActivityTests extends
		ActivityInstrumentationTestCase2<ViewAllTimelinesActivity> {

	public ViewAndEditTimelineActivityTests() {
		// Must set this up as "super(CLASSNAME.class);"
		super(ViewAllTimelinesActivity.class);
		// TODO Auto-generated constructor stub
	}

	private Solo solo; // Need to create this 'solo' object for testing

	protected void setUp() throws Exception {
		super.setUp();
		// This is the testing object:
		solo = new Solo(getInstrumentation(), getActivity());

		// Starting in the first activity
		solo.assertCurrentActivity("Check that first activity runs",
				ViewAllTimelinesActivity.class);

		// Getting the listview that has all of the timelines
		ListView myListView = (ListView) solo.getView(R.id.allTimelineListView,
				0);

		// If there are timelines
		if ((myListView.getChildCount() > 0)) {

			// Getting that first child (a timeline):
			View timelineView = myListView.getChildAt(0);

			// Clicking the first timeline
			solo.clickOnView(timelineView);

			// Give time to change activity
			solo.sleep(1000);
		}

	}
	
	protected void tearDown() throws Exception {
		// check the current activity
		solo.assertCurrentActivity(
				"Check to make sure the test ended in the ViewAndEditEntryActivity",
				ViewAndEditTimelineActivity.class);

		// attempt to go back to the main activity that views all timelines
		solo.goBack();

		// verify that the current activity is the correct activity
		solo.assertCurrentActivity(
				"Check to make sure the current activity is the initial activity",
				ViewAllTimelinesActivity.class);
		super.tearDown();

	}

	/** Testing that the pieces of our first activity still exist. **/
	public void testActionBar() {
		// *** All tests need from here... ***

		// Checking that we've switched activities.
		solo.assertCurrentActivity("Check that first activity runs",
				ViewAndEditTimelineActivity.class);

		// Getting the action bar object
		ActionBar actionBar = getActivity().getActionBar();

		// Asserting that we do have an action bar
		assertNotNull(actionBar);

		solo.clickOnActionBarItem(R.id.timeline_add_entry);
		solo.sleep(1000); // Give time to change activity
		// Showing that we're now at the Create New Entry Activity
		solo.assertCurrentActivity("Current Activity",
				CreateNewEntryActivity.class);

		// return back to the activity this test started at
		solo.clickOnActionBarItem(R.id.cancel_edit_entry);
		solo.clickOnButton(1);
		
		solo.clickOnActionBarItem(R.id.edit_timeline);
		solo.sleep(1000); // Give time to change activity
		// Now try to edit a timeline:
		solo.clickOnActionBarItem(R.id.cancel_edit_timeline);
		// And discard changes:
		solo.clickOnButton(1);

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this is still the correct activity to be in",
				ViewAndEditTimelineActivity.class);

	}

	/** Testing that you can set the start date with the datepicker. **/
	public void testSettingStartDate() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);

		// click on the edit button to go into edit mode
		solo.clickOnActionBarItem(R.id.edit_timeline);

		View view = solo.getView("buttonShowStartDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 1900, 6, 16); // Choosing a date to set

		// Note: Even though we set the month as 6, this is setting
		// it as 7 (July) because Android uses 0 as January, 1 as February,
		// etc, and so when we search for this date on the screen, search for
		// 6+1, which is 7.
		solo.clickOnButton(1); // Clicking set button

		// Look for the date that was set
		assertTrue(solo.searchText("7-16-1900"));

		// return back to the activity this test started at
		solo.clickOnActionBarItem(R.id.cancel_edit_timeline);
		solo.clickOnButton(1);

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);
	}

	/** Testing that you can set the start date with the datepicker. **/
	public void testSettingEndDate() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);

		// click on the edit button to go into edit mode
		solo.clickOnActionBarItem(R.id.edit_timeline);

		View view = solo.getView("buttonShowEndDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2100, 6, 17); // Choosing a date to set

		// Note: Even though we set the month as 6, this is setting
		// it as 7 (July) because Android uses 0 as January, 1 as February,
		// etc, and so when we search for this date on the screen, search for
		// 6+1, which is 7.
		solo.clickOnButton(1); // Clicking set button

		// Look for the date that was set
		assertTrue(solo.searchText("7-17-2100"));

		// return back to the activity this test started at
		solo.clickOnActionBarItem(R.id.cancel_edit_timeline);
		solo.clickOnButton(1);

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);
	}

	/** Testing that you can give an entry a title. **/
	public void testWritingTitle() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);

		// click on the edit button to go into edit mode
		solo.clickOnActionBarItem(R.id.edit_timeline);

		// Giving entry a title
		solo.enterText(0, "Editing Title of Timeline");

		// return back to the activity this test started at
		solo.clickOnActionBarItem(R.id.cancel_edit_timeline);
		solo.clickOnText("OK");

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);
	}

	/** Testing that you can give a description. **/
	public void testWritingDescription() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);

		// click on the edit button to go into edit mode
		solo.clickOnActionBarItem(R.id.edit_timeline);

		// Giving entry a description
		solo.enterText(1, "Editing Description of Timeline");

		// return back to the activity this test started at
		solo.clickOnActionBarItem(R.id.cancel_edit_timeline);
		solo.clickOnText("OK");

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);
	}

	/** Testing that you can give an entry a location. **/
	public void testWritingTitleDescriptionLocation() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);

		// click on the edit button to go into edit mode
		solo.clickOnActionBarItem(R.id.edit_timeline);

		// Giving entry a location
		solo.enterText(2, "ViewAndEditEntryActivity Location of Timeline");

		// return back to the activity this test started at
		solo.clickOnActionBarItem(R.id.cancel_edit_timeline);
		solo.clickOnText("OK");

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);
	}

	/** Testing that we can click an entry in the list of entries. **/
	public void testClickEntry() {

		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditTimelineActivity.class);

		// Getting the listview in this activity
		ListView myListView = (ListView) solo.getView(
				R.id.allEntriesInTimelineListView, 0);

		// If there is at least one entry in existence:
		if ((myListView.getChildCount() > 0)) {
			Log.i("Child count",
					"Child count is: "
							+ Integer.toString(myListView.getChildCount()));
			Log.i("Child count",
					"First visible child is: "
							+ Integer.toString(myListView
									.getFirstVisiblePosition()));

			// Getting that first child:
			View entryView = myListView.getChildAt(0);

			// Clicking the first entry
			solo.clickOnView(entryView);

			solo.sleep(1000); // Give time to change activity

			// Showing that we're now at the Create New Timeline Activity
			solo.assertCurrentActivity("Current Activity",
					ViewAndEditEntryActivity.class);

			// Now we need to go back to the previous activity, the one we're
			// testing
			solo.goBack();

		}

	}

	/** Testing that we have the correct help screen text **/
	public void testClickMenuItemHelp() {
		// Clicking on help menu item
		solo.clickOnMenuItem("Help");

		// Checking that we're now on the help screen:
		solo.assertCurrentActivity("Current Activity", HelpScreenActivity.class);

		// Getting the expected help string:
		String helpMessage = getInstrumentation().getTargetContext()
				.getResources().getString(R.string.view_edit_timeline_help);

		// Checking that we have the correct help screen text:
		solo.waitForText(helpMessage);

		solo.goBack();
	}
}
