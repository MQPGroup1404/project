package com.example.mqpapp.test.create_view_edit_timeline;

import android.app.ActionBar;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;

import com.example.mqpapp.HelpScreenActivity;
import com.example.mqpapp.R;
import com.example.mqpapp.create_view_edit_timeline.CreateNewTimelineActivity;
import com.robotium.solo.Solo;

public class CreateNewTimelineActivityTests extends
		ActivityInstrumentationTestCase2<CreateNewTimelineActivity> {

	public CreateNewTimelineActivityTests() {
		// Must set this up as "super(CLASSNAME.class);"
		super(CreateNewTimelineActivity.class);
		// TODO Auto-generated constructor stub
	}

	private Solo solo; // Need to create this 'solo' object for testing

	protected void setUp() throws Exception {
		super.setUp();
		// This is the testing object:
		solo = new Solo(getInstrumentation(), getActivity());
	}

	/**
	 * Testing that you can click the two buttons in the main activity, in
	 * addition to checking that the first activity runs.
	 */
	/** Testing that the pieces of our first activity still exist. **/
	public void testViewAllTimelinesActivity() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewTimelineActivity.class);
	}

	/** Testing that the pieces of our first activity still exist. **/
	public void testActionBar() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewTimelineActivity.class);

		// Getting the action bar object
		ActionBar actionBar = getActivity().getActionBar();

		// Asserting that we do have an action bar
		assertNotNull(actionBar);

	}

	/** Testing that you can give a timeline a title, description, and location. **/
	public void testWritingTitleDescriptionLocation() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewTimelineActivity.class);

		// Giving entry a title
		solo.enterText(0, "Title of Timeline");

		// Giving entry a description
		solo.enterText(1, "Description of Timeline");

		// Giving entry a location
		solo.enterText(2, "Location of Timeline");

		// Finding all of the text we just entered
		assertTrue(solo.searchText("Title of Timeline"));
		assertTrue(solo.searchText("Description of Timeline"));
		assertTrue(solo.searchText("Location of Timeline"));

	}

	/** Testing that you can set the date with the date picker. **/
	public void testSettingDate() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewTimelineActivity.class);

		// Clicking the pick date button and choosing a starting date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		View view = solo.getView("buttonShowStartDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2014, 6, 16); // Choosing a date to set
		// Note: Even though we set the month as 6, this is setting
		// it as 7 (July) because Android uses 0 as January, 1 as February,
		// etc, and so when we search for this date on the screen, search for
		// 6+1, which is 7.
		solo.clickOnButton(1); // Clicking set button

		// Look for the date that was set
		assertTrue(solo.searchText("7-16-2014"));

		// Clicking the pick date button and choosing an ending date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		view = solo.getView("buttonShowEndDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2015, 7, 17); // Choosing a date to set
		// Note: Even though we set the month as 6, this is setting
		// it as 7 (July) because Android uses 0 as January, 1 as February,
		// etc, and so when we search for this date on the screen, search for
		// 6+1, which is 7.
		solo.clickOnButton(1); // Clicking set button

		// Look for the date that was set
		assertTrue(solo.searchText("8-17-2015"));
		
		// Checking that the start date is still there
		assertTrue(solo.searchText("7-16-2014"));

	}
	
	/** Testing that you can set the date with the date picker. **/
	public void testSettingDateTwo() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewTimelineActivity.class);

		// Clicking the pick date button and choosing a starting date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		View view = solo.getView("buttonShowStartDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2015, 0, 16); // Choosing a date to set
		solo.clickOnButton(1); // Clicking set button

		// Look for the date that was set
		assertTrue(solo.searchText("1-16-2015"));

		// Clicking the pick date button and choosing an ending date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		view = solo.getView("buttonShowEndDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2015, 7, 17); // Choosing a date to set
		solo.clickOnButton(1); // Clicking set button

		// Look for the end date that was set
		assertTrue(solo.searchText("8-17-2015"));
		
		// Checking that the start date is still there
		assertTrue(solo.searchText("1-16-2015"));

	}
	
	/** Testing that you can set the date with the date picker. **/
	public void testSettingDateGetErrorMessage() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewTimelineActivity.class);

		// Clicking the pick date button and choosing a starting date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		View view = solo.getView("buttonShowStartDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2015, 0, 16); // Choosing a date to set
		solo.clickOnButton(1); // Clicking set button

		// Look for the date that was set
		assertTrue(solo.searchText("1-16-2015"));

		// Clicking the pick date button and choosing an ending date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		view = solo.getView("buttonShowEndDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2014, 7, 17); // Choosing a date to set
		solo.clickOnButton(1); // Clicking set button

		// Look for the end date that was NOT set
		assertFalse(solo.searchText("8-17-2014"));
		
		// Error message received when end date is before start date:
		String errorMessage = getInstrumentation().getTargetContext().getResources().getString(R.string.end_date_error);
		
		// Looking for error message regarding incorrect dates (end before start):
		assertTrue(solo.searchText(errorMessage));
		
		// Checking that the start date is still there
		assertTrue(solo.searchText("1-16-2015"));

	}
	
	/** Testing that you can set the date with the date picker. **/
	public void testSettingDateGetErrorMessageTwo() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewTimelineActivity.class);

		// Clicking the pick date button and choosing an ending date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		View view = solo.getView("buttonShowEndDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2009, 7, 17); // Choosing a date to set
		solo.clickOnButton(1); // Clicking set button
		
		// Clicking the pick date button and choosing a starting date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		view = solo.getView("buttonShowStartDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2015, 0, 16); // Choosing a date to set
		solo.clickOnButton(1); // Clicking set button
		
		// Error message received when end date is before start date:
		String errorMessage = getInstrumentation().getTargetContext().getResources().getString(R.string.start_date_error);
		
		// Looking for error message regarding incorrect dates (end before start):
		assertTrue(solo.searchText(errorMessage));

	}

	/** Testing that we have the correct help screen text **/
	public void testClickMenuItemHelp() {
		// Clicking on help menu item
		solo.clickOnMenuItem("Help");
		
		// Checking that we're now on the help screen:
		solo.assertCurrentActivity("Current Activity",
				HelpScreenActivity.class);
		
		// Getting the expected help string:
		String helpMessage = getInstrumentation().getTargetContext().getResources().getString(R.string.new_timeline_help);
		
		// Checking that we have the correct help screen text:
		solo.waitForText(helpMessage);
		
		solo.goBack();
	}
}
