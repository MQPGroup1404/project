package com.example.mqpapp.test.create_view_edit_timeline;

import java.util.Arrays;

import android.test.ActivityInstrumentationTestCase2;

import com.example.mqpapp.create_view_edit_timeline.Timeline;
import com.example.mqpapp.create_view_edit_timeline.ViewAllTimelinesActivity;

public class JunitTestsForTimelines extends
		ActivityInstrumentationTestCase2<ViewAllTimelinesActivity> {

	public JunitTestsForTimelines() {
		// Must set this up as "super("PACKAGE NAME", CLASSNAME.class);"
		super(ViewAllTimelinesActivity.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Checking the default values in the Timeline class
	 */
	public void testCheckValues() {
		// This should always be zero:
		assertEquals(Timeline.get_create_new_timeline_activity(), 0);

		// This should always be one:
		assertEquals(Timeline.get_create_new_entry_activity(), 1);

		// This should always be two:
		assertEquals(Timeline.get_result_load_image(), 2);

		// This should always be three:
		assertEquals(Timeline.get_view_entry_activity(), 3);

		// This should always be four:
		assertEquals(Timeline.get_view_timeline(), 4);
	}

	/**
	 * Checking the usage of clicking the start date
	 */
	public void testSettingAndGettingStartDateClicked() {
		Timeline.reset_start_date_clicked();
		
		// This should start as zero:
		assertEquals(0, Timeline.get_start_date_clicked());

		// Sets this to 1:
		Timeline.set_start_date_clicked();

		// This should now be 1:
		assertEquals(1, Timeline.get_start_date_clicked());

		// Sets this to 0 (resetting it back to false, basically):
		Timeline.reset_start_date_clicked();

		// Now this should be zero:
		assertEquals(Timeline.get_start_date_clicked(), 0);
	}

	/**
	 * Checking the usage of clicking the end date
	 */
	public void testSettingAndGettingEndDateClicked() {
		Timeline.reset_end_date_clicked();
		
		// This should start as zero:
		assertEquals(Timeline.get_end_date_clicked(), 0);

		// Sets this to 1:
		Timeline.set_end_date_clicked();

		// This should now be 1:
		assertEquals(Timeline.get_end_date_clicked(), 1);

		// Sets this to 0 (resetting it back to false, basically):
		Timeline.reset_end_date_clicked();

		// Now this should be zero:
		assertEquals(Timeline.get_end_date_clicked(), 0);
	}

	/**
	 * Showing that checkIfStartDateIsBeforeEndDate method works properly.
	 */
	public void testStartAndEndDateCheckerInOrder() {
		assertTrue(Timeline.checkIfStartDateIsBeforeEndDate(4, 21, 1993, 5, 21,
				1993));

	}
	
	/**
	 * Showing that checkIfStartDateIsBeforeEndDate method works properly.
	 */
	public void testStartAndEndDateCheckerOutOfOrder() {
		assertFalse(Timeline.checkIfStartDateIsBeforeEndDate(4, 21, 1994, 5, 21,
				1993));

	}
	
	/**
	 * Showing that checkIfStartDateIsBeforeEndDate method works properly.
	 */
	public void testStartAndEndDateCheckerSameDate() {
		assertTrue(Timeline.checkIfStartDateIsBeforeEndDate(6, 21, 2015, 6, 21,
				2015));

	}
	
	/**
	 * Showing that getMonthDayYearFromString method works properly.
	 */
	public void testGetMonthDayYearFromString() {
		// This is the date array we're looking for
		Integer[] expectedMonthDayYear = {6, 16, 2016};
		Integer[] actualMonthDayYear = Timeline.getMonthDayYearFromString("6-16-2016");
		// Checking that these two arrays are the same
		assertTrue(Arrays.equals(expectedMonthDayYear, actualMonthDayYear));

		// This is the date array we're looking for
		Integer[] expectedMonthDayYear2 = {1, 4, 1997};
		Integer[] actualMonthDayYear2 = Timeline.getMonthDayYearFromString("1-4-1997");
		// Checking that these two arrays are the same
		assertTrue(Arrays.equals(expectedMonthDayYear2, actualMonthDayYear2));
		
		// This is the date array we're looking for
		Integer[] expectedMonthDayYear3 = {12, 14, 2011};
		Integer[] actualMonthDayYear3 = Timeline.getMonthDayYearFromString("12-14-2011");
		// Checking that these two arrays are the same
		assertTrue(Arrays.equals(expectedMonthDayYear3, actualMonthDayYear3));

	}

}
