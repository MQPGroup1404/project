package com.example.mqpapp.test.create_view_edit_timeline;

import android.app.ActionBar;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.example.mqpapp.HelpScreenActivity;
import com.example.mqpapp.R;

import com.example.mqpapp.create_view_edit_timeline.CreateNewTimelineActivity;
import com.example.mqpapp.create_view_edit_timeline.ViewAllTimelinesActivity;
import com.example.mqpapp.create_view_edit_timeline.ViewAndEditTimelineActivity;
import com.robotium.solo.Solo;

public class ViewAllTimelinesActivityTests extends
		ActivityInstrumentationTestCase2<ViewAllTimelinesActivity> {

	public ViewAllTimelinesActivityTests() {
		// Must set this up as "super(CLASSNAME.class);"
		super(ViewAllTimelinesActivity.class);
		// TODO Auto-generated constructor stub
	}

	private Solo solo; // Need to create this 'solo' object for testing

	protected void setUp() throws Exception {
		super.setUp();
		// This is the testing object:
		solo = new Solo(getInstrumentation(), getActivity());
		solo.sleep(2000);
	}

	/**
	 * Testing that you can click the two buttons in the main activity, in
	 * addition to checking that the first activity runs.
	 */
	/** Testing that the pieces of our first activity still exist. **/
	public void testViewAllTimelinesActivity() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that first activity runs",
				ViewAllTimelinesActivity.class);
	}

	/** Testing that the pieces of our first activity still exist. **/
	public void testActionBar() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that first activity runs",
				ViewAllTimelinesActivity.class);

		// Getting the action bar object
		ActionBar actionBar = getActivity().getActionBar();

		// Asserting that we do have an action bar
		assertNotNull(actionBar);

		// Testing that I can click the add timeline option on the action bar
		assertTrue(getInstrumentation().invokeMenuActionSync(getActivity(),
				R.id.add_timeline, 0));

		solo.sleep(1000); // Give time to change activity
		// Showing that we're now at the Create New Timeline Activity
		solo.assertCurrentActivity("Current Activity",
				CreateNewTimelineActivity.class);

		// Now we need to go back to the previous activity, the one we're
		// testing
		solo.goBack();

	}

	/** Testing that we can click a timeline in the list of timelines. **/
	public void testClickTimeline() {
		Log.i("Child count", "Getting allTimelineListView!");

		// Getting the listview in this activity
		ListView myListView = (ListView) solo.getView(R.id.allTimelineListView,
				0);

		// If there is at least one timeline in existence:
		if ((myListView.getChildCount() > 0)) {
			Log.i("Child count",
					"Child count is: "
							+ Integer.toString(myListView.getChildCount()));
			Log.i("Child count",
					"First visible child is: "
							+ Integer.toString(myListView
									.getFirstVisiblePosition()));

			// Getting that first child:
			View timelineView = myListView.getChildAt(0);

			// Clicking the first timeline
			solo.clickOnView(timelineView);

			solo.sleep(1000); // Give time to change activity

			// Showing that we're now at the Create New Timeline Activity
			solo.assertCurrentActivity("Current Activity",
					ViewAndEditTimelineActivity.class);

			// Now we need to go back to the previous activity, the one we're
			// testing
			solo.goBack();

		}

	}

	/** Testing that we have the correct help screen text **/
	public void testClickMenuItemHelp() {
		// Clicking on help menu item
		solo.clickOnMenuItem("Help");
		
		// Checking that we're now on the help screen:
		solo.assertCurrentActivity("Current Activity",
				HelpScreenActivity.class);
		
		// Getting the expected help string:
		String helpMessage = getInstrumentation().getTargetContext().getResources().getString(R.string.view_all_timelines_help);
		
		// Checking that we have the correct help screen text:
		solo.waitForText(helpMessage);
		
		solo.goBack();
	}

}
