package com.example.mqpapp.test.create_view_edit_entry;

import android.test.ActivityInstrumentationTestCase2;

import com.example.mqpapp.create_view_edit_entry.Entry;
import com.example.mqpapp.create_view_edit_timeline.ViewAllTimelinesActivity;

public class JunitTestsForEntryClass extends
		ActivityInstrumentationTestCase2<ViewAllTimelinesActivity> {

	public JunitTestsForEntryClass() {
		// Must set this up as "super("PACKAGE NAME", CLASSNAME.class);"
		super(ViewAllTimelinesActivity.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Checking the default values in the Timeline class
	 */
	public void testCheckValues() {
		// This should always be one:
		assertEquals(Entry.get_result_load_image(), 1);

		// This should always be two:
		assertEquals(Entry.get_result_load_video(), 2);

	}
}