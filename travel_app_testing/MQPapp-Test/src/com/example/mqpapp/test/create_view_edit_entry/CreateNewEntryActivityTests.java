package com.example.mqpapp.test.create_view_edit_entry;

import android.app.ActionBar;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;

import com.example.mqpapp.HelpScreenActivity;
import com.example.mqpapp.R;
import com.example.mqpapp.create_view_edit_entry.CreateNewEntryActivity;
import com.robotium.solo.Solo;

public class CreateNewEntryActivityTests extends
		ActivityInstrumentationTestCase2<CreateNewEntryActivity> {

	public CreateNewEntryActivityTests() {
		// Must set this up as "super(CLASSNAME.class);"
		super(CreateNewEntryActivity.class);
		// TODO Auto-generated constructor stub
	}

	private Solo solo; // Need to create this 'solo' object for testing

	protected void setUp() throws Exception {
		super.setUp();
		// This is the testing object:
		solo = new Solo(getInstrumentation(), getActivity());
	}

	/**
	 * Testing that you can click the two buttons in the main activity, in
	 * addition to checking that the first activity runs.
	 */
	/** Testing that the pieces of our first activity still exist. **/
	public void testCreateNewEntryActivity() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);
	}

	/** Testing that the pieces of our first activity still exist. **/
	public void testActionBar() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Getting the action bar object
		ActionBar actionBar = getActivity().getActionBar();

		// Asserting that we do have an action bar
		assertNotNull(actionBar);

		// Testing that I can click the cancel_edit_entry option on the action
		// bar
		assertTrue(getInstrumentation().invokeMenuActionSync(getActivity(),
				R.id.cancel_edit_entry, 0));
		// Testing that I can click the save_new_entry option on the action bar
		assertTrue(getInstrumentation().invokeMenuActionSync(getActivity(),
				R.id.save_new_entry, 0));

	}

	/** Testing that you can set the date with the datepicker. **/
	public void testSettingDate() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Clicking the pick date button and choosing a date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		View view = solo.getView("buttonShowDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2014, 6, 16); // Choosing a date to set
		// Note: Even though we set the month as 6, this is setting
		// it as 7 (July) because Android uses 0 as January, 1 as February,
		// etc, and so when we search for this date on the screen, search for
		// 6+1, which is 7.
		solo.clickOnButton(1); // Clicking set button

		// Look for the date that was set
		assertTrue(solo.searchText("7-16-2014"));

	}
	
	/** Testing that you can set the date with the datepicker. **/
	public void testSettingDateTwo() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Clicking the pick date button and choosing a date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		View view = solo.getView("buttonShowDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 1993, 8, 21); // Choosing a date to set
		solo.clickOnButton(1); // Clicking set button

		// Look for the date that was set
		assertTrue(solo.searchText("9-21-1993"));

	}
	
	/** Testing that you can set the date with the datepicker. **/
	public void testSettingDateThree() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Clicking the pick date button and choosing a date
		// Getting date button, using the button's ID in the XML file for that
		// activity
		View view = solo.getView("buttonShowDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2050, 0, 5); // Choosing a date to set
		solo.clickOnButton(1); // Clicking set button

		// Look for the date that was set
		assertTrue(solo.searchText("1-5-2050"));

	}

	/** Testing that you can give an entry a title. **/
	public void testWritingTitle() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Giving entry a title
		solo.enterText(0, "Title of Entry");

		// Finding all of the text we just entered
		assertTrue(solo.searchText("Title of Entry"));

	}
	
	/** Testing that you can give an entry a title. **/
	public void testWritingTitleDifferent() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Giving entry a title
		solo.enterText(0, "This is a different title.");

		// Finding all of the text we just entered
		assertTrue(solo.searchText("This is a different title."));

	}
	
	/** Testing that you can give an entry a title. **/
	public void testWritingTitleSpecialCharacters() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Giving entry a title
		solo.enterText(0, "~!@#$%^&*())");

		// Finding all of the text we just entered
		assertTrue(solo.searchText("~!@#$%^&*())"));

	}
	
	/** Testing that you can give an entry a title. **/
	public void testWritingTitleWithNumbers() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Giving entry a title
		solo.enterText(0, "0123456789");

		// Finding all of the text we just entered
		assertTrue(solo.searchText("0123456789"));

	}
	
	/** Testing that you can give an entry a description. **/
	public void testWritingDescription() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Giving entry a description
		solo.enterText(1, "Description of Entry");

		// Finding all of the text we just entered
		assertTrue(solo.searchText("Description of Entry"));

	}
	
	/** Testing that you can give an entry a description. **/
	public void testWritingDescriptionNumbers() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Giving entry a description
		solo.enterText(1, "0123456789");

		// Finding all of the text we just entered
		assertTrue(solo.waitForText("0123456789"));

	}
	
	/** Testing that you can give an entry a location. **/
	public void testWritingLocation() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Giving entry a location
		solo.enterText(2, "Location of Entry");

		// Finding all of the text we just entered
		assertTrue(solo.searchText("Location of Entry"));

	}
	
	/** Testing that you can give an entry a location. **/
	public void testWritingLocationNumbers() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				CreateNewEntryActivity.class);

		// Giving entry a location
		solo.enterText(2, "0123456789");

		// Finding all of the text we just entered
		assertTrue(solo.waitForText("0123456789"));

	}

	/** Testing that we have the correct help screen text **/
	public void testClickMenuItemHelp() {
		// Clicking on help menu item
		solo.clickOnMenuItem("Help");
		
		// Checking that we're now on the help screen:
		solo.assertCurrentActivity("Current Activity",
				HelpScreenActivity.class);
		
		// Getting the expected help string:
		String helpMessage = getInstrumentation().getTargetContext().getResources().getString(R.string.new_entry_help);
		
		// Checking that we have the correct help screen text:
		solo.waitForText(helpMessage);
		
		solo.goBack();
	}

}
