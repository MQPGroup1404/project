package com.example.mqpapp.test.create_view_edit_entry;

import android.app.ActionBar;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.example.mqpapp.HelpScreenActivity;
import com.example.mqpapp.R;
import com.example.mqpapp.create_view_edit_entry.ViewAndEditEntryActivity;
import com.example.mqpapp.create_view_edit_timeline.ViewAllTimelinesActivity;
import com.example.mqpapp.create_view_edit_timeline.ViewAndEditTimelineActivity;
import com.example.mqpapp.media_handling.MultiPhotoSelectActivity;
import com.robotium.solo.Solo;

public class ViewAndEditEntryTests extends
		ActivityInstrumentationTestCase2<ViewAllTimelinesActivity> {
	private final String testTimeline = "Robotium JUnit Test Timeline";
	private final String testEntry = "Robotium JUnit Test Entry";
	private final String testMedia = "Robotium JUnit Test Media";

	public ViewAndEditEntryTests() {
		// Must set this up as "super(CLASSNAME.class);"
		super(ViewAllTimelinesActivity.class);
	}

	private Solo solo; // Need to create this 'solo' object for testing

	protected void setUp() throws Exception {
		super.setUp();

		// This is the testing object:
		solo = new Solo(getInstrumentation(), getActivity());

		// make sure this current activity is the correct one
		solo.assertCurrentActivity(
				"Make sure the solo is at the correct activity",
				ViewAllTimelinesActivity.class);

		// get the list of timelines
		ListView timelineListView = (ListView) solo
				.getView(R.id.allTimelineListView);

		// wait for the database to populate the list
		solo.sleep(500);

		// if a timeline doesn't exist, then create one for testing
		if (timelineListView.getChildCount() == 0) {
			// go to create a new timeline
			solo.clickOnActionBarItem(R.id.add_timeline);

			// set the title of the new timeline to create for testing
			EditText titleText = solo.getEditText("Title");
			solo.enterText(titleText, testTimeline);

			// create this timeline
			solo.clickOnActionBarItem(R.id.save_new_timeline);
		}

		// at least 1 timeline should exist now, so click on the first one
		solo.clickInList(1);

		// verify this transitioned to the correct activity
		solo.assertCurrentActivity(
				"Make sure the solo is viewing a timeline now",
				ViewAndEditTimelineActivity.class);

		// get the list of entries
		ListView entryListView = (ListView) solo
				.getView(R.id.allEntriesInTimelineListView);

		// wait for the database to populate the list
		solo.sleep(500);

		// if an entry doesn't exist, then create one for testing
		if (entryListView.getChildCount() == 0) {
			// go to create a new timeline
			solo.clickOnActionBarItem(R.id.timeline_add_entry);

			// set the title of the new timeline to create for testing
			EditText titleText = solo.getEditText("Title");
			solo.enterText(titleText, testEntry);

			// create this timeline
			solo.clickOnActionBarItem(R.id.save_new_entry);
		}

		// at least 1 entry should exist now, so click on the first one
		solo.clickInList(1);

		// verify that this activity is the correct activity to be in for
		// further testing
		solo.assertCurrentActivity(
				"Make sure the solo is at the correct activity",
				ViewAndEditEntryActivity.class);
	}

	/**
	 * Testing that you can click the two buttons in the main activity, in
	 * addition to checking that the first activity runs.
	 */
	protected void tearDown() throws Exception {
		// check the current activity
		solo.assertCurrentActivity(
				"Check to make sure the test ended in the ViewAndEditEntryActivity",
				ViewAndEditEntryActivity.class);

		// attempt to go back to the main activity that views all timelines
		solo.goBack();
		solo.goBack();

		// verify that the current activity is the correct activity
		solo.assertCurrentActivity(
				"Check to make sure the current activity is the initial activity",
				ViewAllTimelinesActivity.class);
		super.tearDown();

	}

	/** Testing that you can edit the text on a media item **/
	public void testEditTextFragment() {
		ListView mediaListView = (ListView) solo.getView(
				R.id.allMediaInEntryListView, 0);

		// If there is at least one timeline in existence:
		if ((mediaListView.getChildCount() > 0)) {
			Log.i("Child count",
					"Child count is: "
							+ Integer.toString(mediaListView.getChildCount()));

			// Getting that first child:
			View mediaItemView = mediaListView.getChildAt(0);

			// Getting the edit button
			View editButton = mediaItemView.findViewById(R.id.edit_text_media);

			// Clicking the edit button
			solo.clickOnView(editButton);

			// Getting the edit text box in the fragment
			EditText editTextBox = (EditText) mediaItemView
					.findViewById(R.id.entry_edit_text_new_dialog);

			// enter text to add
			solo.enterText(editTextBox, testMedia);

			// test that text can be entered
			assertTrue(solo.searchText(testMedia));

			// go back to viewing the entry
			solo.clickOnButton(0);

			// make sure this test is ending where it started
			solo.assertCurrentActivity("Check that this activity runs",
					ViewAndEditEntryActivity.class);

		}

	}

	/** Testing that the pieces of our first activity still exist. **/
	public void testCreateNewEntryActivity() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);
	}

	/** Testing that the pieces of our first activity still exist. **/
	public void testActionBar() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);

		// Getting the action bar object
		ActionBar actionBar = getActivity().getActionBar();

		// Asserting that we do have an action bar
		assertNotNull(actionBar);

	}

	/** Testing that the fragment for adding text can be used **/
	public void testAddText() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);

		// display the pop-up for adding media
		solo.clickOnActionBarItem(R.id.entry_add_media);

		// open the fragment for adding text
		solo.clickOnMenuItem("Add Text", true);

		// get the text view for adding text
		EditText addTextBox = (EditText) solo
				.getView(R.id.entry_add_text_new_dialog);

		// enter text to add
		solo.enterText(addTextBox, testMedia);

		// test that text can be entered
		assertTrue(solo.searchText(testMedia));

		// go back to viewing the entry
		solo.clickOnButton(0);

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);
	}

	/** Testing that the activity for adding pictures can be used **/
	public void testAddPicture() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);

		// display the pop-up for adding media
		solo.clickOnActionBarItem(R.id.entry_add_media);

		// open the fragment for adding text
		solo.clickOnMenuItem("Add a Picture", true);

		solo.assertCurrentActivity(
				"Check that the photo selection activity can be accessed",
				MultiPhotoSelectActivity.class);

		// go back to viewing the entry
		solo.goBack();

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);
	}

	/** Testing that the activity for adding videos can be used **/
	public void testAddVideo() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);

		// display the pop-up for adding media
		solo.clickOnActionBarItem(R.id.entry_add_media);

		// make sure the option for adding a video is in the pop-up
		solo.searchText("Add a Video");

		// get rid of the pop-up
		solo.goBack();

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);
	}

	/** Testing that the activity for fragment audio can be used **/
	public void testAddAudio() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);

		// display the pop-up for adding media
		solo.clickOnActionBarItem(R.id.entry_add_media);

		// open the fragment for adding text
		solo.clickOnMenuItem("Add Audio", true);

		// get the text view for adding text to audio
		EditText addAudioCaption = (EditText) solo
				.getView(R.id.entry_audio_caption);

		// enter text to add
		solo.enterText(addAudioCaption, testMedia);

		// test that text can be entered
		assertTrue(solo.searchText(testMedia));

		// go back to viewing the entry
		// click on button 1 because button 0 is for recording audio
		solo.clickOnButton(1);

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);
	}

	/** Testing that you can set the date with the datepicker. **/
	public void testSettingDate() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);

		// click on the edit button to go into edit mode
		solo.clickOnActionBarItem(R.id.edit_entry);

		View view = solo.getView("buttonShowDatePicker");
		solo.clickOnView(view); // Clicking date button
		solo.setDatePicker(0, 2016, 6, 16); // Choosing a date to set

		// Note: Even though we set the month as 6, this is setting
		// it as 7 (July) because Android uses 0 as January, 1 as February,
		// etc, and so when we search for this date on the screen, search for
		// 6+1, which is 7.
		solo.clickOnButton(1); // Clicking set button

		// Look for the date that was set
		assertTrue(solo.searchText("7-16-2016"));

		// return back to the activity this test started at
		solo.clickOnActionBarItem(R.id.cancel_edit_entry);
		solo.clickOnButton(1);

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);
	}

	/** Testing that you can give an entry a title, description, and location. **/
	public void testWritingTitleDescriptionLocation() {
		// This is the first test. Makes sure you have the right activity to
		// start with.
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);

		// click on the edit button to go into edit mode
		solo.clickOnActionBarItem(R.id.edit_entry);

		// Giving entry a title
		solo.enterText(0, "Editing Title of Entry");

		// Giving entry a description
		solo.enterText(1, "Editing Description of Entry");

		// Giving entry a location
		solo.enterText(2, "ViewAndEditEntryActivity Location of Entry");

		// return back to the activity this test started at
		solo.clickOnActionBarItem(R.id.cancel_edit_entry);
		solo.clickOnButton(1);

		// make sure this test is ending where it started
		solo.assertCurrentActivity("Check that this activity runs",
				ViewAndEditEntryActivity.class);
	}

	/** Testing that we have the correct help screen text **/
	public void testClickMenuItemHelp() {
		// Clicking on help menu item
		solo.clickOnMenuItem("Help");
		
		// Checking that we're now on the help screen:
		solo.assertCurrentActivity("Current Activity",
				HelpScreenActivity.class);
		
		// Getting the expected help string:
		String helpMessage = getInstrumentation().getTargetContext().getResources().getString(R.string.view_edit_entry_help);
		
		// Checking that we have the correct help screen text:
		solo.waitForText(helpMessage);
		
		solo.goBack();
	}
}
