package com.example.mqpapp;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

/**
 * This class is used for all help screens. The text on the screen is populated
 * depending on what activity the user is in.
 * 
 */
public class HelpScreenActivity extends FragmentActivity {
	String helpText = "";

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null) {
			Bundle extras = getIntent().getExtras();
			if (extras == null) {
				helpText = null;
			} else {
				// get help text from calling activity if not null
				helpText = extras.getString("HELP_TEXT");
			}
		} else {
			helpText = (String) savedInstanceState.getSerializable("HELP_TEXT");
		}
		if (savedInstanceState != null) {
			helpText = savedInstanceState.getString("HELP_TEXT");

		}

		// set layout
		setContentView(R.layout.help_screen);
		// find text view
		TextView helpTextView = (TextView) findViewById(R.id.help_text);
		// set text to help text
		helpTextView.setText(helpText);

	}

}
