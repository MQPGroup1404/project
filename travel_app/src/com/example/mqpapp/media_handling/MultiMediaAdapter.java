package com.example.mqpapp.media_handling;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.R;
import com.example.mqpapp.create_view_edit_entry.Entry;

/**
 * This is a custom adapter for working with list views that allows for
 * different xml structures for each row in the list.
 * 
 */
public class MultiMediaAdapter extends BaseAdapter {

	// a context for updating purposes
	private Context context;
	// an int indication how much to reduce the size of an images bitmap
	private int imageReduction = 0;
	// Player for audio
	private MediaPlayer audioPlayer = new MediaPlayer();
	// This tells if the audio clip should be started or stopped:
	private boolean startPlaying = true;
	// Used for debugging:
	String LOG_TAG = "AudioLog";
	// the list this adapter is working with
	private List<HashMap<String, String>> mediaList;

	// assigning numeric values to the different media types
	private static int TEXT_MEDIA = 0;
	private static int PICTURE_MEDIA = 1;
	private static int PICTURE_WITH_CAPTION_MEDIA = 2;
	private static int AUDIO_MEDIA = 3;
	private static int VIDEO_MEDIA = 4;

	/*
	 * Example HashMap of Media Note: all keys are public static variables from
	 * the CouchbaseManager class
	 * 
	 * KEY | VALUE -------------------+----------------------------------
	 * MEDIA_TYPE | (any item type static variable) MEDIA_UUID |
	 * "some string uuid from database" MEDIA_MESAGE | "Any string" (not in
	 * picture type) MEDIA_PICTURE_URI | "A URI string" (not in text type)
	 */

	/**
	 * A constructor for this custom adapter
	 * 
	 * @param context
	 *            the context of the calling activity
	 * @param list
	 *            a list to put into the list view the list must be a list of
	 *            hash maps which use the keys defined as public static
	 *            variables above.
	 */
	public MultiMediaAdapter(Context context, List<HashMap<String, String>> list) {
		this.context = context;
		this.mediaList = list;
	}

	/**
	 * This method returns how many different views types there are. In other
	 * words, how many different structures are there to choose from for each
	 * row in the list.
	 */
	@Override
	public int getViewTypeCount() {
		return 5;
	}

	/**
	 * This method returns the type of layout that the given position in the
	 * list should have.
	 * 
	 * @param position
	 *            the row in the list to get the layout of
	 * @return an int representing which type of layout the given row should
	 *         have
	 */
	@Override
	public int getItemViewType(int position) {
		// get the map of the row in the list
		HashMap<String, String> itemMap = mediaList.get(position);
		if (itemMap.get(CouchbaseManager.MEDIA_TYPE).equals(
				CouchbaseManager.MEDIA_TYPE_TEXT)) {
			return TEXT_MEDIA; // Return the int value for text media
		} else if (itemMap.get(CouchbaseManager.MEDIA_TYPE).equals(
				CouchbaseManager.MEDIA_TYPE_PICTURE)) {
			return PICTURE_MEDIA; // Return int value for a picture
		} else if (itemMap.get(CouchbaseManager.MEDIA_TYPE).equals(
				CouchbaseManager.MEDIA_TYPE_AUDIO)) {
			return AUDIO_MEDIA; // Return int value for audio
		} else if (itemMap.get(CouchbaseManager.MEDIA_TYPE).equals(
				CouchbaseManager.MEDIA_TYPE_VIDEO)) {
			return VIDEO_MEDIA; // Return int value for audio
		} else {
			// Return int value for picture with caption
			return PICTURE_WITH_CAPTION_MEDIA;
		}
	}

	@Override
	public int getCount() {
		return mediaList.size();
	}

	@Override
	public Object getItem(int position) {
		return mediaList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * This method returns a view that displays the proper data at the given
	 * position in the list
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		// get the type of this position in the list
		int positionType = getItemViewType(position);

		// check to see if the given view is null
		if (view == null) {
			// the given view is null, so create a new layout inflater to create
			// a view
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			// check the type of this position in the list to know which layout
			// to inflate
			if (positionType == TEXT_MEDIA) {
				view = inflater.inflate(R.layout.list_item_media_text, parent,
						false);
			} else if (positionType == PICTURE_MEDIA) {
				view = inflater.inflate(R.layout.list_item_media_picture,
						parent, false);
			} else if (positionType == AUDIO_MEDIA) {
				view = inflater.inflate(R.layout.list_item_media_audio, parent,
						false);
			} else if (positionType == VIDEO_MEDIA) {
				view = inflater.inflate(R.layout.list_item_media_video, parent,
						false);
			} else { // must be a picture with text
				view = inflater.inflate(
						R.layout.list_item_media_picture_with_text, parent,
						false);
			}
		}

		// get the hash map with all the info for this view
		HashMap<String, String> listItem = mediaList.get(position);

		// assign parts of the views to different values depending on what view
		// type this row is
		if (positionType == TEXT_MEDIA) {
			
			
			// get the parts of this view
			View buttonPanel = view.findViewById(R.id.edit_button_panel);
			buttonPanel.setVisibility(View.GONE);
			View viewSwitcher = (View) parent.getParent().getParent();
			View editView = viewSwitcher.findViewById(R.id.allMediaInEntryListViewEdit);
			if(editView == parent){
				buttonPanel.setVisibility(View.VISIBLE);
			}
			
			
			buttonPanel.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View arg0){
					
				}
			});
			
			TextView message = (TextView) view
					.findViewById(R.id.list_item_text);
			TextView uuid = (TextView) view.findViewById(R.id.list_item_uuid);
			// set the parts of this view
			message.setText(listItem.get(CouchbaseManager.MEDIA_MESSAGE));
			uuid.setText(listItem.get(CouchbaseManager.MEDIA_UUID));
			
			
			

			
		} else if (positionType == PICTURE_MEDIA) {

			// get the parts of this view
			ImageView image = (ImageView) view
					.findViewById(R.id.list_item_image_view);
			TextView uuid = (TextView) view.findViewById(R.id.list_item_uuid);
			
			// set the image
			executePictureTask(image,
					listItem.get(CouchbaseManager.MEDIA_PICTURE_URI));

			uuid.setText(listItem.get(CouchbaseManager.MEDIA_UUID));
			

			
		} else if (positionType == AUDIO_MEDIA) {

			// Getting the URI from the database:
			final String audioURI = listItem
					.get(CouchbaseManager.MEDIA_AUDIO_URI);

			// Get the button from the layout to play the audio
			final Button audioButton = (Button) view
					.findViewById(R.id.list_item_audio_button);
			
			//show button panel for edit mode
			View buttonPanel = view.findViewById(R.id.edit_button_panel);
			buttonPanel.setVisibility(View.GONE); 
			View viewSwitcher = (View) parent.getParent().getParent();
			View editView = viewSwitcher.findViewById(R.id.allMediaInEntryListViewEdit);
			if(editView == parent){
				buttonPanel.setVisibility(View.VISIBLE);
			}
			buttonPanel.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View arg0){
					
				}
			});
			

			// Creating an onclicklistener for the audio button, so audio can be
			// controlled
			audioButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					// Calling playback method, passing in where the audio is:
					onPlay(startPlaying, audioURI);
					// Resetting the value of startPlaying
					startPlaying = !startPlaying;
				}
			});

			// Get the layout pieces for the caption and hidden uuid
			TextView message = (TextView) view
					.findViewById(R.id.list_item_text);
			TextView uuid = (TextView) view.findViewById(R.id.list_item_uuid);
			
			Log.e("Media message for audio should be...", listItem.get(CouchbaseManager.MEDIA_MESSAGE));

			// Setting caption and hidden uuid text boxes' contents
			message.setText(listItem.get(CouchbaseManager.MEDIA_MESSAGE));
			uuid.setText(listItem.get(CouchbaseManager.MEDIA_UUID));

		} else if (positionType == VIDEO_MEDIA) {
			
			//  show button panel for editting
			View buttonPanel = view.findViewById(R.id.edit_button_panel);
			buttonPanel.setVisibility(View.GONE);
			View viewSwitcher = (View) parent.getParent().getParent();
			View editView = viewSwitcher.findViewById(R.id.allMediaInEntryListViewEdit);
			if(editView == parent){
				buttonPanel.setVisibility(View.VISIBLE);
			}
			buttonPanel.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View arg0){
					
				}
			});
			// Getting the URI of the video
			final String videoURI = listItem
					.get(CouchbaseManager.MEDIA_VIDEO_URI);

			// Get the videoview from the layout
			final VideoView videoView = (VideoView) view
					.findViewById(R.id.list_item_video);

			// Setting the videoview's URI
			videoView.setVideoURI(Uri.parse(videoURI));

			// Creating media controller for the video so it may be played:
			MediaController mediaController = new MediaController(context);
			mediaController.setMediaPlayer(videoView);
			mediaController.setAnchorView(videoView);
			videoView.setMediaController(mediaController);

			// Get the caption and hidden uuid text boxes for this video from
			// the layout
			TextView message = (TextView) view
					.findViewById(R.id.list_item_text);
			TextView uuid = (TextView) view.findViewById(R.id.list_item_uuid);

			// Putting the mediacontroller in the correct place
			((ViewGroup) mediaController.getParent())
					.removeView(mediaController);
			final FrameLayout frameLayoutForMediaController = (FrameLayout) view
					.findViewById(R.id.frame_layout);
			frameLayoutForMediaController.addView(mediaController);

			// Creating a preview image for the video
			Bitmap preview = ThumbnailUtils.createVideoThumbnail(
					Entry.convertURIToTruePath(context, videoURI),
					MediaStore.Images.Thumbnails.MINI_KIND);
			
			final ImageView videoPreview = (ImageView) view
					.findViewById(R.id.list_item_video_preview);

			// The box that will contain the image preview and play button
			final RelativeLayout imagePreviewBox = (RelativeLayout) view
					.findViewById(R.id.list_item_video_preview_layout);

			// Putting the image preview into an imageview
			videoPreview.setImageBitmap(preview);
			Log.i("Preview", "This is the preview: " + preview);
			Log.i("Preview", "This is the videopreview imageview: "
					+ videoPreview);

			// When preview image is clicked, hide the preview image
			// and display the videoView and its mediacontroller
			videoPreview.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					// Hiding the image preview:
					imagePreviewBox.setVisibility(View.GONE);

					// Displaying the videoview:
					videoView.setVisibility(View.VISIBLE);
					// Start playing the video:
					videoView.start();

					// Displaying the framelayout containing the media
					// controller
					frameLayoutForMediaController.setVisibility(View.VISIBLE);
				}
			});

			// Getting the mediacontroller to hide and show up again
			videoView.setOnTouchListener(new View.OnTouchListener() {
				Boolean videoIsTouched = true;

				@SuppressLint("ClickableViewAccessibility")
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// Seeing if this was a downward click
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						// Showing the media controller when pressed once
						videoIsTouched = !videoIsTouched;
					}
					// If this is the first time the video was clicked
					if (!videoIsTouched) {
						videoIsTouched = true;
						frameLayoutForMediaController
								.setVisibility(View.VISIBLE);

					} else {
						// If the video is being "re-clicked":
						// Hiding media controller when pressed a second time
						videoIsTouched = false;
						frameLayoutForMediaController.setVisibility(View.GONE);
					}
					return true;
				}
			});

			// Setting the caption and hidden uuid contents
			message.setText(listItem.get(CouchbaseManager.MEDIA_MESSAGE));
			uuid.setText(listItem.get(CouchbaseManager.MEDIA_UUID));
		} else { // must be a picture with text
					// get the parts of this view
			
			Log.i(LOG_TAG, "This is a picture file with a caption");

			// Getting the layout pieces (imageview, caption textbox, and hidden
			// uuid text box
			final ImageView image = (ImageView) view
					.findViewById(R.id.list_item_image_view);
			TextView message = (TextView) view
					.findViewById(R.id.list_item_text);
			TextView uuid = (TextView) view.findViewById(R.id.list_item_uuid);
			TextView imageUri = (TextView) view.findViewById(R.id.image_uri);
			
			// get the parts of this view
			View buttonPanel = view.findViewById(R.id.edit_button_panel);

			buttonPanel.setVisibility(View.GONE);
			View viewSwitcher = (View) parent.getParent().getParent();
			View editView = viewSwitcher.findViewById(R.id.allMediaInEntryListViewEdit);
			
			if(editView == parent){
				buttonPanel.setVisibility(View.VISIBLE);
			}
			buttonPanel.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View arg0){
					
				}
			});
			
			// set the image view width back to match_parent in the event it got changed
			LayoutParams layoutParams = image.getLayoutParams();
			layoutParams.width = LayoutParams.MATCH_PARENT;
			image.setLayoutParams(layoutParams);
			
			//get the image
			final String listItemImage = listItem.get(CouchbaseManager.MEDIA_PICTURE_URI);
					
			// set the contents of the image view in the predrawobserver's onPreDraw method
			// If the image view gets set before onPreDraw, the width will not be measured
			// yet. BitmapWorkerTask relies on the view being measured to work correctly.
			ViewTreeObserver viewTreeObserver = image.getViewTreeObserver();
			viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

				@Override
				public boolean onPreDraw() {
					image.getViewTreeObserver().removeOnPreDrawListener(this);
					executePictureTask(image, listItemImage);
					return true;
				}
				
			});
						
			
			// Setting the caption and hidden UUID boxes' contents
			message.setText(listItem.get(CouchbaseManager.MEDIA_MESSAGE));
			uuid.setText(listItem.get(CouchbaseManager.MEDIA_UUID));
			imageUri.setText(listItemImage);
			
			
			
		}

		return view;
	}
	
	


	/**
	 * This method sets the contents of the list that this list adapter to uses
	 * to populate a list view.
	 * 
	 * @param newList
	 *            the new information to put into the list view
	 */
	public void setList(List<HashMap<String, String>> newList) {
		mediaList = newList;
	}

	/**
	 * This method will execute a worker task to populate the picture in the
	 * list this adapter is linked to
	 * 
	 * @param image
	 *            the image view to populate with an image
	 * @param uri
	 *            the uri for the desired image to populate the image view with
	 */
	private void executePictureTask(ImageView image, String uri) {
		// set the parts of this view
		BitmapWorkerTask task = new BitmapWorkerTask(image, context);

		// check to see if a reduced bitmap should be used
		if (imageReduction > 1) {
			task.execute(uri, String.valueOf(imageReduction));
		}
		else {
			// execute the task for loading the picture into the adapter
			task.execute(uri);
		}
	}

	/*
	 * Referenced from: Android Developer Guide
	 * http://developer.android.com/guide/topics/media/audio-capture.html
	 */
	/**
	 * This method controls playback for an audio clip.
	 * 
	 * @param start
	 *            is a boolean saying whether the audio is starting or stopping
	 * @param location
	 *            of the audio file
	 */
	private void onPlay(boolean start, String audioClipFileLocation) {
		Log.i(LOG_TAG, "start value: " + start);
		if (start) {
			// Making a new MediaPlayer object
			audioPlayer = new MediaPlayer();
			audioPlayer.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer player) {
					startPlaying = true;
				}
			});
			try {
				// Locating the file and starting it
				audioPlayer.setDataSource(audioClipFileLocation);
				audioPlayer.prepare();
				audioPlayer.start();
			} catch (IOException e) {
				Log.e(LOG_TAG, "prepare() failed");
			}
		} else {
			// Stopping the audio clip from playing
			audioPlayer.release();
			audioPlayer = null;
		}
	}
	
	/**
	 * This method gets the current image reduction value
	 * @return the current image reduction value
	 */
	public int getImageReduction() {
		return imageReduction;
	}
	
	/**
	 * This method sets the current image reduction value.
	 * For example: a value of 2 would produce a bitmap that
	 * is roughly 1/2 the size of the image view it will populate.
	 * @param reduction the value to set the image reduction to
	 */
	public void setImageReduction(int reduction) {
		imageReduction = reduction;
	}
}
