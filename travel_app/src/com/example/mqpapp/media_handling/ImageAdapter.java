package com.example.mqpapp.media_handling;

import android.content.Context;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

// Source: http://developer.android.com/guide/topics/ui/layout/gridview.html
// This is a modified version of this example in the Android Development Guide.

public class ImageAdapter extends BaseAdapter {
	/* Interface to global information about an application environment. 
	 * This is an abstract class whose implementation is provided by the Android system. 
	 * It allows access to application-specific resources and classes, as well as up-calls 
	 * for application-level operations such as launching activities, broadcasting 
	 * and receiving intents, etc. */
    private Context mContext;
    private String picturePath;
	
    // Getting the data stream for the file
    final String[] filePathColumn = { MediaStore.Images.Media.DATA };
    
    public Uri[] uriList;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    /* Count the number of pictures that exist */
    public int getCount() {
    	if (uriList == null) {
    		return 0;
    	}
    	else {
    		return uriList.length;
    	}
    }

    public Object getItem(int position) {
        return uriList[position];
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        
    	if (convertView == null) {
    		/* Displays an arbitrary image, such as an icon. The ImageView class can load 
        	 * images from various sources (such as resources or content providers), 
        	 * takes care of computing its measurement from the image so that it can be 
        	 * used in any layout manager, and provides various display options such as 
        	 * scaling and tinting. */
            imageView = new ImageView(mContext);
            
            // Creates a new set of layout parameters with the specified width and height.
            imageView.setLayoutParams(new GridView.LayoutParams(320, 320));
            
            /* Scale the image uniformly (maintain the image's aspect ratio) so that both 
             * dimensions (width and height) of the image will be equal to or larger than 
             * the corresponding dimension of the view (minus padding). */
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            
            // setPadding(int left, int top, int right, int bottom)
            imageView.setPadding(10, 10, 10, 10);
    	} else {
    		imageView = (ImageView) convertView;
    	}

        // imageView.setImageURI(uriList.get(position));
    	BitmapWorkerTask task = new BitmapWorkerTask(imageView, mContext);
    	
    	picturePath = uriList[position].toString();
        
        task.execute(picturePath);
    	
        return imageView;
    }
}
