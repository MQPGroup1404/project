package com.example.mqpapp.media_handling;

import android.app.Activity;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * @author Paresh Mayani (@pareshmayani)
 */
public abstract class BaseActivity extends Activity {

	public static ImageLoader imageLoader = ImageLoader.getInstance();
	
}
