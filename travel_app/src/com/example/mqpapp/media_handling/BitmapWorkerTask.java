package com.example.mqpapp.media_handling;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.example.mqpapp.R;

/*
 * http://developer.android.com/training/displaying-bitmaps/process-bitmap.html
 */
public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
	// a tag for debugging purposes
    private static final String TAG = BitmapWorkerTask.class.getSimpleName();
	
    public static final String GOOGLE_PLUS_PIC = "content://com.google.android.apps.photos.content";
    
    private final WeakReference<ImageView> imageViewReference;
    String data;
    private int imageReduction = 1;
    
    // a reference to the android media storage
    final String[] filePathColumn = { MediaStore.Images.Media.DATA };

    // a context for this bitmap
	private Context mContext;

	/**
	 * Constructs a bit map worker task to load an image in the background
	 * @param imageView the view to load the image to
	 * @param internalStorage true indicates the picture to be loaded is in 
	 * internal storage. false indicates the picture is on the network
	 * @param context the current context
	 */
    public BitmapWorkerTask(ImageView imageView, Context context) {
        // Use a WeakReference to ensure the ImageView can be garbage collected
        imageViewReference = new WeakReference<ImageView>(imageView);
        this.mContext = context;
    }
    
	/**
	 * This method will set the ImageView to a temporary image while the proper
	 * image is loaded
	 */
    @Override
    protected void onPreExecute() {
        if (imageViewReference != null) {
            final ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                imageView.setImageResource(R.drawable.stub_image);
            }
        }
    }

	/**
	 * This method will decode the bitmap for an image in the background
	 * 
	 * @param params
	 *            Any number of input strings following the below criteria
	 * @param params[0]
	 *            a string representing a path to the file (URI) for the
	 *            picture to load a bitmap of
	 * @param params[1]
	 *            (optional) a number as a string representing how much the
	 *            bitmap should be shrunk down. For example: 2 would produce a
	 *            bitmap that is 1/2 or half the size of the ImageView given in
	 *            the constructor.
	 */
    @SuppressLint("NewApi") @Override
    protected Bitmap doInBackground(String... params) {
    	if (params[0] == null) {
    		Log.d(TAG, "Execute called on bitmap worker task without giving a file path");
    		return null;
    	}
        data = params[0];
        if (imageViewReference != null) {
        	final ImageView imageView = imageViewReference.get();
        	if (imageView != null) {
        		int imageWidth = imageView.getWidth();

        		// check to see if a reduction amount was given
        		if (params.length >= 2) {
        			imageReduction = Integer.valueOf(params[1]);
        			
        			// reduce the desired image width by the reduction value
        			imageWidth /= imageReduction;
        			
        			// create a bitmap of reduced size that is limited in both height and width 
        			return decodeSampledBitmapFromResource(data, imageWidth, imageWidth, mContext);
        		}
        		Log.i(TAG, "Image Width (post set) = " + String.valueOf(imageWidth));
        		return decodeSampledBitmapFromResource(data, imageWidth, -1, mContext);
        	}
        }
        return decodeSampledBitmapFromResource(data, 200, 200, mContext);
    }
    
   

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (imageViewReference != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
                
                if (imageReduction > 1) {
        			// set the imageView width to wrap content so that it sizes
        			// to the smaller image
        			LayoutParams layoutParams = imageView.getLayoutParams();
        			layoutParams.width = LayoutParams.WRAP_CONTENT;
        			imageView.setLayoutParams(layoutParams);
                }

                Log.i(TAG, "OnPost image height = " + String.valueOf(imageView.getLayoutParams().height));
                Log.i(TAG, "OnPost image width = " + String.valueOf(imageView.getLayoutParams().width));
                Log.i(TAG, "Bitmap loaded and ImageView set");
            }
        }
    }
    
	/**
	 * This method will calculate the ideal bitmap size with the given
	 * dimensions. The dimensions will likely be the width and height of the
	 * image view the bitmap will populate. One of the dimensions can be set to
	 * -1 to size the image based off of only one of the dimensions.
	 * 
	 * This method was modified from the calculateInSampleSize method at this URL
	 * source :
	 * http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
	 * 
	 * @param options
	 *            A bitmap factory options instance that has the image width and
	 *            height
	 * @param reqWidth
	 *            The width the bitmap should fit within
	 * @param reqHeight
	 *            The height the bitmap should fit within
	 * @return The sample size that the the bitmap should be decoded with using
	 *         the bitmap factory
	 */
    static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inHeightSampleSize = 1;
	    int inWidthSampleSize = 1;
	    
	    // calculate the ideal bitmap size for the height
	    if (reqHeight != -1) {
	    	// is the image higher than the box it is being put in?
	    	if (height > reqHeight) {
	    		
	    		final int halfHeight = height / 2;
	    		
		        // Calculate the largest inSampleSize value that is a power of 2 and keeps
		        // the height larger than the requested height.
	    		while((halfHeight / inHeightSampleSize) > reqHeight) {
	    			inHeightSampleSize *= 2;
	    		}
	    	}
	    	
	    }
	    
	    // calculate the ideal bitmap size for the width
	    if (reqWidth != -1) {
	    	// is the image wider than the box it is being put in?
		    if (width > reqWidth) {
		
		        final int halfWidth = width / 2;
		
		        // Calculate the largest inSampleSize value that is a power of 2 and keeps
		        // the width larger than the requested width.
		        while ((halfWidth / inWidthSampleSize) > reqWidth) {
		            inWidthSampleSize *= 2;
		        }
		    }
	    }
	    
	    // return the correct sample size based on which values were given
	    if (reqWidth != -1) {
	    	if (reqHeight != -1) {
	    		// Both height and width were given so return the smaller sample size
	    		if (inWidthSampleSize <= inHeightSampleSize) {
	    			return inWidthSampleSize;
	    		}
	    		else {
	    			return inHeightSampleSize;
	    		}
	    	}
	    	else { // reqHeight == -1
	    		// Only the width was given, so return the width sample size
	    		return inWidthSampleSize;
	    	}
	    }
	    else { // reqWidth == -1
	    	if (reqHeight != -1) {
	    		// Only the height was given, so return the height sample size
	    		return inHeightSampleSize;
	    	}
	    	else { // reqHeight == -1
	    		// Both height and width were -1, so return 1
	    		return 1;
	    	}
	    }
	}
    
    /*
     * http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
     */
    public static Bitmap decodeSampledBitmapFromResource(String pathToFile,
            int reqWidth, int reqHeight, Context context) {
    	boolean useInternalStorage = true;
    	
    	Log.i(TAG, "Path to file = " + pathToFile);
    	
    	// check to see if this file is on the device or on the network
    	if (pathToFile.startsWith(GOOGLE_PLUS_PIC)) {
    		useInternalStorage = false;
    	}

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;

        if (useInternalStorage) {
            BitmapFactory.decodeFile(pathToFile, options);
        }
        else {
        	try {
				InputStream iStream = context.getContentResolver().openInputStream(Uri.parse(pathToFile));
				BitmapFactory.decodeStream(iStream, null, options);
			} catch (FileNotFoundException e) {
				Log.e(TAG, "unable to load image from network");
			}
        }

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        
        if (useInternalStorage) {
            return BitmapFactory.decodeFile(pathToFile, options);
        }
        else {
	        try {
				InputStream iStream = context.getContentResolver().openInputStream(Uri.parse(pathToFile));
				return BitmapFactory.decodeStream(iStream, null, options);
			} catch (FileNotFoundException e) {
				Log.e(TAG, "unable to load image from network");
			}
        }
		return null;
    }
    
    /**
     * This method helps get ready to execute this worker task by converting the string version
     * of a Uri to the actual file path that this worker task needs.
     * @param picture the Uri to convert from
     * @return a string representing the file path to the given Uri. This file path can be
     * used to execute an instance of this worker task.
     */
    public String convertUriStringToStoragePath(String uri) {
    	uri = "content:/" + uri;
    	Uri pictureUri = Uri.parse(uri);
    	Log.w(TAG, "Converting to storage path : " + pictureUri.toString());
    	
    	if (pictureUri.toString().startsWith(GOOGLE_PLUS_PIC)) {
    		return pictureUri.toString();
    	}
    	
    	// Make a cursor using the URI for the image and its data stream
        Cursor cursor = mContext.getContentResolver().query(pictureUri,filePathColumn, null, null, null);
    	Log.d(TAG, "pictureURI : " + pictureUri + ", and filePathColumn: " + filePathColumn + ", and context: " + mContext);
        
        //Move the cursor to the first row.
        cursor.moveToFirst();
        
        // Returns index for column name
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        
        // getString Returns the value of the requested column as a String.
        String picturePath = cursor.getString(columnIndex);
        
        // close the cursor before returning
        cursor.close();
        
        // return the file path to the picture
        return picturePath;
    }
}