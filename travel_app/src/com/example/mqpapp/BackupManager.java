package com.example.mqpapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.IOUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.MetadataChangeSet;

@SuppressLint("SimpleDateFormat")
public class BackupManager {
	// Used for uploading to Google Drive
	protected static final int REQUEST_CODE_UPLOAD_DATABASE = 444;
	// Used for downloading from Google Drive
	protected static final int REQUEST_CODE_DOWNLOAD_DATABASE = 555;

	// Used for authorization with Google Play Services
	private static final int REQ_AUTH = 8989;

	// Source: https://developer.android.com/google/auth/api-client.html
	// Request code to use when launching the resolution activity
	private static final int REQUEST_RESOLVE_ERROR = 1001;
	// Unique tag for the error dialog fragment
	private static final String DIALOG_ERROR = "dialog_error";
	// Bool to track whether the app is already resolving an error
	private static boolean mResolvingError = false;
	// For logging, so you know you're downloading from Google Drive
	// and overwriting the database with this copy
	private static String googleDownloadDatabaseTag = "Google Download Database";

	/**
	 * This method will make a backup of the current database. It calls
	 * makeDatabaseFileBackup, which produces an array with the current database
	 * file (0) and an empty backup database file (1), and then it uses
	 * backUpDatabaseToExternal to copy the current database into the backup
	 * file.
	 * 
	 * @param context
	 *            is the application's context
	 */
	public static void copyAndBackUpDatabase(Context context) {
		ArrayList<File> databases = makeDatabaseFileBackup(context);

		// Backing up the database to the SD card of a device
		// 0 is the currentDB
		// 1 is the backupDB
		backUpDatabaseToExternal(databases.get(0), databases.get(1));
	}

	/**
	 * This method will make a backup of the current database.
	 * 
	 * @param context
	 *            is the application's context
	 */
	public static ArrayList<File> makeDatabaseFileBackup(Context context) {
		// This array contains the location of the current and external
		// databases
		ArrayList<File> databases = new ArrayList<File>();
		try {
			File sdCard = Environment.getExternalStorageDirectory();

			if (sdCard.canWrite()) {
				// This is where the current copy of the database is:
				String currentDBPath = context.getFilesDir().toString() + "/"
						+ CouchbaseManager.getDatabaseName() + ".cblite";
				// Create a current DB file
				File currentDB = new File(currentDBPath);

				// Will be the name of the backup String
				String backupDBPath = CouchbaseManager.getDatabaseName()
						+ ".cblite";
				// Create a backup DB file on the SD card:
				File backupDB = new File(sdCard, backupDBPath);

				// Adding these two locations to the array
				databases.add(currentDB);
				databases.add(backupDB);

			}
		} catch (Exception e) {
			Log.e("Error",
					"Error in creating backup of database: " + e.toString());
		}

		// Returning array with backup locations
		return databases;
	}

	/**
	 * This method backs up the database to an external save location.
	 * 
	 * @param currentDB
	 *            is the path to the current database's location
	 * @param backupDB
	 *            is the path to the backup database's location on external
	 *            storage
	 */
	public static void backUpDatabaseToExternal(File currentDB, File backupDB) {
		// Now, copy the file from internal to external storage
		try {
			@SuppressWarnings("resource")
			FileChannel src = new FileInputStream(currentDB).getChannel();
			@SuppressWarnings("resource")
			FileChannel dst = new FileOutputStream(backupDB).getChannel();
			dst.transferFrom(src, 0, src.size());
			src.close();
			dst.close();
		} catch (Exception e) {
			Log.e("Error", "Error in copying backup of database to SD card: "
					+ e.toString());
		}
	}

	/**
	 * This method is used when the user downloads a backup of the database from
	 * google drive
	 * 
	 * @param context
	 *            is the application context
	 * @param fileInputStream
	 *            is the file that is being used as the database file
	 */
	public static void overwriteDatabaseWithGoogleDriveFile(Context context,
			FileInputStream fileInputStream) {
		// Getting current database file
		File currentDB = new File(context.getFilesDir().toString() + "/"
				+ CouchbaseManager.getDatabaseName() + ".cblite");

		// Now, copy the file from external to internal
		// storage
		try {
			// Source file, the past DB version:
			FileChannel src = fileInputStream.getChannel();
			// Current database location:
			@SuppressWarnings("resource")
			FileChannel dst = new FileOutputStream(currentDB).getChannel();

			Log.i(googleDownloadDatabaseTag, "Have src and dist");

			// Copy over data and close
			dst.transferFrom(src, 0, src.size());
			src.close();
			dst.close();

			Log.i(googleDownloadDatabaseTag, "Did transfer and close.");

		} catch (Exception e) {
			Log.e("Error",
					"Error in setting up new database from google drive: "
							+ e.toString());
		}
	}

	/**
	 * This method is used when the user wants to restore a previous copy of the
	 * database (with their old timelines and entries)
	 * 
	 * @param context
	 *            is the application context
	 */
	public static void restorePreviousDatabase(Context context) {
		// Now, we will locate the current database:
		File currentDBPath = new File(context.getFilesDir().toString() + "/"
				+ CouchbaseManager.getDatabaseName() + ".cblite");

		// Next we locate the backup copy:
		File locatingBackupDBPath = new File(
				Environment.getExternalStorageDirectory() + "/"
						+ CouchbaseManager.getDatabaseName() + ".cblite");

		// Now, copy the file from external to internal storage
		try {
			// Source file, the past DB version:
			@SuppressWarnings("resource")
			FileChannel src = new FileInputStream(locatingBackupDBPath)
					.getChannel();
			// Current database location:
			@SuppressWarnings("resource")
			FileChannel dst = new FileOutputStream(currentDBPath).getChannel();

			// Copy over data and close
			dst.transferFrom(src, 0, src.size());
			src.close();
			dst.close();

		} catch (Exception e) {
			Log.e("Error", "Error in restoring database: " + e.toString());
		}
	}

	/**
	 * This method is used to create a copy of the database that can then be
	 * sent via email.
	 * 
	 * @return the intent for sending an email.
	 */
	public static Intent createEmailDatabaseIntent() {
		Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
		// Set the type to 'email'
		sendEmailIntent.setType("vnd.android.cursor.dir/email");
		// This is the email subject
		DateFormat dateFormater = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		sendEmailIntent.putExtra(
				Intent.EXTRA_SUBJECT,
				"Wanderlog Backup (Created on "
						+ dateFormater.format(new Date()) + ")");

		// Getting the backup file from the SD card:
		File backupDB = new File(Environment.getExternalStorageDirectory(),
				CouchbaseManager.getDatabaseName() + ".cblite");
		// Getting the URI for the file
		Uri fileURI = Uri.fromFile(backupDB);
		// This is the attachment
		sendEmailIntent.putExtra(Intent.EXTRA_STREAM, fileURI);
		return sendEmailIntent;
	}

	/**
	 * This method lets a user download his or her database from Google Drive.
	 * 
	 * @param googleApiClient
	 *            is the client used to connect to Google Drive
	 * @param activity
	 *            is the current activity
	 */
	public static void downloadFromGoogleDrive(GoogleApiClient googleApiClient,
			Activity activity) {

		IntentSender intentSender = Drive.DriveApi.newOpenFileActivityBuilder()
				.setActivityTitle("Download Data Backup")
				.build(googleApiClient);

		try {
			// Start the activity to download from Google Drive
			activity.startIntentSenderForResult(intentSender,
					REQUEST_CODE_DOWNLOAD_DATABASE, null, 0, 0, 0);
		} catch (SendIntentException e) {
			Log.w("Downloading from Google Drive", e.toString());
		}
	}

	/**
	 * This method will save the current copy of the database to Google Drive
	 * Referenced from:
	 * https://github.com/googledrive/android-demos/blob/master/src/com/google
	 * /android/gms/drive/sample/demo/CreateFileActivity.java
	 * 
	 * @param context
	 *            is the application's context
	 * @param googleApiClient
	 *            is the client used to connect to Google Drive
	 * @param activity
	 *            is the current activity
	 * @param manager
	 *            is the couchbase manager
	 */
	public final static void saveToGoogleDrive(Context context,
			final GoogleApiClient googleApiClient, final Activity activity,
			CouchbaseManager manager) {

		// Now, we acquire a file that is the current database
		// (NOT the backup) using makeDatabseFileBackup
		ArrayList<File> databaseArray = BackupManager
				.makeDatabaseFileBackup(context);

		// This is the current copy of the database,
		// which now has byte arrays in it
		final File currentDatabase = databaseArray.get(0);

		Log.i("Database updated", "Tried to get current version of database.");

		Drive.DriveApi.newDriveContents(googleApiClient).setResultCallback(
				new ResultCallback<DriveContentsResult>() {

					@Override
					public void onResult(DriveContentsResult result) {

						if (result.getStatus().isSuccess()) {
							// Otherwise, we can write our data to the new
							// contents.
							Log.i("Google Drive Upload",
									"New contents created.");
							// Get an output stream for the contents.
							OutputStream outputStream = result
									.getDriveContents().getOutputStream();

							// File Input Stream for the current database file
							FileInputStream fis;
							try {
								fis = new FileInputStream(currentDatabase);

								// Turning the file to a byte array
								byte[] fileData = IOUtils.toByteArray(fis);

								outputStream.write(fileData);
							} catch (Exception e2) {
								Log.i("Google Drive Copying", e2.toString());
							}

							// Getting the current date
							DateFormat dateFormater = new SimpleDateFormat(
									"MM-dd-yyyy");

							// Create the initial metadata - MIME type and
							// title.
							MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
									.setMimeType(
											MimeTypeMap.getSingleton()
													.getExtensionFromMimeType(
															"cblite"))
									.setTitle(
											"WanderLog_"
													+ dateFormater
															.format(new Date())
													+ ".cblite").build();
							// Create an intent for the file chooser, and start
							// it.
							IntentSender intentSender = Drive.DriveApi
									.newCreateFileActivityBuilder()
									.setInitialMetadata(metadataChangeSet)
									.setInitialDriveContents(
											result.getDriveContents())
									.build(googleApiClient);
							try {
								activity.startIntentSenderForResult(
										intentSender,
										REQUEST_CODE_UPLOAD_DATABASE, null, 0,
										0, 0);
							} catch (SendIntentException e) {
								Log.i("Google Drive",
										"Failed to launch file chooser.");
							}
						} else {
							Log.e("Google Drive Upload",
									"Failed to create new contents.");
							return;
						}
					}
				});

		// Lastly, we need to restore the previous copy of the database,
		// so the database doesn't always contain the images themselves
		// and just contains URIs.
		BackupManager.restorePreviousDatabase(context);
	}

	/**
	 * Used for errors when connecting to google play
	 * 
	 * @return REQUEST_RESOLVE_ERROR value
	 */
	public static int getRequestResolveError() {
		return REQUEST_RESOLVE_ERROR;
	}

	/**
	 * Used for errors when connecting to google play
	 * 
	 * @return DIALOG_ERROR string
	 */
	public static String getDialogError() {
		return DIALOG_ERROR;
	}

	/**
	 * Used for errors when connecting to google play
	 * 
	 * @return mResolvingError boolean
	 */
	public static boolean getmResolvingError() {
		return mResolvingError;
	}

	/**
	 * Used for errors when connecting to google play
	 * 
	 * @param mResolvingError
	 *            is set
	 */
	public static void setmResolvingError(boolean value) {
		mResolvingError = value;
	}

	/**
	 * Used for connecting to Google Play Services and uploading to google drive
	 */
	public static int getRequestCodeUploadDatabase() {
		return REQUEST_CODE_UPLOAD_DATABASE;
	}

	/**
	 * Used for connecting to Google Play Services and downloading from google
	 * drive
	 */
	public static int getRequestCodeDownloadDatabase() {
		return REQUEST_CODE_DOWNLOAD_DATABASE;
	}

	/**
	 * Used for connecting to Google Play Services
	 */
	public static int getRequestCodeAuth() {
		return REQ_AUTH;
	}

	/**
	 * Method to acquire the location of the database.
	 * 
	 * @param context
	 *            is the application context
	 * @return A string that brings you to the database location.
	 */
	public String getDatabaseLocation(Context context) {
		return context.getFilesDir().toString() + "/"
				+ CouchbaseManager.getDatabaseName() + ".cblite";
	}
}
