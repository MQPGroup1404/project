package com.example.mqpapp;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * This class is for the date picker fragment. The content within this class is
 * based on this source:
 * http://developer.android.com/guide/topics/ui/controls/pickers.html#DatePicker
 * 
 * 
 */
public class DatePickerFragment extends DialogFragment {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Getting the past date and having it be the default date shown:
		Calendar c = Calendar.getInstance(); // Making an instance of the calendar
		Bundle bundleWithPreviousDate = getArguments(); // This will contain the old date
		Integer month = null; // Will be the month
		Integer year = null; // Will be the year
		Integer day = null; // Will be the day
		
		if(bundleWithPreviousDate != null) {
			// Bundle that passes the date along:
			Bundle setDate = this.getArguments();
			Long currDate = setDate.getLong("setDate");
			c.setTimeInMillis(currDate);
			month = c.get(Calendar.MONTH) + 1; // Will be off by one if you don't add
											// (Android does 0-11, not 1-12 for months)
		} else {
			c = Calendar.getInstance();
			month = c.get(Calendar.MONTH);
		}
		year = c.get(Calendar.YEAR);
		day = c.get(Calendar.DAY_OF_MONTH);

		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(),
				(OnDateSetListener) getActivity(), year, month, day);
	}
}