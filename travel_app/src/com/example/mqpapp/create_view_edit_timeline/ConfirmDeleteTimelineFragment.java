package com.example.mqpapp.create_view_edit_timeline;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import com.example.mqpapp.BackupManager;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.R;

/**
 * This class is used to ask a user if they do in fact want to delete a
 * timeline.
 * 
 */
@SuppressLint("InflateParams")
public class ConfirmDeleteTimelineFragment extends DialogFragment {

	private static final String TAG = ConfirmDeleteTimelineFragment.class
			.getSimpleName();

	TextView list_item_uuid;

	// a manager for the database for getting documents and updating data
	CouchbaseManager manager;

	public ConfirmDeleteTimelineFragment(TextView uuid) {
		this.list_item_uuid = uuid;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog dialog;
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// get a manager for the database
		manager = CouchbaseManager.getInstance(this.getActivity()
				.getApplicationContext());
		// get document from the database w/ all the information for this entry

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		View V = inflater.inflate(R.layout.delete_timeline_dialog, null);
		builder.setView(V);

		builder.setMessage("Edit Item Text")
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Log.i(TAG, "uuid : "
										+ list_item_uuid.getText().toString());
								// update the database to contain the new data
								/*
								 * manager.update(CouchbaseManager.MEDIA_MESSAGE,
								 * edit_media_text_dialog.getText().toString(),
								 * list_item_uuid.getText().toString());
								 */
								// remove the timeline from the database
								manager.deleteTimeline(list_item_uuid.getText()
										.toString());
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// User cancelled the dialog

							}
						});

		// Create the AlertDialog object and return it
		dialog = builder.create();
		dialog.getWindow().setSoftInputMode(
				LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		return dialog;
	}

	/**
	 * When this fragment is stopped, we will make a backup of the current state
	 * of the database.
	 */
	@Override
	public void onStop() {
		super.onStop();
		Context context = getActivity().getApplicationContext();
		// Calling the "make backup" method to create a backup
		BackupManager.copyAndBackUpDatabase(context);
	}
}