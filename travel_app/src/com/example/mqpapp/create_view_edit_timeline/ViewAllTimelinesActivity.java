package com.example.mqpapp.create_view_edit_timeline;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryRow;
import com.example.mqpapp.BackupManager;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.R;
import com.example.mqpapp.main_activity_fragments.GoogleDriveWrongFileFragment;
import com.example.mqpapp.main_activity_fragments.InternetConnectionLostFragment;
import com.example.mqpapp.main_activity_fragments.MustConnectToInternetFragment;
import com.example.mqpapp.main_activity_fragments.WelcomeToWanderlogFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.OpenFileActivityBuilder;

/**
 * This class allows a user to view and edit a new timeline.
 * 
 */
public class ViewAllTimelinesActivity extends FragmentActivity implements
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener {
	// Used for remembering preferences
	private static final String MY_PREFS_NAME = "WanderLog";
	// A database manager for all the database needs
	private CouchbaseManager manager;
	// a list view to display the timelines in the database
	private ListView timelineListView;
	// names for the columns in the list view
	private String[] columnTags = new String[] { "timelineTitle", "uuid",
			"timelineDescription", "timelineLocation" };
	// every row in the list view will be a layout
	// get the ids of the text views in the layout for the rows
	// these text views will correspond to the columnTags
	private int[] columnIds = new int[] { R.id.list_item_title,
			R.id.list_item_uuid, R.id.list_item_description,
			R.id.list_item_location };
	// an array list to hold the rows to be displayed in the list view
	// the array list is an array of hash maps that have keys corresponding
	// to the column tags.
	private ArrayList<HashMap<String, String>> timelineListData;
	// an adapter to work with the timeline list view
	private SimpleAdapter timelineListAdapter;
	private OnItemClickListener exampleListener;

	// Used to connect to Google Play
	public static GoogleApiClient googleApiClient;
	// Used for Google Play Services authorization
	private static boolean mIsInAuth;
	// For logging, so you know you're downloading from Google Drive
	// and overwriting the database with this copy
	private static String googleDownloadDatabaseTag = "Google Download Database";
	// App preferences
	static SharedPreferences wanderLogPreferences;
	// Resetting to let user connect to Google Play
	static SharedPreferences.Editor wanderLogPreferencesEditor;
	// Checking if we have internet
	Boolean internetConnection;
	BroadcastReceiver checkInternetConnectionReceiver;

	// This is the menu bar in this activity
	private Menu menuBar;

	/**
	 * This method is called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// This is the XML for this activity
		setContentView(R.layout.view_all_timelines_activity);

		// Setting preferences and preference editor up
		wanderLogPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
		internetConnection = wanderLogPreferences.getBoolean(
				"internetConnection", false);
		wanderLogPreferencesEditor = getSharedPreferences(MY_PREFS_NAME,
				MODE_PRIVATE).edit();

		// This will acknowledge when the internet is disconnected:
		checkInternetConnectionReceiver = new BroadcastReceiver() {
			public void onReceive(Context context, Intent intent) {
				// True if internet is currently connected, else false.
				boolean internetConnectivity = !(intent.getBooleanExtra(
						ConnectivityManager.EXTRA_NO_CONNECTIVITY, false));

				// Marking that the internet connection is now false
				wanderLogPreferencesEditor.putBoolean("internetConnection",
						internetConnectivity);
				wanderLogPreferencesEditor.commit();

				Log.i("Internet is connected?",
						Boolean.toString(internetConnectivity));

			}
		};

		// Check if user has seen welcome message, if not, show it:
		checkIfUserHasSeenWelcomeMessage();

		// Checking if we have an internet connection (either Wifi or 3G):
		if (internetConnection) {
			// This is used for authentication and uploading to Google Drive
			googleApiClient = new GoogleApiClient.Builder(this)
					.addApi(Drive.API).addScope(Drive.SCOPE_FILE)
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this).build();
		}

		// get the instance of the database manager
		manager = CouchbaseManager.getInstance(this.getApplicationContext());

		// get a reference to the list view for population later
		timelineListView = (ListView) findViewById(R.id.allTimelineListView);

		// set up an adapter for managing the list
		setUpTimelineListAdapter();

		// set the list view to get populated from an adapter
		timelineListView.setAdapter(timelineListAdapter);

		// get all the data from the database
		readAndDisplayTimelineData();

		// create the listener to be called when a timeline it tapped
		exampleListener = new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View v,
					int position, long arg) {
				@SuppressWarnings("unchecked")
				// get the map representing the row that was tapped
				Map<String, String> row = (Map<String, String>) adapter
						.getItemAtPosition(position);

				// create an activity to view the timeline selected
				viewTimeline(row);
			}
		};

		// set up a listener for the list view
		timelineListView.setOnItemClickListener(exampleListener);
	}

	/**
	 * Decides what to do when a resulting activity returns
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// Referenced from:
		// https://developer.android.com/google/auth/api-client.html
		if (requestCode == BackupManager.getRequestResolveError()) {
			BackupManager.setmResolvingError(false);
			if (resultCode == RESULT_OK) {
				// Make sure the app is not already connected or attempting to
				// connect
				if (!googleApiClient.isConnecting()
						&& !googleApiClient.isConnected()) {
					googleApiClient.connect();
				}
			}
		}

		// check to see which activity returned
		else if (requestCode == Timeline.get_create_new_timeline_activity()) {
			// this is information for creating a new timeline

			// check to make sure the activity returned correctly
			if (resultCode == RESULT_OK) {

				// get all the timeline information necessary for insertion
				String title = data
						.getStringExtra(CouchbaseManager.TIMELINE_TITLE);
				String description = data
						.getStringExtra(CouchbaseManager.TIMELINE_DESCRIPTION);
				String location = data
						.getStringExtra(CouchbaseManager.TIMELINE_LOCATION);
				String startDate = data
						.getStringExtra(CouchbaseManager.TIMELINE_START_DATE);
				String endDate = data
						.getStringExtra(CouchbaseManager.TIMELINE_END_DATE);

				// create a new timeline in the database with this date
				manager.createTimeline(title, description, location, startDate,
						endDate);
			}
		}
		// If the user is downloading something from the database
		else if (requestCode == BackupManager.getRequestCodeDownloadDatabase()
				&& data != null) {

			// This method (seen below) will use the result
			// of this Google Drive intent to set up the new database
			try {
				setUpNewDatabaseWithGoogleDriveData(data);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (requestCode == BackupManager.getRequestCodeAuth()) {
			// This is used with connecting to Google Play Services and
			// connecting or not connecting
			mIsInAuth = false;

			// If they said they do want to connect to Google Play
			if (resultCode == RESULT_OK) {
				Log.i("result", "User chose to connect to Google Drive.");

				// Marking that the user once chose this.
				wanderLogPreferencesEditor.putBoolean(
						"choseToConnectToGoogleDrive", true);
				wanderLogPreferencesEditor.commit();

				// Getting the menu option for connecting to Google Drive
				MenuItem action_connect_to_google_drive = menuBar
						.findItem(R.id.action_connect_to_google_drive);
				// Hide this now
				action_connect_to_google_drive.setVisible(false);

				// Getting the menu option for downloading from Google Drive
				MenuItem action_google_drive_download = menuBar
						.findItem(R.id.action_google_drive_download);
				// We now want to see this
				action_google_drive_download.setVisible(true);

				// Getting the menu option for saving to Google Drive
				MenuItem action_google_drive_save = menuBar
						.findItem(R.id.action_google_drive_save);
				// We now want to see this
				action_google_drive_save.setVisible(true);
				Log.i("result", "Set visibilities of Google Drive menu items.");

				googleApiClient.connect(); // Lastly, we need to connect.
			} else if (resultCode == RESULT_CANCELED) {
				// Getting the menu option for connecting to Google Drive
				MenuItem action_connect_to_google_drive = menuBar
						.findItem(R.id.action_connect_to_google_drive);
				// Show this now
				action_connect_to_google_drive.setVisible(true);

				// Getting the menu option for downloading from Google Drive
				MenuItem action_google_drive_download = menuBar
						.findItem(R.id.action_google_drive_download);
				// We now want to hide this
				action_google_drive_download.setVisible(false);

				// Getting the menu option for saving to Google Drive
				MenuItem action_google_drive_save = menuBar
						.findItem(R.id.action_google_drive_save);
				// We now want to hide this
				action_google_drive_save.setVisible(false);

				// Marking that the user said no to connect to Google Drive
				wanderLogPreferencesEditor.putBoolean(
						"choseToConnectToGoogleDrive", false);
				wanderLogPreferencesEditor.commit();
			}
		}
	}

	/**
	 * Method used to overwrite old database file with new Google Drive one
	 * 
	 * @param data
	 *            is the data returned from the Google Drive activity
	 * @throws InterruptedException
	 */
	private void setUpNewDatabaseWithGoogleDriveData(Intent data)
			throws InterruptedException {

		Log.i(googleDownloadDatabaseTag, "Starting to overwrite database.");

		// Getting the drive ID for the file
		DriveId driveId = (DriveId) data
				.getParcelableExtra(OpenFileActivityBuilder.EXTRA_RESPONSE_DRIVE_ID);

		Log.i(googleDownloadDatabaseTag, "Got driveId: " + driveId.toString());

		// Getting the selected file
		final DriveFile googleDriveFile = Drive.DriveApi.getFile(
				googleApiClient, driveId);

		// Getting the Google Drive file's name and saving it into the
		// preferences (so it isn't lost)
		Thread dbGoogleDriveThread = new Thread(new Runnable() {
			public void run() {

				DriveResource.MetadataResult result = googleDriveFile
						.getMetadata(googleApiClient).await();
				Metadata metadata = result.getMetadata();
				// Putting title of file in preferences
				wanderLogPreferencesEditor.putString(
						"googleDriveDatabaseFileName", metadata.getTitle());
				wanderLogPreferencesEditor.commit();
			}
		});
		// Running a thread to get the name, making other threads wait on this
		// one
		dbGoogleDriveThread.start();
		dbGoogleDriveThread.join();

		// Getting preferences
		SharedPreferences wanderLogPreferences = getSharedPreferences(
				MY_PREFS_NAME, MODE_PRIVATE);

		// This is the name of the selected file
		String fileName = wanderLogPreferences.getString(
				"googleDriveDatabaseFileName", "");

		// Checking if file name ends with ".cblite," meaning it's of the
		// correct type:
		if (fileName.endsWith(".cblite")) {

			// Let the user know that their timelines are loading
			Toast.makeText(getApplicationContext(), "Now Loading...",
					Toast.LENGTH_LONG).show();

			Log.i(googleDownloadDatabaseTag, "Opening DriveFile...");

			googleDriveFile.open(googleApiClient, DriveFile.MODE_READ_ONLY,
					null).setResultCallback(
					new ResultCallback<DriveContentsResult>() {
						@Override
						public void onResult(DriveContentsResult result) {
							if (!result.getStatus().isSuccess()) {
								Log.e("ViewAllTimelinesActivity",
										"Google Drive Error");
								return;
							}
							// Getting the contents of the file
							DriveContents driveContents = result
									.getDriveContents();

							Log.i(googleDownloadDatabaseTag, "Got contents");

							// Turning the file into a usable format
							ParcelFileDescriptor parcelFileDescriptor = driveContents
									.getParcelFileDescriptor();

							Log.i(googleDownloadDatabaseTag,
									"got parcelfiledescriptor");

							final FileInputStream fileInputStream = new FileInputStream(
									parcelFileDescriptor.getFileDescriptor());

							Log.i(googleDownloadDatabaseTag,
									"Got fileInputStream");

							Thread dbGoogleDriveThread = new Thread(
									new Runnable() {
										public void run() {
											// Calling method to overwrite
											// current
											// database with
											// version from Google Drive
											BackupManager
													.overwriteDatabaseWithGoogleDriveFile(
															getApplicationContext(),
															fileInputStream);
										}
									});
							dbGoogleDriveThread.start(); // Running this thread
															// to set up Google
															// Drive copy of
															// database
							Boolean restart = false; // Used to check if
														// activity has been
														// restarted
							while (dbGoogleDriveThread.isAlive()) {
								// If the thread died:
								if (!dbGoogleDriveThread.isAlive()) {
									// Restart the activity
									restart = true;
									Intent intent = getIntent();
									finish();
									startActivity(intent);
								}
							}
							// In case thread dies in the while loop check:
							if (!restart) {
								Intent intent = getIntent();
								finish();
								startActivity(intent);
							}

						}
					});

		} else {
			GoogleDriveWrongFileFragment wrongFileDialog = new GoogleDriveWrongFileFragment();
			wrongFileDialog.show(getSupportFragmentManager(),
					"wrong_file_dialog");
		}

	}

	/**
	 * This method will initialize a list adapter for the list of timelines and
	 * will initialize the list used by the adapter to populate the list view.
	 */
	private void setUpTimelineListAdapter() {

		// set up an array list to pass to the adapter for
		// list view population
		timelineListData = new ArrayList<HashMap<String, String>>();

		// create a new SimpleAdapter for populating the list view
		timelineListAdapter = new SimpleAdapter(this, timelineListData,
				R.layout.list_item_timeline_and_entry_delete, columnTags,
				columnIds);
	}

	/**
	 * This method will create a live query on the timeline view in the
	 * database. The live query will auto update whenever any of the data in the
	 * database gets changed in some way. Any updates cause a call to the
	 * changed method defined within the change listener.
	 */
	private void readAndDisplayTimelineData() {
		// set up the query
		Query query = manager.getDatabase()
				.getView(CouchbaseManager.VIEW_TIMELINE).createQuery();

		query.setMapOnly(true);

		// make the query a live query
		final LiveQuery liveQuery = query.toLiveQuery();

		liveQuery.addChangeListener(new LiveQuery.ChangeListener() {

			private Object changedLiveQuery = liveQuery;

			@Override
			public void changed(final LiveQuery.ChangeEvent event) {
				if (event.getSource().equals(this.changedLiveQuery)) {
					runOnUiThread(new Runnable() {
						@SuppressWarnings("unchecked")
						public void run() {

							// clear out the existing list for repopulation
							timelineListData.clear();

							// get an iterator over the items to insert into the
							// list view
							Iterator<QueryRow> iter = event.getRows();

							// a temporary value for the current row while
							// looping
							QueryRow currentRow = null;

							// add everything in this query result to the list
							// view adapter
							while (iter.hasNext()) {

								// create a map for this row
								HashMap<String, String> tempMap = new HashMap<String, String>();

								// temporarily store the current row
								currentRow = iter.next();

								// populate the map for this row
								// the key is the title of the timeline, so
								// display that
								tempMap.put(
										columnTags[0],
										(String) ((Map<String, Object>) currentRow
												.getValue())
												.get(CouchbaseManager.TIMELINE_TITLE));
								// a uuid for referencing this document in the
								// future
								tempMap.put(columnTags[1],
										currentRow.getDocumentId());
								// get description
								tempMap.put(
										columnTags[2],
										(String) ((Map<String, Object>) currentRow
												.getValue())
												.get(CouchbaseManager.TIMELINE_DESCRIPTION));

								// get location
								tempMap.put(
										columnTags[3],
										(String) ((Map<String, Object>) currentRow
												.getValue())
												.get(CouchbaseManager.TIMELINE_LOCATION));
								timelineListData.add(tempMap);
							}

							// let the list view know that the data it is
							// displaying got changed,
							// so it can update the list view
							timelineListAdapter.notifyDataSetChanged();
						}
					});

				}

			}
		});

		liveQuery.start();
	}

	/**
	 * This method will start another activity for viewing a timeline.
	 * 
	 * @param row
	 *            a database row with information about the timeline to be
	 *            viewed.
	 */
	private void viewTimeline(Map<String, String> row) {
		final Intent intent = new Intent(this,
				ViewAndEditTimelineActivity.class);

		intent.putExtra("uuid", row.get(columnTags[1]));

		startActivityForResult(intent, Timeline.get_view_timeline());
	}

	/**
	 * This method will add the given view to the list that will be deleted when
	 * edit mode is saved. This method is called in the onClick property for the
	 * delete entry button in the XML files for each list item
	 * 
	 * @param view
	 *            the view passed to this function is the view of the delete
	 *            button attached to a entry list item in editmode
	 */
	public void deleteRow(View v) {
		v.setBackgroundResource(R.drawable.deletex_clicked);
		// get the view of the list item from the delete button
		View listItem = ((View) v.getParent());
		// find the UUID of the current list item
		TextView listItemUuid = (TextView) listItem
				.findViewById(R.id.list_item_uuid);

		// TODO : can someone please add a confirmation dialogue
		ConfirmDeleteTimelineFragment deleteTimelineFragment = new ConfirmDeleteTimelineFragment(
				listItemUuid);
		deleteTimelineFragment.show(this.getSupportFragmentManager(),
				"Delete Timeline Fragment");

	}

	/**
	 * Display action bar buttons
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		getMenuInflater().inflate(R.menu.timelinelist, menu);
		// Getting the menu bar
		menuBar = menu;

		// Getting preferences
		SharedPreferences wanderLogPreferences = getSharedPreferences(
				MY_PREFS_NAME, MODE_PRIVATE);

		// Seeing if the user once said "yes" to syncing with Google Drive
		boolean choseToConnectToGoogleDrive = wanderLogPreferences.getBoolean(
				"choseToConnectToGoogleDrive", false);

		// If the user has connected to Google Drive in the past, show these
		// menu items
		if (choseToConnectToGoogleDrive) {
			// Getting the menu option for connecting to Google Drive
			MenuItem action_connect_to_google_drive = menuBar
					.findItem(R.id.action_connect_to_google_drive);
			// Hide this now
			action_connect_to_google_drive.setVisible(false);

			// Getting the menu option for downloading from Google Drive
			MenuItem action_google_drive_download = menuBar
					.findItem(R.id.action_google_drive_download);
			// We now want to see this
			action_google_drive_download.setVisible(true);

			// Getting the menu option for saving to Google Drive
			MenuItem action_google_drive_save = menuBar
					.findItem(R.id.action_google_drive_save);
			// We now want to see this
			action_google_drive_save.setVisible(true);
			Log.i("result", "Set visibilities of Google Drive menu items.");
		}

		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * This method is used when a user clicks a particular menu item in this
	 * activity.
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		SharedPreferences wanderLogPreferences = getSharedPreferences(
				MY_PREFS_NAME, MODE_PRIVATE);
		// Checking if internet connection was lost
		Boolean internetConnection = wanderLogPreferences.getBoolean(
				"internetConnection", false);

		final Intent createTimelineIntent = new Intent(this,
				CreateNewTimelineActivity.class);

		switch (item.getItemId()) {
		case R.id.add_timeline:
			/*
			 * When creating a new timeline:
			 */
			startActivityForResult(createTimelineIntent,
					Timeline.get_create_new_timeline_activity());
			return true;
		case R.id.help:
			Timeline.showHelpScreen(R.string.view_all_timelines_help,
					getApplicationContext());

			return true;
		case R.id.action_restore:
			/*
			 * When a user wants to restore a previous set of timelines and
			 * entries
			 */
			// Restoring the previous copy of the database.
			BackupManager.restorePreviousDatabase(getApplicationContext());

			// Lastly, we need to restart this activity to show changes
			Intent intent = getIntent();
			finish();
			startActivity(intent);

			return true;

		case R.id.action_connect_to_google_drive:
			/*
			 * This is for connecting to Google Drive after user initally
			 * refuses to do so
			 */

			// Checking if we have an internet connection (either Wifi or 3G):
			// Also, we will display this if the user has said "no" to
			// connecting to Google Drive before
			if (internetConnection) {
				// This is used for authentication and uploading to Google Drive
				googleApiClient = new GoogleApiClient.Builder(this)
						.addApi(Drive.API).addScope(Drive.SCOPE_FILE)
						.addConnectionCallbacks(this)
						.addOnConnectionFailedListener(this).build();

				// Marking that the user once chose to connect to Google Drive
				wanderLogPreferencesEditor.putBoolean(
						"choseToConnectToGoogleDrive", true);
				wanderLogPreferencesEditor.commit();

				// This only creates the google API client object, it doesn't
				// connect. Now, we need to restart the activity to actually
				// connect:
				intent = getIntent();
				finish();
				startActivity(intent);
			} else {
				MustConnectToInternetFragment mustConnectToInternetDialog = new MustConnectToInternetFragment();
				mustConnectToInternetDialog.show(getSupportFragmentManager(),
						"must_connect_to_internet_dialog");
			}

			return true;
		case R.id.action_google_drive_save:
			/*
			 * This is for saving timelines to Google Drive
			 */
			// If they originally didn't want to connect to google play,
			// reconnect.

			// Checking if we have an internet connection (either Wifi or 3G):
			if (internetConnection) {
				// Calling method to save to google drive
				BackupManager.saveToGoogleDrive(getApplicationContext(),
						googleApiClient, this, manager);
				Log.i("!!!", "!!! Did connect");
			} else {
				Log.i("!!!", "!!! Did not connect");
				// If they just can't connect to the internet
				MustConnectToInternetFragment mustConnectToInternetDialog = new MustConnectToInternetFragment();
				mustConnectToInternetDialog.show(getSupportFragmentManager(),
						"must_connect_to_internet_dialog");
			}

			return true;
		case R.id.action_google_drive_download:
			/*
			 * This is for download timelines from Google Drive
			 */

			// Checking if we have an internet connection (either Wifi or 3G):
			if (internetConnection) {
				// Calling method to download from Google Drive
				BackupManager.downloadFromGoogleDrive(googleApiClient, this);
				Log.i("!!!", "!!! Did connect");
			} else {
				Log.i("!!!", "!!! Did not connect");
				// If they just can't connect to the internet
				MustConnectToInternetFragment mustConnectToInternetDialog = new MustConnectToInternetFragment();
				mustConnectToInternetDialog.show(getSupportFragmentManager(),
						"must_connect_to_internet_dialog");
			}

			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Overriding the method that is called when a screen is paused.
	 */
	@Override
	public void onPause() {
		super.onPause();
		try {
			unregisterReceiver(checkInternetConnectionReceiver);
		} catch (IllegalArgumentException e) {
			Log.e("ViewAllTimelinesActivity", "Cannot unregister receiver: "
					+ e.toString());
		}
		timelineListView.setOnItemClickListener(null);
	}

	/**
	 * Overriding the method that is called when a screen is resumed.
	 */
	@Override
	public void onResume() {
		super.onResume();

		Log.i("register", "registering receiver");
		registerReceiver(checkInternetConnectionReceiver, new IntentFilter(
				"android.net.conn.CONNECTIVITY_CHANGE"));
		Log.i("register", "registered receiver");

		if (timelineListView != null) {
			timelineListView.setOnItemClickListener(exampleListener);
		}
	}

	/**
	 * This method connects the google API client when the activity opens.
	 * Source: https://developer.android.com/google/auth/api-client.html
	 */
	@Override
	protected void onStart() {
		// Did the user at one time say yes to Google Drive? If so, just connect
		// them. This must be here because if it isn't, every time this activity
		// is
		// opened, it will try to connect the user.
		Boolean choseToConnectToGoogleDrive = wanderLogPreferences.getBoolean(
				"choseToConnectToGoogleDrive", false);

		// Checking if we have an internet connection (either Wifi or 3G):
		if (internetConnection && choseToConnectToGoogleDrive) {
			// This is used for authentication and uploading to Google Drive
			googleApiClient = new GoogleApiClient.Builder(this)
					.addApi(Drive.API).addScope(Drive.SCOPE_FILE)
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this).build();
			// This must be in onStart because it can't be called more than
			// once.
			googleApiClient.connect();
		}
		super.onStart();
	}

	/**
	 * When this activity is stopped, we will make a backup of the current state
	 * of the database. Source:
	 * https://developer.android.com/google/auth/api-client.html
	 */
	@Override
	public void onStop() {
		super.onStop();
		Context context = getApplicationContext();
		BackupManager.copyAndBackUpDatabase(context);
	}

	/**
	 * This method is used when the connection to google drive fails. It
	 * attempts to fix the error. Source:
	 * https://developer.android.com/google/auth/api-client.html
	 */
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!mIsInAuth) {
			/*
			 * If this can be fixed and user hasn't already said no to
			 * connecting to Google Play:
			 */
			if (result.hasResolution()) {
				Log.i("ViewAllTimelinesActivity",
						" Trying to connect to Google Play again");
				try {
					mIsInAuth = true;
					result.startResolutionForResult(this,
							BackupManager.getRequestCodeAuth());
				} catch (IntentSender.SendIntentException e) {
					finish();
				}
			} else {
				// No resolution for this issue
				Log.i("ViewAllTimelinesActivity",
						"Not trying to connect to Google Play again");
				BackupManager.setmResolvingError(true);
			}
		} else if (BackupManager.getmResolvingError()) {
			Log.i("ViewAllTimelinesActivity",
					"Already attempting to resolve an error");
			return;
		} else if (result.hasResolution()) {
			try {
				BackupManager.setmResolvingError(true);
				result.startResolutionForResult(this,
						BackupManager.getRequestResolveError());
			} catch (SendIntentException e) {
				// There was an error with the resolution intent. Try again.
				googleApiClient.connect();
			}
		} else {
			// Show dialog using GooglePlayServicesUtil.getErrorDialog()
			showErrorDialog(result.getErrorCode());
			BackupManager.setmResolvingError(true);
		}
	}

	/**
	 * This method creates a dialog for an error message Source:
	 * https://developer.android.com/google/auth/api-client.html
	 */
	private void showErrorDialog(int errorCode) {
		// Create a fragment for the error dialog
		ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
		// Pass the error that should be displayed
		Bundle args = new Bundle();
		args.putInt(BackupManager.getDialogError(), errorCode);
		dialogFragment.setArguments(args);
		dialogFragment.show(getSupportFragmentManager(), "errordialog");
	}

	/**
	 * This method is called from ErrorDialogFragment when the dialog is
	 * dismissed. Source:
	 * https://developer.android.com/google/auth/api-client.html
	 * */
	public void onDialogDismissed() {
		BackupManager.setmResolvingError(false);
	}

	/**
	 * A fragment to display an error dialog Source:
	 * https://developer.android.com/google/auth/api-client.html
	 * */
	public static class ErrorDialogFragment extends DialogFragment {
		public ErrorDialogFragment() {
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Get the error code and retrieve the appropriate dialog
			int errorCode = this.getArguments().getInt(
					BackupManager.getDialogError());
			return GooglePlayServicesUtil.getErrorDialog(errorCode,
					this.getActivity(), BackupManager.getRequestResolveError());
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			((ViewAllTimelinesActivity) getActivity()).onDialogDismissed();
		}
	}

	/**
	 * Source: https://developer.android.com/google/auth/api-client.html
	 */
	@Override
	public void onConnectionSuspended(int cause) {
		InternetConnectionLostFragment internetConnectionLostDialog = new InternetConnectionLostFragment();
		internetConnectionLostDialog.show(getSupportFragmentManager(),
				"internet_connection_lost_dialog");

	}

	/**
	 * Source: https://developer.android.com/google/auth/api-client.html
	 */
	@Override
	public void onConnected(Bundle connectionHint) {
		// Connected to Google Play services!
		// Nothing automatically happens when connected, so nothing needs to go
		// here yet.

	}

	/**
	 * This method checks if the user has seen the welcome message yet. If not,
	 * the message is displayed, and then we mark that the message has been
	 * seen. If not, the message isn't displayed.
	 */
	public void checkIfUserHasSeenWelcomeMessage() {
		// Checking if we've seen the welcome message before`
		boolean haveSeenWelcomeMessage = wanderLogPreferences.getBoolean(
				"hasSeenWelcomeMessage", false);
		// If we haven't seen the welcome message, show it:
		if (!haveSeenWelcomeMessage) {
			// Welcome message is displayed, only once
			WelcomeToWanderlogFragment welcomeToWanderlogFragment = new WelcomeToWanderlogFragment();
			welcomeToWanderlogFragment.show(getSupportFragmentManager(),
					"welcome_to_wanderlog_fragment");
			// Marking that we've seen this
			wanderLogPreferencesEditor
					.putBoolean("hasSeenWelcomeMessage", true);
			wanderLogPreferencesEditor.commit();
		}
	}

}
