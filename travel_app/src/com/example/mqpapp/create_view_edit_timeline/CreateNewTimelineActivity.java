package com.example.mqpapp.create_view_edit_timeline;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mqpapp.BackupManager;
import com.example.mqpapp.CancelEditsFragment;
import com.example.mqpapp.CancelEditsFragment.DiscardDialogListener;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.DatePickerFragment;
import com.example.mqpapp.R;

/**
 * This class allows a user to create a new timeline.
 * 
 */
@SuppressLint("SimpleDateFormat")
public class CreateNewTimelineActivity extends FragmentActivity implements
		OnDateSetListener, DiscardDialogListener {
	Timeline timeline = new Timeline();
	TextView noTitleError; // Error that appears when a user tries to save
							// without adding a title
	TextView invalidStartDateError; // Error that shows if start date is after
									// end date
	TextView invalidEndDateError; // Error that shows if end date is before
									// start date

	/**
	 * This method is called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// This is the XML for this activity
		setContentView(R.layout.create_new_timeline_activity);

		// Putting into variables all of the textviews and buttons that exist in
		// this activity
		// in DISPLAY mode (when data already exists)
		timeline.title_editmode = (EditText) findViewById(R.id.timeline_title_new);
		timeline.description_editmode = (EditText) findViewById(R.id.timeline_description_new);
		timeline.start_date_editmode = (TextView) findViewById(R.id.timeline_start_date_new);
		timeline.end_date_editmode = (TextView) findViewById(R.id.timeline_end_date_new);
		timeline.location_editmode = (EditText) findViewById(R.id.timeline_location_new);
		noTitleError = (TextView) findViewById(R.id.no_title_error);
		invalidStartDateError = (TextView) findViewById(R.id.invalid_start_date_error);
		invalidEndDateError = (TextView) findViewById(R.id.invalid_end_date_error);

		// Setting default date in the entry, today's date
		// First, create a new date object, set it to today's date
		DateFormat dateFormat = new SimpleDateFormat("M-d-yyyy");
		Date date = new Date();
		// Now set the text in the date box to today's date
		timeline.start_date_editmode.setText(dateFormat.format(date));
		timeline.end_date_editmode.setText(dateFormat.format(date));

		// Checking to see if start or end date button was clicked:

		// Start button:
		ImageView buttonShowStartDatePicker = (ImageView) findViewById(R.id.buttonShowStartDatePicker);

		// Setting the behavior when the start date button (and when the start
		// date itself) is clicked:
		buttonShowStartDatePicker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Timeline.set_start_date_clicked();// Flag start date as clicked
				Timeline.reset_end_date_clicked();
				showDatePickerDialog(view);
			}
		});

		timeline.start_date_editmode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Timeline.set_start_date_clicked();// Flag start date as clicked
				Timeline.reset_end_date_clicked();
				showDatePickerDialog(view);
			}
		});

		// End button:
		ImageView buttonShowEndDatePicker = (ImageView) findViewById(R.id.buttonShowEndDatePicker);

		// Setting the behavior when the end date button (and when the end
		// date itself) is clicked:
		buttonShowEndDatePicker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Timeline.set_end_date_clicked(); // Flag end date as clicked
				Timeline.reset_start_date_clicked();
				showDatePickerDialog(view);
			}
		});

		timeline.end_date_editmode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Timeline.set_end_date_clicked();// Flag end date as clicked
				Timeline.reset_start_date_clicked();
				showDatePickerDialog(view);
			}
		});
	}

	/**
	 * This method will return the new timeline data in an intent.
	 */
	private void returnNewTimelineData() {

		// create a new intent to return
		Intent insertionIntent = new Intent();

		// add all the necessary information to the intent
		insertionIntent.putExtra(CouchbaseManager.TIMELINE_TITLE,
				timeline.title_editmode.getText().toString());
		insertionIntent.putExtra(CouchbaseManager.TIMELINE_DESCRIPTION,
				timeline.description_editmode.getText().toString());
		insertionIntent.putExtra(CouchbaseManager.TIMELINE_LOCATION,
				timeline.location_editmode.getText().toString());
		insertionIntent.putExtra(CouchbaseManager.TIMELINE_START_DATE,
				timeline.start_date_editmode.getText().toString());
		insertionIntent.putExtra(CouchbaseManager.TIMELINE_END_DATE,
				timeline.end_date_editmode.getText().toString());

		// set the result of this intent being returned to the calling activity
		setResult(RESULT_OK, insertionIntent);

		// close this activity and pass this intent onto the calling activity
		finish();
	}

	/**
	 * This gets the datepicker to display.
	 * 
	 * @param v
	 *            : The view
	 */
	// This will show the datepicker to the user
	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}

	/**
	 * This is the method that sets the textview displaying the date to what was
	 * chosen by the user with the datepicker.
	 * 
	 * @param view
	 *            : The datepicker
	 * @param year
	 *            : Chosen year (by user)
	 * @param month
	 *            : Chosen month (by user)
	 * @param day
	 *            : Chosen day (by user)
	 */
	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		if ((Timeline.get_start_date_clicked() == 1)
				&& (Timeline.get_end_date_clicked() == 0)) {
			// If the start date button was clicked:

			// Getting the current end date
			// Item at [0] is the month, [1] is the day, and [2] is the year
			Integer[] endDateMonthDayYear = Timeline
					.getMonthDayYearFromString(timeline.end_date_editmode
							.getText().toString());

			// Check if the start date is before the end date
			boolean areDatesInCorrectOrder = Timeline
					.checkIfStartDateIsBeforeEndDate(month, day, year,
							endDateMonthDayYear[0] - 1, endDateMonthDayYear[1],
							endDateMonthDayYear[2]);

			if (!areDatesInCorrectOrder) {
				invalidStartDateError.setVisibility(View.VISIBLE);
			} else {
				// Hide error messages:
				invalidStartDateError.setVisibility(View.INVISIBLE);
				invalidEndDateError.setVisibility(View.INVISIBLE);
				// Updating the text that contains the date
				((TextView) findViewById(R.id.timeline_start_date_new))
						.setText((month + 1) + "-" + day + "-" + year);
			}

			Timeline.reset_start_date_clicked(); // Set back to zero

		} else if ((Timeline.get_end_date_clicked() == 1)
				&& (Timeline.get_start_date_clicked() == 0)) {
			// This means end date button was clicked:

			// Getting the current start date
			// Item at [0] is the month, [1] is the day, and [2] is the year
			Integer[] startDateMonthDayYear = Timeline
					.getMonthDayYearFromString(timeline.start_date_editmode
							.getText().toString());
			// Check if the start date is before the end date
			boolean areDatesInCorrectOrder = Timeline
					.checkIfStartDateIsBeforeEndDate(startDateMonthDayYear[0] - 1,
							startDateMonthDayYear[1], startDateMonthDayYear[2], month, day,
							year);

			if (!areDatesInCorrectOrder) {
				invalidEndDateError.setVisibility(View.VISIBLE);
			} else {
				// Hide error messages:
				invalidEndDateError.setVisibility(View.INVISIBLE);
				invalidStartDateError.setVisibility(View.INVISIBLE);
				// You may update the date
				// Updating the text that contains the date
				((TextView) findViewById(R.id.timeline_end_date_new))
						.setText((month + 1) + "-" + day + "-" + year);
			}
			Timeline.reset_end_date_clicked(); // Set back to zero

		} else {
			// This is bad, means there's an error:
			Log.e("CreateNewTimelineActivity",
					"Neither date was clicked; this should not have been triggered.");
		}
	}

	@Override
	public void onDiscardOkDialog() {
		this.finish();

	}

	/**
	 * Display action bar buttons
	 * 
	 * @param menu
	 *            is the menu object being used
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		getMenuInflater().inflate(R.menu.create_timeline, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * When a menu item is selected, this will handle those clicks.
	 * 
	 * @param item
	 *            is the menu item that is pressed
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.cancel_edit_timeline:
			CancelEditsFragment cancelDialog = new CancelEditsFragment();
			cancelDialog.show(getSupportFragmentManager(), "cancel_dialog");
			return true;
		case R.id.save_new_timeline:
			if (timeline.title_editmode.getText().toString().equals("")) {
				noTitleError.setVisibility(View.VISIBLE);
			} else {
				returnNewTimelineData();
			}
			return true;
		case R.id.help:
			Timeline.showHelpScreen(R.string.new_timeline_help,
					getApplicationContext());
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * When this activity is stopped, we will make a backup of the current state
	 * of the database.
	 */
	@Override
	public void onStop() {
		super.onStop();
		Context context = getApplicationContext();
		// Calling the "make backup" method to create a backup
		BackupManager.copyAndBackUpDatabase(context);
	}

}
