package com.example.mqpapp.create_view_edit_timeline;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mqpapp.HelpScreenActivity;

@SuppressLint("SimpleDateFormat")
public class Timeline {
	// integer values designating which activity is called so that
	// when the activities return, the resulting intents can
	// distinguish amongst one another
	private static int CREATE_NEW_TIMELINE_ACTIVITY = 0;

	// integer values designating which activity is called so that
	// when the activities return, the resulting intents can
	// distinguish amongst one another
	private static int CREATE_NEW_ENTRY_ACTIVITY = 1;

	// Handles when an image is returned from the gallery:
	private static int RESULT_LOAD_IMAGE = 2;

	// Identifiers for viewing entries and activities
	private static int VIEW_ENTRY_ACTIVITY = 3;
	private static int VIEW_TIMELINE = 4;

	// How to tell if someone clicked start or end date
	// Default these both to zero:
	private static int START_DATE_CLICKED = 0;
	private static int END_DATE_CLICKED = 0;

	// Viewing mode
	TextView title;
	TextView description;
	TextView start_date;
	TextView end_date;
	TextView location;
	// edit mode
	EditText title_editmode;
	EditText description_editmode;
	TextView start_date_editmode;
	TextView end_date_editmode;
	EditText location_editmode;

	// Menu items for timelines
	MenuItem edit;
	MenuItem cancel;
	MenuItem save;
	MenuItem add;

	/**
	 * Setter for START_DATE_CLICKED. This sets it to true.
	 * 
	 * @param value
	 */
	public static void set_start_date_clicked() {
		START_DATE_CLICKED = 1;
	}

	/**
	 * Setter for START_DATE_CLICKED. This sets it to false.
	 * 
	 * @param value
	 */
	public static void reset_start_date_clicked() {
		START_DATE_CLICKED = 0;
	}

	/**
	 * Setter for END_DATE_CLICKED. This sets it to true.
	 * 
	 * @param value
	 */
	public static void set_end_date_clicked() {
		END_DATE_CLICKED = 1;
	}

	/**
	 * Setter for END_DATE_CLICKED. This sets it to false.
	 * 
	 * @param value
	 */
	public static void reset_end_date_clicked() {
		END_DATE_CLICKED = 0;
	}

	/**
	 * Getter for START_DATE_CLICKED.
	 * 
	 * @return
	 */
	public static int get_start_date_clicked() {
		return START_DATE_CLICKED;
	}

	/**
	 * Getter for END_DATE_CLICKED.
	 * 
	 * @return
	 */
	public static int get_end_date_clicked() {
		return END_DATE_CLICKED;
	}

	/**
	 * Getter for CREATE_NEW_TIMELINE_ACTIVITY
	 */
	public static int get_create_new_timeline_activity() {
		return CREATE_NEW_TIMELINE_ACTIVITY;
	}

	/**
	 * Getter for CREATE_NEW_ENTRY_ACTIVITY
	 */
	public static int get_create_new_entry_activity() {
		return CREATE_NEW_ENTRY_ACTIVITY;
	}

	/**
	 * Getter for RESULT_LOAD_IMAGE
	 */
	public static int get_result_load_image() {
		return RESULT_LOAD_IMAGE;
	}

	/**
	 * Getter for VIEW_ENTRY_ACTIVITY
	 */
	public static int get_view_entry_activity() {
		return VIEW_ENTRY_ACTIVITY;
	}

	/**
	 * Getter for VIEW_TIMELINE
	 */
	public static int get_view_timeline() {
		return VIEW_TIMELINE;
	}

	/**
	 * This method will take a date in MM-dd-yyyy format and turn it into an
	 * array with the month in the first slot, date in the second slot, and the
	 * year in the third slot.
	 * 
	 * @param dateAsString
	 *            is the date in the format of a "MM-dd-yyyy" string
	 * @return integer array with the date pieces
	 */
	public static Integer[] getMonthDayYearFromString(String dateAsString) {
		Integer[] datePieces = new Integer[3]; // This will contain the matches
		// Regex to get a date in the format MM-dd-yyyy
		Matcher m = Pattern
				.compile(
						"(0?[1-9]|1[012])[-](0?[1-9]|[12][0-9]|3[01])[-]([0-9][0-9][0-9][0-9])")
				.matcher(dateAsString);
		// Loop through regex results
		while (m.find()) {
			// Put each group item into a different slot in this integer array
			datePieces[0] = Integer.parseInt(m.group(1)); // Month
			datePieces[1] = Integer.parseInt(m.group(2)); // Day
			datePieces[2] = Integer.parseInt(m.group(3)); // Year
		}
		return datePieces;
	}

	/**
	 * This method checks if the start date chosen for a timeline is before the
	 * end date
	 * 
	 * @param startMonth
	 *            starting date month
	 * @param startDay
	 *            starting date day
	 * @param startYear
	 *            starting date year
	 * @param endMonth
	 *            ending date month
	 * @param endDay
	 *            ending date day
	 * @param endYear
	 *            ending date year
	 * @return true if start is before end, else false.
	 */
	public static boolean checkIfStartDateIsBeforeEndDate(int startMonth,
			int startDay, int startYear, int endMonth, int endDay, int endYear) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		// Making our start and end date variables
		Date startDate = new Date();
		Date endDate = new Date();
		// Getting the start date in a date object format
		try {
			startDate = sdf
					.parse(startMonth + "-" + startDay + "-" + startYear);
		} catch (ParseException e) {
			Log.e("CreateNewTimelineActivity",
					"Cannot parse start date: " + e.toString());
		}
		// Getting the end date in a date object format
		try {
			endDate = sdf.parse(endMonth + "-" + endDay + "-" + endYear);
		} catch (ParseException e) {
			Log.e("CreateNewTimelineActivity",
					"Cannot parse end date: " + e.toString());
		}
		// Now, check if start date is before or after end date,
		// or if they're the same date. Either is valid and returns true:
		if (endDate.after(startDate) || endDate.equals(startDate)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This method brings up the help screen for a user.
	 * @param helpString is the ID for the help string for this specific activity
	 * @param context is the application's context
	 */
	public static void showHelpScreen(int helpString, Context context) {
		String helpText = context.getString(helpString);
		// create an intent for the full screen image activity
		Intent helpScreenIntent = new Intent(context,
				HelpScreenActivity.class);
		// pass the image uri to the new activity
		helpScreenIntent.putExtra("HELP_TEXT", helpText);
		
		helpScreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// start the new activity
		context.startActivity(helpScreenIntent);
	}
}
