package com.example.mqpapp.create_view_edit_timeline;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryRow;
import com.example.mqpapp.CancelEditsFragment;
import com.example.mqpapp.CancelEditsFragment.DiscardDialogListener;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.DatePickerFragment;
import com.example.mqpapp.R;
import com.example.mqpapp.create_view_edit_entry.CreateNewEntryActivity;
import com.example.mqpapp.create_view_edit_entry.ViewAndEditEntryActivity;

/**
 * This class allows a user to view and edit a new timeline.
 */
public class ViewAndEditTimelineActivity extends FragmentActivity implements
		OnDateSetListener, DiscardDialogListener {
	Timeline timeline = new Timeline();
	// A database manager for all the database needs
	private CouchbaseManager manager;
	// a list view to display the entries in the database
	private ListView entryListView;
	private ListView entryListViewEditmode;
	// a linked list of entry uuid's to delete when the database is updated
	// after the changes are saved
	private LinkedList<String> entriesToDelete;
	// names for the columns in the list view
	private String[] columnTags = new String[] { "entryTitle", "entryUuid",
			"entryDescription", "entryDate", "entryLoaction" };
	// every row in the list view will be a layout
	// get the ids of the text views in the layout for the rows
	// these text views will correspond to the columnTags
	int[] columnIds = new int[] { R.id.list_item_title, R.id.list_item_uuid,
			R.id.list_item_description, R.id.list_item_date,
			R.id.list_item_location };
	// an array list to hold the rows to be displayed in the list view
	// the array list is an array of hash maps that have keys corresponding
	// to the column tags.
	private ArrayList<HashMap<String, String>> entryListData;
	// an adapter to work with the entry list view
	private SimpleAdapter entryListAdapter;
	// an adapter to work with the list of timelines in editmode
	private SimpleAdapter entryListAdapterEditmode;
	// a live query that monitors the database for changes and updates the list
	// view when a change has been made.
	private LiveQuery liveQuery;
	// a unique identifier for the timeline being displayed / edited
	String timelineUuid;
	// Used for error checking with start and end dates
	TextView invalidStartDateError;
	TextView invalidEndDateError;

	/**
	 * This method is called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// This is the XML for this activity
		setContentView(R.layout.view_and_edit_timeline_activity);

		// ViewSwitcher, used for display mode vs. edit mode
		// TODO: Remove? Is this unneeded?:
		// final ViewSwitcher viewSwitcher = (ViewSwitcher)
		// findViewById(R.id.viewSwitcher);

		// -------- Section: Display mode

		// Getting all of the textviews and buttons that exist in this activity
		// in DISPLAY mode
		// (when data already exists)
		timeline.title = (TextView) findViewById(R.id.timeline_title);
		timeline.description = (TextView) findViewById(R.id.timeline_description);
		timeline.start_date = (TextView) findViewById(R.id.timeline_start_date);
		timeline.end_date = (TextView) findViewById(R.id.timeline_end_date);
		timeline.location = (TextView) findViewById(R.id.timeline_location);
		invalidStartDateError = (TextView) findViewById(R.id.invalid_start_date_error);
		invalidEndDateError = (TextView) findViewById(R.id.invalid_end_date_error);

		// get the intent that was used to call this activity
		Intent callingIntent = getIntent();

		// get the uuid of the timeline to display from the intent
		timelineUuid = callingIntent.getStringExtra("uuid");

		// get a manager for the database
		manager = CouchbaseManager.getInstance(this.getApplicationContext());

		// get the document from the database with all the information for this
		// timeline
		Map<String, Object> timelineData = manager.retrieve(timelineUuid);

		// update the layout to display the timeline values
		timeline.title.setText((String) timelineData
				.get(CouchbaseManager.TIMELINE_TITLE));
		timeline.description.setText((String) timelineData
				.get(CouchbaseManager.TIMELINE_DESCRIPTION));
		timeline.start_date.setText((String) timelineData
				.get(CouchbaseManager.TIMELINE_START_DATE));
		timeline.end_date.setText((String) timelineData
				.get(CouchbaseManager.TIMELINE_END_DATE));
		timeline.location.setText((String) timelineData
				.get(CouchbaseManager.TIMELINE_LOCATION));

		// -------- Section: Edit mode

		// These are for the editing view:
		// Getting all of the edit texts and buttons that exist in this activity
		// in EDIT mode
		timeline.title_editmode = (EditText) findViewById(R.id.timeline_title_editmode);
		timeline.description_editmode = (EditText) findViewById(R.id.timeline_description_editmode);
		timeline.start_date_editmode = (TextView) findViewById(R.id.timeline_start_date_editmode);
		timeline.end_date_editmode = (TextView) findViewById(R.id.timeline_end_date_editmode);
		timeline.location_editmode = (EditText) findViewById(R.id.timeline_location_editmode);

		// Setting these EditText boxes to what's in the database:
		timeline.title_editmode.setText(timeline.title.getText().toString());
		timeline.description_editmode.setText(timeline.description.getText()
				.toString());
		timeline.start_date_editmode.setText(timeline.start_date.getText()
				.toString());
		timeline.end_date_editmode.setText(timeline.end_date.getText()
				.toString());
		timeline.location_editmode.setText(timeline.location.getText()
				.toString());

		// -------- Section: Datepickers

		// Checking to see if start or end date button was clicked:

		// Start button:
		ImageView buttonShowStartDatePicker = (ImageView) findViewById(R.id.buttonShowStartDatePicker);

		buttonShowStartDatePicker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				view.setBackgroundResource(R.drawable.calendar_clicked);
				Timeline.set_start_date_clicked(); // Flag start date as clicked
				Timeline.reset_end_date_clicked();
				try {
					showDatePickerDialog(view);
				} catch (ParseException e) {
					Log.e("ViewAndEditTimelineActivity", e.toString());
				}
			}
		});
		// End button:
		ImageView buttonShowEndDatePicker = (ImageView) findViewById(R.id.buttonShowEndDatePicker);

		buttonShowEndDatePicker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				view.setBackgroundResource(R.drawable.calendar_clicked);
				Timeline.reset_start_date_clicked();
				Timeline.set_end_date_clicked(); // Flag end date as clicked
				try {
					showDatePickerDialog(view);
				} catch (ParseException e) {
					Log.e("ViewAndEditTimelineActivity", e.toString());
				}
			}
		});

		// Also making the actual dates themselves clickable, in addition to
		// being able to click the calendar icon
		timeline.start_date_editmode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Timeline.set_start_date_clicked();// Flag start date as clicked
				Timeline.reset_end_date_clicked();
				try {
					showDatePickerDialog(view);
				} catch (ParseException e) {
					Log.e("ViewAndEditTimelineActivity", e.toString());
				}
			}
		});

		// Also making the actual dates themselves clickable, in addition to
		// being able to click the calendar icon
		timeline.end_date_editmode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				Timeline.set_end_date_clicked();// Flag end date as clicked
				Timeline.reset_start_date_clicked();
				try {
					showDatePickerDialog(view);
				} catch (ParseException e) {
					Log.e("ViewAndEditTimelineActivity", e.toString());
				}
			}
		});

		// get a reference to the list view for population later
		entryListView = (ListView) findViewById(R.id.allEntriesInTimelineListView);
		// get a reference to the editmode list view for populate
		// uses a different layout than the displaymode list view that has
		// buttons for deleting entries on it
		entryListViewEditmode = (ListView) findViewById(R.id.allEntriesInTimelineListViewEditmode);

		// initialize the list of entries to delete
		entriesToDelete = new LinkedList<String>();

		// set up an adapter for managing the list
		setUpEntryListAdapter();

		// set the list view to get populated from an adapter
		entryListView.setAdapter(entryListAdapter);
		// set the editmode list view to get populated from an adapter
		entryListViewEditmode.setAdapter(entryListAdapterEditmode);

		// get all the data from the database
		readAndDisplayTimelineData();

		// set up a listener for the list view
		entryListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View v,
					int position, long arg) {
				@SuppressWarnings("unchecked")
				Map<String, String> row = (Map<String, String>) adapter
						.getItemAtPosition(position);

				viewEntry(row);
			}
		});

	}

	/**
	 * This gets the datepicker to display.
	 * 
	 * @param v
	 *            : The view
	 * @throws ParseException
	 */
	// This will show the datepicker to the user
	@SuppressLint("SimpleDateFormat")
	public void showDatePickerDialog(View v) throws ParseException {
		Calendar date = Calendar.getInstance(); // This will contain the current
												// date
		SimpleDateFormat fmt;

		// If the start date button was clicked:
		if ((Timeline.get_start_date_clicked() == 1)
				&& (Timeline.get_end_date_clicked() == 0)) {
			// If this wasn't left blank when entry was originally made:
			fmt = new SimpleDateFormat("MM-dd-yyyy");
			// Parse the old date
			date.setTime(fmt.parse(timeline.start_date.getText().toString()));

			// This means end date button was clicked:
		} else if ((Timeline.get_end_date_clicked() == 1)
				&& (Timeline.get_start_date_clicked() == 0)) {

			fmt = new SimpleDateFormat("MM-dd-yyyy");
			// Parse the old date
			date.setTime(fmt.parse(timeline.end_date.getText().toString()));

			// This is bad, means there's an error:
		} else {
			Log.e("CreateNewTimelineActivity",
					"Neither date was clicked; this should not have been triggered.");
		}

		date.add(Calendar.MONTH, -1); // Need to subtract one

		// Create a bundle to pass the date
		Bundle currentDateBundle = new Bundle();
		currentDateBundle.putLong("setDate", date.getTimeInMillis());

		DialogFragment datePickerFragment = new DatePickerFragment();
		datePickerFragment.setArguments(currentDateBundle);
		datePickerFragment.show(getSupportFragmentManager(), "datePicker");
	}

	/**
	 * This is the method that sets the textview displaying the date to what was
	 * chosen by the user with the datepicker.
	 * 
	 * @param view
	 *            : The datepicker
	 * @param year
	 *            : Chosen year (by user)
	 * @param month
	 *            : Chosen month (by user)
	 * @param day
	 *            : Chosen day (by user)
	 */
	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		if ((Timeline.get_start_date_clicked() == 1)
				&& (Timeline.get_end_date_clicked() == 0)) {
			// If the start date button was clicked:

			// Getting the current end date
			Integer[] endDatePieces = Timeline
					.getMonthDayYearFromString(timeline.end_date_editmode
							.getText().toString());
			// Check if the start date is before the end date
			boolean correctDateOrder = Timeline
					.checkIfStartDateIsBeforeEndDate(month, day, year,
							endDatePieces[0] - 1, endDatePieces[1],
							endDatePieces[2]);

			if (!correctDateOrder) {
				// If the dates are not int he correct order,
				// show error messages
				invalidStartDateError.setVisibility(View.VISIBLE);
				invalidEndDateError.setVisibility(View.INVISIBLE);
			} else {
				// Hide error messages:
				invalidStartDateError.setVisibility(View.INVISIBLE);
				invalidEndDateError.setVisibility(View.INVISIBLE);
				// Updating the text that contains the date
				((TextView) findViewById(R.id.timeline_start_date_editmode))
						.setText((month + 1) + "-" + day + "-" + year);
			}

			Timeline.reset_start_date_clicked(); // Set back to zero

		} else if ((Timeline.get_end_date_clicked() == 1)
				&& (Timeline.get_start_date_clicked() == 0)) {
			// This means end date button was clicked:

			// Getting the current start date
			Integer[] startDatePieces = Timeline
					.getMonthDayYearFromString(timeline.start_date_editmode
							.getText().toString());
			// Check if the start date is before the end date
			boolean correctDateOrder = Timeline
					.checkIfStartDateIsBeforeEndDate(startDatePieces[0] - 1,
							startDatePieces[1], startDatePieces[2], month, day,
							year);

			if (!correctDateOrder) {
				// If the dates are not int he correct order,
				// show error messages
				invalidEndDateError.setVisibility(View.VISIBLE);
				invalidStartDateError.setVisibility(View.INVISIBLE);
			} else {
				// Hide error messages:
				invalidEndDateError.setVisibility(View.INVISIBLE);
				invalidStartDateError.setVisibility(View.INVISIBLE);
				// You may update the date
				// Updating the text that contains the date
				((TextView) findViewById(R.id.timeline_end_date_editmode))
						.setText((month + 1) + "-" + day + "-" + year);
			}
			Timeline.reset_end_date_clicked(); // Set back to zero

		} else {
			// This is bad, means there's an error:
			Log.e("CreateNewTimelineActivity",
					"Neither date was clicked; this should not have been triggered.");
		}
	}

	/**
	 * This method will add the given view to the list that will be deleted when
	 * edit mode is saved. This method is called in the onClick property for the
	 * delete entry button in the XML files for each list item
	 * 
	 * @param view
	 *            the view passed to this function is the view of the delete
	 *            button attached to a entry list item in editmode
	 */
	public void deleteRow(View v) {
		// get the view of the list item from the delete button
		View listItem = ((View) v.getParent());
		// find the UUID of the current list item
		TextView listItemUuid = (TextView) listItem
				.findViewById(R.id.list_item_uuid);

		// get the position of the view to delete
		int position = entryListViewEditmode.getPositionForView(listItem);

		// add the unique identifier for the entry to delete to the list of
		// entries to delete
		entriesToDelete.add(listItemUuid.getText().toString());

		// remove the view from the currently displayed list
		entryListData.remove(position);
		entryListAdapterEditmode.notifyDataSetChanged();
	}

	/**
	 * Decides what to do when a resulting activity returns
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// check to see which activity returned
		if (requestCode == Timeline.get_create_new_entry_activity()) {
			// this is information for creating a new entry

			// check to make sure the activity returned correctly
			if (resultCode == RESULT_OK) {

				// get all the entry information necessary for insertion
				String title = data
						.getStringExtra(CouchbaseManager.ENTRY_TITLE);
				String description = data
						.getStringExtra(CouchbaseManager.ENTRY_DESCRIPTION);
				String location = data
						.getStringExtra(CouchbaseManager.ENTRY_LOCATION);
				String date = data.getStringExtra(CouchbaseManager.ENTRY_DATE);

				// create a new entry in the database
				manager.createEntry(title, description, location, date,
						timelineUuid);
			}
		}
	}

	private void setUpEntryListAdapter() {

		// set up an array list to pass to the adapter for
		// list view population
		entryListData = new ArrayList<HashMap<String, String>>();

		// create a new SimpleAdapter for populating the list view
		entryListAdapter = new SimpleAdapter(this, entryListData,
				R.layout.list_item_timeline_and_entry, columnTags, columnIds);

		entryListAdapterEditmode = new SimpleAdapter(this, entryListData,
				R.layout.list_item_timeline_and_entry_delete, columnTags,
				columnIds);
	}

	/**
	 * This displays the data for a timeline (the entries).
	 */
	private void readAndDisplayTimelineData() {
		// set up the query
		Query query = manager.getDatabase()
				.getView(CouchbaseManager.VIEW_ENTRY).createQuery();

		// only the mapper is needed to return all the results necessary
		query.setMapOnly(true);

		// set up the start and end keys to select all the entries in this
		// timeline
		Object[] startKey = new Object[] { timelineUuid,
				CouchbaseManager.MIN_ORDER_NUM };
		Object[] endKey = new Object[] { timelineUuid,
				CouchbaseManager.MAX_ORDER_NUM };
		query.setStartKey(startKey);
		query.setEndKey(endKey);

		// make the query a live query
		liveQuery = query.toLiveQuery();

		liveQuery.addChangeListener(new LiveQuery.ChangeListener() {

			@Override
			public void changed(final LiveQuery.ChangeEvent event) {
				runOnUiThread(new Runnable() {
					@SuppressWarnings("unchecked")
					public void run() {

						// clear out the existing list for repopulation
						entryListData.clear();

						// get an iterator over the items to insert into the
						// list view
						Iterator<QueryRow> iter = event.getRows();

						// a temporary value for the current row while looping
						QueryRow currentRow = null;

						// add everything in this query result to the list view
						// adapter
						while (iter.hasNext()) {

							// create a map for this row
							HashMap<String, String> tempMap = new HashMap<String, String>();

							// temporarily store the current row
							currentRow = iter.next();

							// populate the map for this row
							// get the value of this row (the document) then get
							// the entry title from that
							tempMap.put(columnTags[0],
									((Map<Object, String>) currentRow
											.getValue())
											.get(CouchbaseManager.ENTRY_TITLE));
							// a uuid for referencing this document in the
							// future
							tempMap.put(columnTags[1],
									currentRow.getDocumentId());

							// the entry description from that
							tempMap.put(
									columnTags[2],
									((Map<Object, String>) currentRow
											.getValue())
											.get(CouchbaseManager.ENTRY_DESCRIPTION));

							// the entry date from that
							tempMap.put(columnTags[3],
									((Map<Object, String>) currentRow
											.getValue())
											.get(CouchbaseManager.ENTRY_DATE));

							// the entry location from that
							tempMap.put(
									columnTags[4],
									((Map<Object, String>) currentRow
											.getValue())
											.get(CouchbaseManager.ENTRY_LOCATION));

							entryListData.add(tempMap);
						}

						// reverse the order of the list so that the most recent
						// date is on top
						Collections.reverse(entryListData);

						Log.d("ViewAndEditTimeline", "Updating Entry List View");

						// let the list view know that the data it is displaying
						// got changed,
						// so it can update the list view
						entryListAdapter.notifyDataSetChanged();
					}
				});
			}
		});

		liveQuery.start();
	}

	/**
	 * Method used to view an entry when it is clicked.
	 * 
	 * @param row
	 */
	private void viewEntry(Map<String, String> row) {
		final Intent intent = new Intent(this, ViewAndEditEntryActivity.class);

		intent.putExtra(CouchbaseManager.ENTRY_UUID, row.get(columnTags[1]));

		startActivityForResult(intent, Timeline.get_view_entry_activity());
	}

	/**
	 * This method is called when the user agrees to discard edits made to a
	 * timeline.
	 */
	@Override
	public void onDiscardOkDialog() {
		final ViewSwitcher viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
		viewSwitcher.showNext();
		// The cancel and save buttons should go away:
		timeline.cancel.setVisible(false);
		timeline.save.setVisible(false);
		// The edit and add buttons should reappear:
		timeline.edit.setVisible(true);
		timeline.add.setVisible(true);

	}

	/**
	 * Display action bar buttons
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		getMenuInflater().inflate(R.menu.view_timeline, menu);
		// set MenuItems by id
		timeline.edit = menu.findItem(R.id.edit_timeline);
		timeline.cancel = menu.findItem(R.id.cancel_edit_timeline);
		timeline.save = menu.findItem(R.id.save_edit_timeline);
		timeline.add = menu.findItem(R.id.timeline_add_entry);

		timeline.cancel.setVisible(false);
		timeline.save.setVisible(false);

		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * This method is called when a menu item has been clicked.
	 * 
	 * @param item
	 *            is the menu item that was clicked.
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		final ViewSwitcher viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);

		switch (item.getItemId()) {

		case R.id.edit_timeline:
			// This is when a user clicks the icon to edit a timeline:
			// Use the viewswitcher to switch to edit mode:
			viewSwitcher.showNext();
			item.setVisible(false);
			// Let the viewer see the cancel and save buttons:
			timeline.cancel.setVisible(true);
			timeline.save.setVisible(true);
			// Hide the add button:
			timeline.add.setVisible(false);

			// clear out the list of entries to delete to ensure that
			// no entries already deleted get deleted a second time
			entriesToDelete.clear();

			return true;
		case R.id.cancel_edit_timeline:
			// This is when a user clicks the cancel button when editing a
			// timeline:
			CancelEditsFragment cancelDialog = new CancelEditsFragment();
			// This will alert the user of changes that are going to be discarded:
			cancelDialog.show(getSupportFragmentManager(), "cancel_dialog");
			return true;
		case R.id.save_edit_timeline:

			// update the database with all the new values
			manager.update(CouchbaseManager.TIMELINE_TITLE,
					timeline.title_editmode.getText().toString(), timelineUuid);
			manager.update(CouchbaseManager.TIMELINE_DESCRIPTION,
					timeline.description_editmode.getText().toString(),
					timelineUuid);
			manager.update(CouchbaseManager.TIMELINE_START_DATE,
					timeline.start_date_editmode.getText().toString(),
					timelineUuid);
			manager.update(CouchbaseManager.TIMELINE_END_DATE,
					timeline.end_date_editmode.getText().toString(),
					timelineUuid);
			manager.update(CouchbaseManager.TIMELINE_LOCATION,
					timeline.location_editmode.getText().toString(),
					timelineUuid);

			// transfer all the current values in edit mode to view mode
			timeline.title
					.setText(timeline.title_editmode.getText().toString());
			timeline.description.setText(timeline.description_editmode
					.getText().toString());
			timeline.start_date.setText(timeline.start_date_editmode.getText()
					.toString());
			timeline.end_date.setText(timeline.end_date_editmode.getText()
					.toString());
			timeline.location.setText(timeline.location_editmode.getText()
					.toString());

			// delete all the items to be deleted
			manager.deleteMultipleEntries(entriesToDelete);

			// hide keyboard
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(
					timeline.title_editmode.getWindowToken(), 0);

			viewSwitcher.showPrevious();
			item.setVisible(false);
			timeline.cancel.setVisible(false);
			timeline.edit.setVisible(true);
			timeline.add.setVisible(true);
			// toast to show changes have been saved:
			Toast.makeText(getApplicationContext(), "Changes Saved",
					Toast.LENGTH_SHORT).show();

			return true;
		case R.id.timeline_add_entry:
			final Intent createEntryIntent = new Intent(this,
					CreateNewEntryActivity.class);
			startActivityForResult(createEntryIntent,
					Timeline.get_create_new_entry_activity());
			return true;
		case R.id.help:
			Timeline.showHelpScreen(R.string.view_edit_timeline_help,
					getApplicationContext());
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Just switch views back when you hit this button.
	 */
	@Override
	public void onBackPressed() {
		Log.d("CDA", "onBackPressed Called");
		/*
		 * If we can see the save button, we're clearly in edit mode. This means
		 * we're overwriting the onBackPressed method.
		 */
		if (timeline.save.isVisible()) {
			// Do same "okay to discard" procedure:
			CancelEditsFragment cancelDialog = new CancelEditsFragment();
			cancelDialog.show(getSupportFragmentManager(), "cancel_dialog");

			// restart the list view automatic update when the database changes
			liveQuery.start();
		} else {
			// Do the usual back button procedure
			super.onBackPressed();
		}
	}

}
