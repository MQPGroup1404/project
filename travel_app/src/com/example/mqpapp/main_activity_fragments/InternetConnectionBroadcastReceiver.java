package com.example.mqpapp.main_activity_fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * This class is used to check if there is an internet connection, in case the user
 * turns on an internet connection after the app has already started.
 *
 */
public class InternetConnectionBroadcastReceiver extends BroadcastReceiver {

	    @Override
	    public void onReceive(Context context, Intent intent) {
	        final String action = intent.getAction();
	        if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
	            if (intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED, false)){
	                Log.e("found wifi", "We have found internet");
	            } else {
	                Log.e("not found wifi", "Have not found internet");
	            }
	        }
	    }
	
}