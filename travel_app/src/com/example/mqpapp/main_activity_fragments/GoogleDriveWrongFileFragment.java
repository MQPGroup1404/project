package com.example.mqpapp.main_activity_fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;

import com.example.mqpapp.R;

/**
 * This class is used when the user chooses a file type that is not a .cblite file.
 *
 */
@SuppressLint("InflateParams")
public class GoogleDriveWrongFileFragment extends DialogFragment {
	
    public interface DiscardDialogListener {
        void onDiscardOkDialog();
    }
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog dialog;
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();
		
		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		View V = inflater.inflate(R.layout.google_drive_wrong_file, null);
		builder.setView(V);
		

		builder.setMessage("Incorrect File Type Chosen")
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
							}
						});
		
		
		// Create the AlertDialog object and return it
		dialog = builder.create();
		dialog.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		return dialog;
	}

}

