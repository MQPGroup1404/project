package com.example.mqpapp.create_view_edit_entry;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mqpapp.BackupManager;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.R;

/**
 * This class is used when a user edits text in an entry.
 * 
 */
@SuppressLint("InflateParams")
public class EditTextFragment extends DialogFragment {

	private static final String TAG = EditTextFragment.class.getSimpleName();

	TextView list_item_text; // Text of the item being edited
	TextView list_item_uuid; // UUID of the item being edited
	EditText edit_media_text_dialog; // Edit text where user types new text
	Entry entry = new Entry();
	// a unique identifier for the entry being displayed / edited
	String entryUuid;
	// a manager for the database for getting documents and updating data
	CouchbaseManager manager;

	public interface EditNameDialogListener {
		void onFinishEditDialog(String inputText);
	}

	public EditTextFragment(TextView text, TextView uuid) {
		this.list_item_text = text;
		this.list_item_uuid = uuid;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog dialog;
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// get the intent that was used to call this activity
		Intent callingIntent = getActivity().getIntent();
		// get the uuid of the entry to display from the intent
		entryUuid = callingIntent.getStringExtra(CouchbaseManager.ENTRY_UUID);
		// get a manager for the database
		manager = CouchbaseManager.getInstance(this.getActivity()
				.getApplicationContext());
		// get document from the database w/ all the information for this entry

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		View V = inflater.inflate(R.layout.edit_text_dialog, null);
		builder.setView(V);

		// EditText where the user types
		edit_media_text_dialog = (EditText) V
				.findViewById(R.id.entry_edit_text_new_dialog);
		edit_media_text_dialog.setText(list_item_text.getText());
		builder.setMessage("Edit Item Text")
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// hide keyboard
								InputMethodManager imm = (InputMethodManager) getActivity()
										.getSystemService(
												Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(
										edit_media_text_dialog.getWindowToken(),
										0);

								// get the calling activity
								EditNameDialogListener activity = (EditNameDialogListener) getActivity();

								Log.i(TAG, "Updating text to : "
										+ edit_media_text_dialog.getText()
												.toString());

								// pass the text being returned to the calling
								// activity
								activity.onFinishEditDialog(edit_media_text_dialog
										.getText().toString());

								Log.i(TAG, "uuid : "
										+ list_item_uuid.getText().toString());
								// update the database to contain the new data
								manager.update(CouchbaseManager.MEDIA_MESSAGE,
										edit_media_text_dialog.getText()
												.toString(), list_item_uuid
												.getText().toString());
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// User cancelled the dialog
								// hide keyboard
								InputMethodManager imm = (InputMethodManager) getActivity()
										.getSystemService(
												Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(
										edit_media_text_dialog.getWindowToken(),
										0);
							}
						});

		// Create the AlertDialog object and return it
		dialog = builder.create();
		dialog.getWindow().setSoftInputMode(
				LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		return dialog;
	}

	/**
	 * When this fragment is stopped, we will make a backup of the current state
	 * of the database.
	 */
	@Override
	public void onStop() {
		super.onStop();
		Context context = getActivity().getApplicationContext();
		// Calling the "make backup" method to create a backup
		BackupManager.copyAndBackUpDatabase(context);
	}
}