package com.example.mqpapp.create_view_edit_entry;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mqpapp.BackupManager;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.R;
import com.example.mqpapp.media_handling.BitmapWorkerTask;

/**
 * This class is used when a user adds a picture to an entry.
 *
 */
@SuppressLint("InflateParams")
public class AddPictureFragment extends DialogFragment {
	// image view that will display the selected image
	ImageView new_picture_dialog;
	// list view that will display the selected images
	ListView new_listview_dialog;
	// invisibly text view to attach the selected image URI to the fragment
	TextView uri_text_view;
	//string value of uri for rotation
	String imageIdString;
	// an edit text box for adding a caption to a picture
	EditText caption_text_view;
	Entry entry = new Entry();
	// a unique identifier for the entry being displayed / edited
	String entryUuid;
	// a manager for the database for getting documents and updating data
	CouchbaseManager manager;
	View V;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// get the intent that was used to call this activity
		Intent callingIntent = getActivity().getIntent();
		// get the uuid of the entry to display from the intent
		entryUuid = callingIntent.getStringExtra(CouchbaseManager.ENTRY_UUID);
		// get a manager for the database
		manager = CouchbaseManager.getInstance(this.getActivity()
				.getApplicationContext());
		// get document from the database w/ all the information for this entry

		// Inflate and set the layout for the dialog
		V = inflater.inflate(R.layout.add_picture_dialog, null);

		builder.setView(V);
		
		// For when one image is selected:
		// This is the singular image:
		new_picture_dialog = (ImageView) V
				.findViewById(R.id.single_picture);
		// This is the singular caption:
		caption_text_view = (EditText) V
				.findViewById(R.id.new_picture_caption);

		// This is the listview that contains the image and uri
		new_listview_dialog = (ListView) V
				.findViewById(R.id.list_of_pictures);

		builder.setMessage(R.string.add_picture)
				.setPositiveButton("Add Picture",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// Calling the method to update the database
								ViewAndEditEntryActivity callingActivity = (ViewAndEditEntryActivity) getActivity();
								// have the calling activity list scroll to the bottom to view the new media
								callingActivity.setMediaAdded(true);
								// Add to the database the selected picture(s) and potential caption
								callingActivity.addNewPicturesToDatabase(caption_text_view.getText().toString());
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
							}
						});
		// Create the AlertDialog object and return it
		return builder.create();

	}

	/**
	 * Source:
	 * http://developer.android.com/training/displaying-bitmaps/process-bitmap.html
	 * @param filePath is the path to an image
	 * @param imageView is the imageView where the image will be put
	 */
	public void loadBitmap(String filePath, ImageView imageView) {
		BitmapWorkerTask task = new BitmapWorkerTask(imageView, getActivity()
				.getApplicationContext());
		task.execute(filePath);

	}

	/**
	 * gets image view on add picture dialog to load picture through
	 * @return the imageview where the newly-selected picture goes
	 */
	public ImageView getImageView() {
		return new_picture_dialog;
	}

	/** gets list view on add picture dialog to load picture through
	* OnActivityForResult method
	* in ViewAndEditEntry
	* @return the listview
	* */
	public ListView getListView() {
		return new_listview_dialog;
	}

	/** gets (visibility=gone) text view on add picture dialog to save the
	* picture URI
	* and carry it over from OnActivityForResult to the fragment
	* @return the box where the URI goes (and is hidden)
	*/
	public TextView getTextView() {
		return uri_text_view;
	}

	/** gets (visibility=gone) text view on add picture dialog to save the
	* picture URI
	* and carry it over from OnActivityForResult to the fragment
	* @return the box where a caption goes
	*/
	public EditText getEditTextView() {
		return caption_text_view;
	}
	
	/**
	 * When this fragment is stopped, we will make a backup of the current state of the database.
	 */
	@Override
	public void onStop(){
		super.onStop();
		Context context = getActivity().getApplicationContext();
		// Calling the "make backup" method to create a backup
		BackupManager.copyAndBackUpDatabase(context);
	}
	

}
