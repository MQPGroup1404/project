package com.example.mqpapp.create_view_edit_entry;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mqpapp.BackupManager;
import com.example.mqpapp.CancelEditsFragment;
import com.example.mqpapp.CancelEditsFragment.DiscardDialogListener;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.DatePickerFragment;
import com.example.mqpapp.R;
import com.example.mqpapp.create_view_edit_timeline.Timeline;

/**
 * This class allows a user to create a new timeline.
 */
@SuppressLint("SimpleDateFormat")
public class CreateNewEntryActivity extends FragmentActivity implements
		OnDateSetListener, DiscardDialogListener {
	Entry entry = new Entry();
	// This image holds the button to show the datepicker
	ImageView imageView;
	// This textview shows up when a user tries to save an entry without a title
	TextView noTitleError;
	ArrayList<HashMap<String, String>> listData;

	/**
	 * This method is called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// This is the XML for this example
		setContentView(R.layout.create_new_entry_activity);

		// ---------- Text boxes on screen ----------
		// Putting into variables all of the textviews and buttons that exist in
		// this activity
		entry.title = (EditText) findViewById(R.id.entry_title_new);
		entry.description = (EditText) findViewById(R.id.entry_description_new);
		entry.date = (TextView) findViewById(R.id.entry_date_new);
		entry.location = (EditText) findViewById(R.id.entry_location_new);
		noTitleError = (TextView) findViewById(R.id.no_title_error);

		// Setting default date in the entry, today's date
		// Step 1: Create a new date object, set it to today's date
		DateFormat dateFormat = new SimpleDateFormat("M-d-yyyy");
		Date date = new Date();
		// Step 2: Set the text in the date box to today's date
		entry.date.setText(dateFormat.format(date));

		// Date picker button
		ImageView buttonShowDatePicker = (ImageView) findViewById(R.id.buttonShowDatePicker);
		
		// Displays the datepicker
		buttonShowDatePicker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				showDatePickerDialog(view);
			}
		});

		entry.date.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				showDatePickerDialog(view);
			}
		});

	}

	/**
	 * This gets the datepicker to display.
	 * 
	 * @param v
	 *            : The view
	 */
	// This will show the datepicker to the user
	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}

	/**
	 * This is the method that sets the textview displaying the date to what was
	 * chosen by the user with the datepicker.
	 * 
	 * @param view
	 *            : The datepicker
	 * @param year
	 *            : Chosen year (by user)
	 * @param month
	 *            : Chosen month (by user)
	 * @param day
	 *            : Chosen day (by user)
	 */
	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		((TextView) findViewById(R.id.entry_date_new)).setText((month + 1)
				+ "-" + day + "-" + year);
	}

	/**
	 * This method returns the new entry's data.
	 */
	private void returnNewEntryData() {

		// create a new intent to return
		Intent insertionIntent = new Intent();

		// add all the necessary information to the intent

		insertionIntent.putExtra(CouchbaseManager.ENTRY_TITLE, entry.title
				.getText().toString());
		insertionIntent.putExtra(CouchbaseManager.ENTRY_DESCRIPTION,
				entry.description.getText().toString());
		insertionIntent.putExtra(CouchbaseManager.ENTRY_LOCATION,
				entry.location.getText().toString());
		insertionIntent.putExtra(CouchbaseManager.ENTRY_DATE, entry.date
				.getText().toString());

		// set the result of this intent being returned to the calling activity
		setResult(RESULT_OK, insertionIntent);

		// close this activity and pass this intent onto the calling activity
		finish();
	}

	@Override
	public void onDiscardOkDialog() {
		this.finish();

	}

	/**
	 * Display action bar buttons
	 * 
	 * @param menu
	 *            is the menu object that is being used
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		getMenuInflater().inflate(R.menu.create_entry, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * This method will handle pressing of action bar items.
	 * 
	 * @param item
	 *            is the menubar item being pressed.
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.cancel_edit_entry:
			CancelEditsFragment cancelDialog = new CancelEditsFragment();
			cancelDialog.show(getSupportFragmentManager(), "cancel_dialog");
			// this.finish();
			return true;
		case R.id.save_new_entry:
			if (entry.title.getText().toString().equals("")) {
				noTitleError.setVisibility(View.VISIBLE);
			} else {
				returnNewEntryData();
			}
			return true;
		case R.id.help:
			Timeline.showHelpScreen(R.string.new_entry_help,
					getApplicationContext());
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * When this activity is stopped, we will make a backup of the current state
	 * of the database.
	 */
	@Override
	public void onStop() {
		super.onStop();
		Context context = getApplicationContext();
		// Calling the "make backup" method to create a backup
		BackupManager.copyAndBackUpDatabase(context);
	}
}
