package com.example.mqpapp.create_view_edit_entry;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;

import com.example.mqpapp.BackupManager;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.R;

/**
 * This class is used when the user wants to add text to an entry.
 *
 */
@SuppressLint("InflateParams")
public class AddTextFragment extends DialogFragment {
	EditText new_text_dialog;
	Entry entry = new Entry();
	// a unique identifier for the entry being displayed / edited
	String entryUuid;
	// a manager for the database for getting documents and updating data
	CouchbaseManager manager;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog dialog;
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// get the intent that was used to call this activity
		Intent callingIntent = getActivity().getIntent();
		// get the uuid of the entry to display from the intent
		entryUuid = callingIntent.getStringExtra(CouchbaseManager.ENTRY_UUID);
		// get a manager for the database
		manager = CouchbaseManager.getInstance(this.getActivity()
				.getApplicationContext());
		// get document from the database w/ all the information for this entry

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		View V = inflater.inflate(R.layout.add_text_dialog, null);
		builder.setView(V);

		// EditText where the user types
		new_text_dialog = (EditText) V
				.findViewById(R.id.entry_add_text_new_dialog);
		builder.setMessage(R.string.add_text)
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// Checking if the user entered blank text
								if (!(new_text_dialog.getText().toString().equals(""))) {
									// have the calling activity list scroll to
									// the bottom to view the new media
									ViewAndEditEntryActivity callingActivity = (ViewAndEditEntryActivity) getActivity();
									callingActivity.setMediaAdded(true);

									// Now, we need to save to the database.
									// Create a new media document in the
									// database with this date
									manager.createMedia(
											CouchbaseManager.MEDIA_TYPE_TEXT,
											entryUuid, new_text_dialog
													.getText().toString(), null);
								}
							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// User cancelled the dialog
							}
						});

		// Create the AlertDialog object and return it
		dialog = builder.create();
		dialog.getWindow().setSoftInputMode(
				LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		return dialog;
	}

	/**
	 * When this fragment is stopped, we will make a backup of the current state
	 * of the database.
	 */
	@Override
	public void onStop() {
		super.onStop();
		Context context = getActivity().getApplicationContext();
		// Calling the "make backup" method to create a backup
		BackupManager.copyAndBackUpDatabase(context);
	}
}