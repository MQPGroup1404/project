package com.example.mqpapp.create_view_edit_entry;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.mqpapp.R;
import com.example.mqpapp.media_handling.BitmapWorkerTask;

/**
 * This class is used to let a user click an image and make it full screen.
 */
public class FullScreenImage extends FragmentActivity {

	String imageIdString = "";

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null) {
			Bundle extras = getIntent().getExtras();
			if (extras == null) {
				imageIdString = null;
			} else {
				// get image URI from calling activity if not null
				imageIdString = extras.getString("IMAGE_URI");
			}
		} else {
			imageIdString = (String) savedInstanceState
					.getSerializable("IMAGE_URI");
		}
		if (savedInstanceState != null) {
			imageIdString = savedInstanceState.getString("imageIdString");

		}

		setContentView(R.layout.full_image);

		// find the image view for the full screen image
		final ImageView imageView = (ImageView) findViewById(R.id.full_image);

		// set the image view to be full screen
		imageView.setScaleType(ImageView.ScaleType.FIT_XY);
		imageView.setAdjustViewBounds(true);
		// create an onPreDrawListener to wait until the ImageView is
		// measured before trying to populate it
		ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
		viewTreeObserver
				.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

					@Override
					public boolean onPreDraw() {
						imageView.getViewTreeObserver()
								.removeOnPreDrawListener(this);

						// create a bitmap worker to display the image
						BitmapWorkerTask task = new BitmapWorkerTask(imageView,
								getApplicationContext());

						// load the image
						task.execute(imageIdString);

						return true;
					}

				});

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putString("imageIdString", imageIdString);

	}

	public void onFullImageTap(View v) {
		this.finish();

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Hiding the action bar if the device is turned to landscape
		// orientation:
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			getActionBar().hide();
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			// But if the orientation is portrait, leave the action bar.
			getActionBar().show();
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}

	}
}