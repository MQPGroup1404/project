package com.example.mqpapp.create_view_edit_entry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.ViewSwitcher;

import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryRow;
import com.example.mqpapp.BackupManager;
import com.example.mqpapp.CancelEditsFragment;
import com.example.mqpapp.CancelEditsFragment.DiscardDialogListener;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.DatePickerFragment;
import com.example.mqpapp.R;
import com.example.mqpapp.create_view_edit_entry.EditTextFragment.EditNameDialogListener;
import com.example.mqpapp.create_view_edit_timeline.Timeline;
import com.example.mqpapp.media_handling.BaseActivity;
import com.example.mqpapp.media_handling.BitmapWorkerTask;
import com.example.mqpapp.media_handling.MultiMediaAdapter;
import com.example.mqpapp.media_handling.MultiPhotoSelectActivity;
import com.mobeta.android.dslv.DragSortListView;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * This class allows a user to view and edit a new entry.
 */
@SuppressLint("ViewHolder")
public class ViewAndEditEntryActivity extends FragmentActivity implements
		OnDateSetListener, OnMenuItemClickListener, EditNameDialogListener,
		DiscardDialogListener {
	// a tag for log messages
	private static final String TAG = ViewAndEditEntryActivity.class
			.getSimpleName();
	private final int IMAGE_REDUCTION = 4;

	Entry entry = new Entry();
	// a unique identifier for the entry being displayed / edited
	String entryUuid;

	// a manager for the database for getting documents and updating data
	CouchbaseManager manager;

	// a list view to display the entries in the database
	private ListView mediaListView;
	private DragSortListView mediaListViewEdit;
	// an array list to hold the rows to be displayed in the list view
	// the array list is an array of hash maps that have keys corresponding
	// to the column tags.
	private ArrayList<HashMap<String, String>> mediaListData;
	// an array list to maintain an ordering of the list before reordering
	// and deleting entries in the event that the changes are canceled
	private ArrayList<HashMap<String, String>> previousMediaListData;
	// a linked list of media uuid's to delete when the database is updated
	// after edits mode is left.
	private LinkedList<String> mediaToDelete;
	// an adapter to work with the entry list view
	private MultiMediaAdapter mediaListAdapter;
	// a live query that monitors the database for changes and updates the list
	// view when a change has been made.
	private LiveQuery liveQuery;

	// a boolean representing whether media was recently added or not
	private boolean mediaAdded;

	// Dialog fragments that pop up when a user makes a selection:
	private DialogFragment addTextFragment = new AddTextFragment();
	private DialogFragment addPictureFragment = new AddPictureFragment();
	private DialogFragment addAudioFragment = new AddAudioFragment();
	private DialogFragment addVideoFragment = new AddVideoFragment();

	// This will contain the selected video
	VideoView videoView;
	// text views for keeping track of which list item is being edited
	TextView currentTextView;
	TextView currentUuid;

	/**
	 * This method is called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 */
	public void onCreate(Bundle savedInstanceState) {
		BaseActivity.imageLoader.init(ImageLoaderConfiguration
				.createDefault(getBaseContext()));
		super.onCreate(savedInstanceState);
		// This is the XML file for this activity
		setContentView(R.layout.view_and_edit_entry_activity);

		// ---------- Database ----------

		// get the intent that was used to call this activity
		Intent callingIntent = getIntent();
		// get the uuid of the entry to display from the intent
		entryUuid = callingIntent.getStringExtra(CouchbaseManager.ENTRY_UUID);
		// get a manager for the database
		manager = CouchbaseManager.getInstance(this.getApplicationContext());
		// get document from the database w/ all the information for this entry
		Map<String, Object> entryData = manager.retrieve(entryUuid);

		// Media list for an entry:
		// get a reference to the list view for population later
		mediaListView = (ListView) findViewById(R.id.allMediaInEntryListView);
		mediaListViewEdit = (DragSortListView) findViewById(R.id.allMediaInEntryListViewEdit);
		// initialize the array list that will be used to populate
		// the list view in this activity
		mediaListData = new ArrayList<HashMap<String, String>>();

		// initialize the list of items to delete
		mediaToDelete = new LinkedList<String>();

		// set up an adapter for managing the list
		mediaListAdapter = new MultiMediaAdapter(this, mediaListData);
		// set the list view to get populated from an adapter
		mediaListView.setAdapter(mediaListAdapter);
		mediaListViewEdit.setAdapter(mediaListAdapter);

		// media is not being added when the activity is created
		mediaAdded = false;

		// get all the data from the database
		readAndDisplayMediaData();

		// ---------- Text boxes on screen ----------
		// Getting all of the textviews and buttons that exist in this activity
		// in DISPLAY mode
		// (when data already exists)
		entry.title = (TextView) findViewById(R.id.entry_title);
		entry.description = (TextView) findViewById(R.id.entry_description);
		entry.date = (TextView) findViewById(R.id.entry_date);
		entry.location = (TextView) findViewById(R.id.entry_location);
		// ---------- Text boxes on screen ----------
		// update the layout to display the entry values
		entry.title.setText((String) entryData
				.get(CouchbaseManager.ENTRY_TITLE));
		entry.description.setText((String) entryData
				.get(CouchbaseManager.ENTRY_DESCRIPTION));
		entry.date.setText((String) entryData.get(CouchbaseManager.ENTRY_DATE));
		entry.location.setText((String) entryData
				.get(CouchbaseManager.ENTRY_LOCATION));

		// These are for the editing view:
		// Getting all of the edit texts and buttons that exist in this activity
		// in EDIT mode
		entry.title_editmode = (EditText) findViewById(R.id.entry_title_editmode);
		entry.description_editmode = (EditText) findViewById(R.id.entry_description_editmode);
		entry.date_editmode = (TextView) findViewById(R.id.entry_date_editmode);
		entry.location_editmode = (EditText) findViewById(R.id.entry_location_editmode);

		// Setting these EditText boxes to what's in the database:
		entry.title_editmode.setText(entry.title.getText().toString());
		entry.description_editmode.setText(entry.description.getText()
				.toString());
		entry.date_editmode.setText(entry.date.getText().toString());
		entry.location_editmode.setText(entry.location.getText().toString());

		// Date button:
		ImageView buttonShowDatePicker = (ImageView) findViewById(R.id.buttonShowDatePicker);

		// Checking to see if start or end date button was clicked:
		buttonShowDatePicker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				showDatePickerDialog(view);
			}
		});

		entry.date_editmode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				showDatePickerDialog(view);
			}
		});

		// Add a drop listener for the drag sort list view.
		mediaListViewEdit.setDropListener(new DragSortListView.DropListener() {
			@Override
			public void drop(int from, int to) {

				// update the array that this list gets its data from
				mediaListData.add(to, mediaListData.remove(from));

				// make sure the adapter knows its data changed
				mediaListAdapter.notifyDataSetChanged();
			}
		});

		entry.date_editmode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showDatePickerDialog(v);

			}
		});
	}

	/**
	 * This gets the datepicker to display.
	 * 
	 * @param v
	 *            : The view
	 */
	// This will show the datepicker to the user
	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}

	/**
	 * This gets the edit text dialog to display.
	 * 
	 * @param v
	 *            : The view of the edit button on a list item that is pressed
	 */
	public void showEditTextDialog(View v) {
		// get the fragment manager for this activity
		FragmentManager fm = getSupportFragmentManager();
		// get parent view of edit button (list item)
		View listItem = ((View) v.getParent().getParent());
		// find the UUID of the current list item
		TextView listItemUuid = (TextView) listItem
				.findViewById(R.id.list_item_uuid);

		// find the list item text view (either text media, or caption)
		TextView listItemTextView = (TextView) listItem
				.findViewById(R.id.list_item_text);

		// set current text view to text view of this list item
		currentTextView = listItemTextView;
		// set current list item uuid text view to the list item uuid of this
		// item
		currentUuid = listItemUuid;

		// create a new editTextFragment dialog and pass it the text and uuid
		EditTextFragment editTextFragment = new EditTextFragment(
				listItemTextView, listItemUuid);
		// show the dialog fragment
		editTextFragment.show(fm, "editText");

	}

	/**
	 * This method gets the text from the edit text dialog fragment and puts it
	 * in the text view for the list item
	 * 
	 * @param inputText
	 *            the text that is returned from the dialog fragment
	 */
	@Override
	public void onFinishEditDialog(String inputText) {
		Log.i(TAG, "Received save update request in activity...");

		String documentUuid = currentUuid.getText().toString();
		Log.i(TAG, "Uuid of edited text = " + documentUuid);
		for (HashMap<String, String> currentDocMap : mediaListData) {

			Log.i(TAG,
					"This doc uuid = "
							+ currentDocMap.get(CouchbaseManager.MEDIA_UUID));
			if (documentUuid.equals(currentDocMap
					.get(CouchbaseManager.MEDIA_UUID))) {
				currentDocMap.put(CouchbaseManager.MEDIA_MESSAGE, inputText);
				break;
			}
		}
		currentTextView.setText(inputText);

	}

	/**
	 * When the user is okay with discarding changes.
	 */
	@Override
	public void onDiscardOkDialog() {
		final ViewSwitcher viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
		viewSwitcher.showPrevious();

		entry.add.setVisible(true);
		entry.save.setVisible(false);
		entry.cancel.setVisible(false);
		entry.edit.setVisible(true);
		// restore the displayed list to the stored copy of it
		mediaListData = copyList(previousMediaListData);
		mediaListAdapter.setList(mediaListData);
		mediaListAdapter.notifyDataSetChanged();
	}

	/**
	 * This method will add the given view to the list that will be deleted when
	 * edit mode is saved. This method is called in the onClick property for the
	 * delete media button in the XML files for each list item
	 * 
	 * @param view
	 *            the view passed to this function is the view of the delete
	 *            button attached to a media list item in editmode
	 */
	public void deleteListItem(View view) {
		// get the view of the list item from the delete button
		View listItem = ((View) view.getParent().getParent());
		// find the UUID of the current list item
		TextView listItemUuid = (TextView) listItem
				.findViewById(R.id.list_item_uuid);

		// get the position of the view to delete
		int position = mediaListViewEdit.getPositionForView(listItem);

		// add the unique identifier for the media to delete to the list of
		// media to delete
		mediaToDelete.add(listItemUuid.getText().toString());

		// remove the view from the currently displayed list
		mediaListData.remove(position);
		mediaListAdapter.notifyDataSetChanged();
	}

	/**
	 * This is the method that sets the textview displaying the date to what was
	 * chosen by the user with the datepicker.
	 * 
	 * @param view
	 *            : The datepicker
	 * @param year
	 *            : Chosen year (by user)
	 * @param month
	 *            : Chosen month (by user)
	 * @param day
	 *            : Chosen day (by user)
	 */
	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		((TextView) findViewById(R.id.entry_date_editmode)).setText((month + 1)
				+ "-" + day + "-" + year);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// If you get back your image (if it was an image trigger), if the
		// result is okay, and the data isn't null
		if (requestCode == Entry.get_result_load_image()
				&& resultCode == RESULT_OK && null != data) {

			// Getting the list of URIs from
			// MultiPhotoSelectActivity
			Bundle extras = data.getExtras();
			ArrayList<String> listOfURIs = extras
					.getStringArrayList("listOfURIs");

			// Clearing the list of previously-selected images
			entry.selectedImages.clear();

			// Turning into objects of type URI
			for (String selectedImage : listOfURIs) {
				Uri uriForImage = Uri.parse(selectedImage);
				entry.selectedImages.add(uriForImage);
			}

			// Getting the string that would say nothing was selected
			// when choosing images:
			String NoItemsSelected = extras.getString("NoItemsSelected");

			// If no images were selected:
			if ((listOfURIs.size() == 0) || NoItemsSelected != null) {
				// Dismiss this dialogfragment
				addPictureFragment.dismiss();
			} else if (listOfURIs.size() == 1) {
				// Checking to see if only one image was selected:

				// Get the first (and only) thing in the
				String pictureURI = listOfURIs.get(0);

				// Accessing the ImageView in the AddPictureFragment
				ImageView imageView = ((AddPictureFragment) addPictureFragment)
						.getImageView();

				// Calling bitmapworkertask on this image
				loadBitmap(pictureURI, imageView);

				// Clear this imageview
				imageView = null;

				// This means that multiple images were selected:
			} else {
				// No captioning with the list view, so hide the edit text box:
				// Accessing the box for typing a caption
				EditText editText = ((AddPictureFragment) addPictureFragment)
						.getEditTextView();
				editText.setVisibility(View.GONE);

				// Get the listview from AddPictureFragment
				ListView listViewForPicturesAndCaptions = ((AddPictureFragment) addPictureFragment)
						.getListView();

				ListViewImageAdapter listViewAdapter = new ListViewImageAdapter(
						this, R.layout.list_photos, entry.selectedImages);

				listViewAdapter.notifyDataSetChanged();

				// Set the adapter for the images
				listViewForPicturesAndCaptions.setAdapter(listViewAdapter);
			}

			// create a new worker task to load the picture
		} else if (requestCode == Entry.get_result_load_video()
				&& resultCode == RESULT_OK && null != data) {
			// Get the URI for the video you picked
			entry.selectedVideo = data.getData();

			// Getting the data stream for the file
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			// Make a cursor using the URI for the image and its data stream
			Cursor cursor = getContentResolver().query(entry.selectedVideo,
					filePathColumn, null, null, null);

			// Move the cursor to the first row.
			cursor.moveToFirst();

			// Returns index for column name
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

			// getString Returns the value of the requested column as a String.
			String videoPath = cursor.getString(columnIndex);

			// You no longer need this cursor
			cursor.close();

			// Getting the view where the video belongs:
			videoView = ((AddVideoFragment) addVideoFragment).getVideoView();
			// Set the content of that view to be the video chosen:
			videoView.setVideoPath(entry.selectedVideo.toString());

			// Creating a preview image for the video
			Bitmap preview = ThumbnailUtils.createVideoThumbnail(Entry
					.convertURIToTruePath(getApplicationContext(),
							entry.selectedVideo.toString()),
					MediaStore.Images.Thumbnails.MINI_KIND);
			final ImageView videoPreview = ((AddVideoFragment) addVideoFragment)
					.getImagePreview();
			// Putting the image preview into an imageview
			videoPreview.setImageBitmap(preview);

			MediaController controller = new MediaController(
					ViewAndEditEntryActivity.this);
			controller.setMediaPlayer(videoView);
			videoView.setMediaController(controller);

			// Putting the controller into the frame layout
			((ViewGroup) controller.getParent()).removeView(controller);
			final FrameLayout frameLayoutForMediaController = ((AddVideoFragment) addVideoFragment)
					.getMediaControllerFrameLayout();
			frameLayoutForMediaController.addView(controller);

			// Setting the width and height of the video if it's too big
			setVideoWidthAndHeight(videoView, videoPath);

			// When preview image is clicked, hide the preview image
			// and display the videoView and its mediacontroller
			videoPreview.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					// Hiding the image preview:
					videoPreview.setVisibility(View.GONE);
					((AddVideoFragment) addVideoFragment)
							.getImagePreviewPlayButton().setVisibility(
									View.GONE);

					// Displaying the videoview:
					videoView.setVisibility(View.VISIBLE);
					// Start playing the video:
					videoView.start();

					// Displaying the framelayout containing the media
					// controller
					frameLayoutForMediaController.setVisibility(View.VISIBLE);
				}
			});

		}
	}

	/**
	 * Display action bar buttons
	 * 
	 * @param menu
	 *            is the menu item that is pressed
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		getMenuInflater().inflate(R.menu.view_entry, menu);
		// set MenuItems by id
		entry.edit = menu.findItem(R.id.edit_entry);
		entry.cancel = menu.findItem(R.id.cancel_edit_entry);
		entry.add = menu.findItem(R.id.entry_add_media);
		entry.save = menu.findItem(R.id.save_edit_entry);
		// don't show cancel or add in display mode
		entry.cancel.setVisible(false);
		entry.save.setVisible(false);
		// entry.add.setVisible(false);

		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Add media popup menu
	 * 
	 * @param v
	 *            is the view
	 */
	public void showPopup(View v) {
		PopupMenu popup = new PopupMenu(this, v);

		popup.setOnMenuItemClickListener(this);
		MenuInflater inflater = popup.getMenuInflater();
		inflater.inflate(R.menu.add_media_menu, popup.getMenu());
		popup.show();
	}

	/**
	 * When a user clicks something in the menu bar, this method is called.
	 * 
	 * @param item
	 *            is the menu item that was clicked.
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		final ViewSwitcher viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);

		switch (item.getItemId()) {
		case R.id.edit_entry:
			// When a user wants to edit an entry:
			viewSwitcher.showNext();
			item.setVisible(false);
			entry.save.setVisible(true);
			entry.cancel.setVisible(true);
			entry.add.setVisible(false);
			// maintain a copy of the current array arrangement
			previousMediaListData = copyList(mediaListData);

			// clear out the list of media to delete just in case
			// the user backed out of editing and canceled the changes
			mediaToDelete.clear();

			// set the media adapter to populate the list with images of a
			// reduced size
			mediaListAdapter.setImageReduction(IMAGE_REDUCTION);

			// pause the list from automatically updating with the database
			liveQuery.stop();

			return true;
		case R.id.cancel_edit_entry:
			// When a user cancels editing an entry:
			CancelEditsFragment cancelDialog = new CancelEditsFragment();
			cancelDialog.show(getSupportFragmentManager(), "cancel_dialog");

			// reset the image reduction back to full image size
			mediaListAdapter.setImageReduction(1);

			// restart the list view automatic update when the database changes
			liveQuery.start();

			return true;
		case R.id.save_edit_entry:
			// update the database with all the new values
			manager.update(CouchbaseManager.ENTRY_TITLE, entry.title_editmode
					.getText().toString(), entryUuid);
			manager.update(CouchbaseManager.ENTRY_DESCRIPTION,
					entry.description_editmode.getText().toString(), entryUuid);
			manager.update(CouchbaseManager.ENTRY_DATE, entry.date_editmode
					.getText().toString(), entryUuid);
			manager.update(CouchbaseManager.ENTRY_LOCATION,
					entry.location_editmode.getText().toString(), entryUuid);

			// transfer all the current values in edit mode to view mode
			entry.title.setText(entry.title_editmode.getText().toString());
			entry.description.setText(entry.description_editmode.getText()
					.toString());
			entry.date.setText(entry.date_editmode.getText().toString());
			entry.location
					.setText(entry.location_editmode.getText().toString());

			// delete all the items to be deleted
			manager.deleteMultipleMedia(mediaToDelete);
			// update the database to contain the new media order
			manager.updateOrder(mediaListData);

			// reset the image reduction back to full image size
			mediaListAdapter.setImageReduction(1);

			// restart the list view automatic update when the database changes
			liveQuery.start();

			// hide keyboard
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(entry.title_editmode.getWindowToken(),
					0);

			// toast
			Toast.makeText(getApplicationContext(), "Changes Saved",
					Toast.LENGTH_SHORT).show();
			// switch back to view mode
			viewSwitcher.showPrevious();
			item.setVisible(false);
			entry.edit.setVisible(true);
			entry.add.setVisible(true);
			entry.save.setVisible(false);
			entry.cancel.setVisible(false);
			return true;
		case R.id.help:
			// When a user requests to see the help screen:
			Timeline.showHelpScreen(R.string.view_edit_entry_help,
					getApplicationContext());
			return true;
		case R.id.entry_add_media:
			// When a user wants to add media to an entry:
			showPopup(findViewById(R.id.entry_add_media));
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * When a user clicks a menu item, this method is called.
	 * 
	 * @param item
	 *            is the item that was clicked.
	 */
	@Override
	public boolean onMenuItemClick(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.entry_add_text:

			addTextFragment.show(getSupportFragmentManager(), "addText");

			return true;
		case R.id.entry_add_photo:

			// Create a new intent when a picture is clicked, lets you
			// choose a photo
			Intent intent = new Intent(this, MultiPhotoSelectActivity.class);

			// Now start a new activity with this image. See
			// "onActivityForResult" below
			startActivityForResult(intent, Entry.get_result_load_image());

			// Calling the add picture fragment:
			addPictureFragment.show(getSupportFragmentManager(), "addPicture");

			return true;

		case R.id.entry_add_video:
			// entry.new_video.setVisibility(View.VISIBLE);
			Intent videoIntent = new Intent(
					Intent.ACTION_PICK,
					android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
			// Now start a new activity with this video. See
			// "onActivityForResult" below
			startActivityForResult(videoIntent, Entry.get_result_load_video());
			addVideoFragment.show(getSupportFragmentManager(), "addVideo");

			return true;
			// For adding audio to an entry:
		case R.id.entry_add_audio:
			addAudioFragment.show(getSupportFragmentManager(), "addAudio");
			return true;
		default:
			return false;
		}
	}

	/**
	 * This method shows the media within an entry on the screen.
	 */
	private void readAndDisplayMediaData() {
		// set up the query to query for all the media
		Query query = manager.getDatabase()
				.getView(CouchbaseManager.VIEW_MEDIA).createQuery();

		// only the mapper is needed to return all the results necessary
		query.setMapOnly(true);

		// set up the start and end keys to select all the media in this entry
		Object[] startKey = new Object[] { entryUuid,
				CouchbaseManager.MIN_ORDER_NUM };
		Object[] endKey = new Object[] { entryUuid,
				CouchbaseManager.MAX_ORDER_NUM };
		query.setStartKey(startKey);
		query.setEndKey(endKey);

		// make the query a live query so that it will update automatically
		liveQuery = query.toLiveQuery();

		liveQuery.addChangeListener(new LiveQuery.ChangeListener() {

			@Override
			public void changed(final LiveQuery.ChangeEvent event) {
				runOnUiThread(new Runnable() {
					public void run() {

						// clear out the existing list for repopulation
						mediaListData.clear();

						// get an iterator over the items to insert into the
						// list view
						Iterator<QueryRow> iter = event.getRows();

						// a temporary value for the current row while looping
						QueryRow currentRow = null;

						// add everything in this query result to the list view
						// adapter
						while (iter.hasNext()) {

							// get the next returned row from the database
							currentRow = iter.next();

							// create a map to be inserted into the adapter list
							HashMap<String, String> tempMap = new HashMap<String, String>();

							// create a map for this database document
							@SuppressWarnings("unchecked")
							Map<Object, String> docMap = (Map<Object, String>) currentRow
									.getValue();

							// add the required fields from the doc map to the
							// temp map
							tempMap.put(CouchbaseManager.TYPE,
									CouchbaseManager.TYPE_MEDIA);
							tempMap.put(CouchbaseManager.MEDIA_TYPE,
									docMap.get(CouchbaseManager.MEDIA_TYPE));
							tempMap.put(CouchbaseManager.MEDIA_UUID,
									currentRow.getDocumentId());
							// If it's a text item
							if (docMap.get(CouchbaseManager.MEDIA_TYPE).equals(
									CouchbaseManager.MEDIA_TYPE_TEXT)
									// Or if it's a picture with text
									|| docMap
											.get(CouchbaseManager.MEDIA_TYPE)
											.equals(CouchbaseManager.MEDIA_TYPE_PICTURE_WITH_TEXT)
									// Or if it's audio, which will have a
									// name/text
									|| docMap
											.get(CouchbaseManager.MEDIA_TYPE)
											.equals(CouchbaseManager.MEDIA_TYPE_AUDIO)
									// Or if it's a video:
									|| docMap
											.get(CouchbaseManager.MEDIA_TYPE)
											.equals(CouchbaseManager.MEDIA_TYPE_VIDEO)) {
								tempMap.put(
										CouchbaseManager.MEDIA_MESSAGE,
										docMap.get(CouchbaseManager.MEDIA_MESSAGE));
							}
							// If it's a picture:
							if (docMap.get(CouchbaseManager.MEDIA_TYPE).equals(
									CouchbaseManager.MEDIA_TYPE_PICTURE)
									// Or if this is a picture with text:
									|| docMap
											.get(CouchbaseManager.MEDIA_TYPE)
											.equals(CouchbaseManager.MEDIA_TYPE_PICTURE_WITH_TEXT)) {
								tempMap.put(
										CouchbaseManager.MEDIA_PICTURE_URI,
										docMap.get(CouchbaseManager.MEDIA_PICTURE_URI));
							}

							// If it's audio:
							if (docMap.get(CouchbaseManager.MEDIA_TYPE).equals(
									CouchbaseManager.MEDIA_TYPE_AUDIO)) {
								tempMap.put(
										CouchbaseManager.MEDIA_AUDIO_URI,
										docMap.get(CouchbaseManager.MEDIA_AUDIO_URI));
							}

							// If it's video:
							if (docMap.get(CouchbaseManager.MEDIA_TYPE).equals(
									CouchbaseManager.MEDIA_TYPE_VIDEO)) {
								tempMap.put(
										CouchbaseManager.MEDIA_VIDEO_URI,
										docMap.get(CouchbaseManager.MEDIA_VIDEO_URI));
							}

							// add this map to the list to be displayed
							mediaListData.add(tempMap);
						}

						Log.d("ViewAndEditEntry", "Updating Media List View");

						// update the list used to populate the list view
						mediaListAdapter.setList(mediaListData);

						// let the list view know that the data it is displaying
						// got changed,
						// so it can update the list view
						mediaListAdapter.notifyDataSetChanged();

						// check to see if media was added. If so, scroll down
						// to the
						// bottom where the media was just added.
						if (mediaAdded) {
							mediaListView.setSelection(mediaListAdapter
									.getCount() - 1);
							mediaAdded = false;
						}
					}
				});
			}
		});

		liveQuery.start();
	}

	/**
	 * This method updates the database with all of the newly-selected pictures.
	 * 
	 * @param potentialCaption
	 *            is a potential caption that indicates if one or more images
	 *            are being added.
	 */
	public void addNewPicturesToDatabase(String potentialCaption) {
		// If there was a caption, meaning there was a singular image
		// that was selected:
		if (potentialCaption.length() != 0) {
			// Add to the database the only item in the entry.selectedImages
			// list, and add the user's caption as well.
			manager.createMedia(CouchbaseManager.MEDIA_TYPE_PICTURE_WITH_TEXT,
					entryUuid, potentialCaption, entry.selectedImages.get(0)
							.toString());
		} else {
			// This means that more than one picture was selected.

			// Looping through the items in the listview
			// that contains the user's selected pictures:
			for (int index = 0; index < entry.selectedImages.size(); index++) {
				// This is the uri for an image in this list of selectedImages
				String imageURI = entry.selectedImages.get(index).toString();
				// If the URI in this list doesn't exist (shouldn't happen,
				// but just in case we will check for that)
				if (!imageURI.isEmpty()) {
					manager.createMedia(
							CouchbaseManager.MEDIA_TYPE_PICTURE_WITH_TEXT,
							entryUuid, "", imageURI);
				}
			}
		}
	}

	/**
	 * This method updates the database with the newly-selected video.
	 */
	public void addNewVideoToDatabase(String potentialCaption) {
		// Getting the video path
		String videoURI = entry.selectedVideo.toString();
		// If the URI in this list doesn't exist (shouldn't happen,
		// but just in case we will check for that)
		if (!videoURI.isEmpty()) {
			manager.createMedia(CouchbaseManager.MEDIA_TYPE_VIDEO, entryUuid,
					potentialCaption, videoURI);
		}
	}

	// An adapter that will prepare all of the images to be displayed in the
	// dialog fragment
	class ListViewImageAdapter extends ArrayAdapter<ArrayList<Uri>> {

		private ArrayList<Uri> listOfImageURIs;
		private Context context;

		public ListViewImageAdapter(Context context, int textViewResourceId,
				ArrayList<Uri> selectedImages) {
			super(context, textViewResourceId);
			this.listOfImageURIs = selectedImages;
			this.context = context;
		}

		@Override
		public int getCount() {
			return listOfImageURIs.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			// Getting a specific picture:
			Uri pictureURI = listOfImageURIs.get(position);

			// Getting the view for the list
			View view;
			@SuppressWarnings("static-access")
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.list_photos, parent, false);

			// If the picture can be found:
			if (pictureURI != null) {
				// Each picture:
				ImageView imageViewForSelectedImage = (ImageView) view
						.findViewById(R.id.add_new_picture_dialog);
				TextView uriForSelectedImage = (TextView) view
						.findViewById(R.id.selected_image_uri);

				// If imageview was successfully created:
				if (imageViewForSelectedImage != null) {
					// Setting the hidden textview to contain the image's
					// URI
					uriForSelectedImage.setText(pictureURI.toString());

					// Prepping a bitmap worker task to process the image
					BitmapWorkerTask task = new BitmapWorkerTask(
							imageViewForSelectedImage, getApplicationContext());
					// execute the worker task on the picture path
					task.execute(pictureURI.toString());
				}
			}

			return view;

		}
	}

	/**
	 * This method creates and starts an intent to display an image in an entry
	 * full screen. Called by the image view in the xml file
	 */
	public void showImageFullScreen(View v) {

		// get image view parent and find image URI
		View listItemView = (View) v.getParent().getParent();
		TextView listItemImage = (TextView) listItemView
				.findViewById(R.id.image_uri);
		String imageString = (String) listItemImage.getText();

		// create an intent for the full screen image activity
		Intent fullScreenIntent = new Intent(v.getContext(),
				FullScreenImage.class);
		// pass the image uri to the new activity
		fullScreenIntent.putExtra("IMAGE_URI", imageString);

		// start the new activity
		ViewAndEditEntryActivity.this.startActivity(fullScreenIntent);
	}

	/*
	 * http://developer.android.com/training/displaying-bitmaps/process-bitmap.html
	 */
	public void loadBitmap(String filePath, ImageView imageView) {
		BitmapWorkerTask task = new BitmapWorkerTask(imageView,
				getApplicationContext());
		task.execute(filePath);
	}

	/**
	 * used to set width and height of videos added in app
	 * 
	 * @param video
	 *            is the view
	 * @param filePath
	 *            is the video location
	 */
	public void setVideoWidthAndHeight(VideoView video, String filePath) {
		// To be used in getting the metadata for this video:
		MediaMetadataRetriever metadataRetriever = new MediaMetadataRetriever();
		Bitmap bitmap = null;

		// Retrieving the metadata, passing in the full path
		// (not the URI)
		metadataRetriever.setDataSource(filePath);
		bitmap = metadataRetriever.getFrameAtTime();
		// Getting the height and width of the selected video
		int videoHeight = bitmap.getHeight();
		int videoWidth = bitmap.getWidth();

		// Gets the layout parameters for the videoview
		LayoutParams parameters = video.getLayoutParams();

		// If this video's height is greater than 480
		if (videoHeight > 400) {
			// Set it a reasonable height
			parameters.height = 400;
		} else if (videoWidth > 400) {
			// Set width to a reasonable width
			parameters.width = 400;
		}
		// Now, actually setting the width and height:
		video.setLayoutParams(parameters);
	}

	/**
	 * This method will take an array list of hash maps and perform a deep copy
	 * of the list.
	 * 
	 * @param listToCopy
	 *            the list to copy
	 * @return a copy of the given list
	 */
	@SuppressWarnings("unchecked")
	private ArrayList<HashMap<String, String>> copyList(
			ArrayList<HashMap<String, String>> listToCopy) {
		// create the copied list
		ArrayList<HashMap<String, String>> copiedList = new ArrayList<HashMap<String, String>>();

		// copy all the hash maps within the array
		for (int i = 0; i < listToCopy.size(); i++) {
			copiedList.add((HashMap<String, String>) listToCopy.get(i).clone());
		}

		return copiedList;
	}

	/**
	 * This method is a setter for the mediaAdded variable
	 * 
	 * @param value
	 *            the value to set mediaAdded to
	 */
	public void setMediaAdded(boolean value) {
		mediaAdded = value;
	}

	/**
	 * When this activity is stopped, we will make a backup of the current state
	 * of the database.
	 */
	@Override
	public void onStop() {
		super.onStop();
		Context context = getApplicationContext();
		// Calling the "make backup" method to create a backup
		BackupManager.copyAndBackUpDatabase(context);
	}

	/**
	 * Just switch views back when you hit this button.
	 */
	@Override
	public void onBackPressed() {
		Log.d("CDA", "onBackPressed Called");
		/*
		 * If we can see the save button, we're clearly in edit mode. This means
		 * we're overwriting the onBackPressed method.
		 */
		if (entry.save.isVisible()) {
			// Do same "okay to discard" procedure:
			CancelEditsFragment cancelDialog = new CancelEditsFragment();
			cancelDialog.show(getSupportFragmentManager(), "cancel_dialog");

			// reset the image reduction back to full image size
			mediaListAdapter.setImageReduction(1);

			// restart the list view automatic update when the database changes
			liveQuery.start();
		} else {
			// Do the usual back button procedure
			super.onBackPressed();
		}
	}

}