package com.example.mqpapp.create_view_edit_entry;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;

import com.example.mqpapp.BackupManager;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.R;

/**
 * This class is used when a user wants to add an audio clip to an entry.
 *
 */
@SuppressLint("InflateParams")
public class AddAudioFragment extends DialogFragment {
	private static final String LOG_TAG = "AudioLog";
	// This is the button used to record audio
	Button recordButton;
	// Telling the button if it's a start or stop recording button
	private boolean startRecording = true;
	// This is the button used to play back audio
	Button playbackButton;
	// Telling the button if it's a start or stop playing button
	private boolean startPlaying = true;
	// Location of the audio clip on the device, a WanderLog-specific folder:
	String audioClipFileLocation = null;
	// This will contain the recorded audio
	private MediaRecorder audioRecorder = null;
	// This will allow for audio playback
	private MediaPlayer audioPlayer = null;
	// Caption for the audio
	private EditText audio_caption = null;
	// Boolean to check if audio was recorded:
	private boolean wasAudioRecorded = false;

	Entry entry = new Entry();
	// a unique identifier for the entry being displayed / edited
	String entryUuid;
	// a manager for the database for getting documents and updating data
	CouchbaseManager manager;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		audioClipFileLocation = getActivity().getApplicationContext()
				.getFilesDir().toString();
		AlertDialog dialog;
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// get the intent that was used to call this activity
		Intent callingIntent = getActivity().getIntent();
		// get the uuid of the entry to display from the intent
		entryUuid = callingIntent.getStringExtra(CouchbaseManager.ENTRY_UUID);
		// get a manager for the database
		manager = CouchbaseManager.getInstance(this.getActivity()
				.getApplicationContext());
		// get document from the database w/ all the information for this entry

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		View V = inflater.inflate(R.layout.add_audio_dialog, null);
		builder.setView(V);

		// EditText where the user types a caption for the audio
		audio_caption = (EditText) V.findViewById(R.id.entry_audio_caption);

		// This is the button used to record audio
		recordButton = (Button) V.findViewById(R.id.record_button);
		// Setting the record button's controls
		recordButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// Calling record method
				onRecord(startRecording);
				/*
				 * Changing the text depending on if you're currently recording
				 * audio or not.
				 */
				if (startRecording) {
					// Change the button's text to say "Stop recording"
					recordButton.setText("Stop Recording");
				} else {
					// Change the button's text to say "Start recording"
					recordButton.setText("Re-Record Audio");
					// Make the playback button visible
					playbackButton.setVisibility(View.VISIBLE);
					// Flagging that audio was recorded
					wasAudioRecorded = true;
				}
				// Flipping the button to its other value
				startRecording = !startRecording;
			}
		});

		// This is the button used to record audio
		playbackButton = (Button) V.findViewById(R.id.playback_button);
		// Setting the playback button's controls
		playbackButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// Calling playback method
				onPlay(startPlaying);
				// Resetting the value of startPlaying
				startPlaying = !startPlaying;
			}
		});

		builder.setMessage(R.string.add_audio)
				// When the user clicks "OK"
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// Checking to see if audio was ever recorded
								// Also checking to see if audio is currently
								// recording
								if (wasAudioRecorded || !startRecording) {
									// If something's currently recording, stop
									// it
									if (!startRecording) {
										audioRecorder.stop();
										audioRecorder.release();
										audioRecorder = null;
										// Flipping the button to its other
										// value
										startRecording = !startRecording;
									}
									ViewAndEditEntryActivity callingActivity = (ViewAndEditEntryActivity) getActivity();
									// have the calling activity list scroll to
									// the bottom to view the new media
									callingActivity.setMediaAdded(true);

									// Now, we need to save to the database.
									// Create a new media document in the
									// database
									// with this data
									manager.createMedia(
											CouchbaseManager.MEDIA_TYPE_AUDIO,
											entryUuid, audio_caption.getText()
													.toString(),
											audioClipFileLocation);
									// Resetting wasAudioRecorded value
									wasAudioRecorded = false;
								}

							}
						})
				// When the user clicks "cancel"
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// If something was recording while you clicked cancel:
								if (!startRecording) {
									// Stop the recording and don't keep it
									audioRecorder.stop();
									audioRecorder.release();
									audioRecorder = null;
									// Flipping the button to its other value
									startRecording = !startRecording;
								}
							}
						});

		// Create the AlertDialog object and return it
		dialog = builder.create();
		dialog.getWindow().setSoftInputMode(
				LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		return dialog;
	}

	/*
	 * Referenced from: Android Developer Guide
	 * http://developer.android.com/guide/topics/media/audio-capture.html
	 */
	/**
	 * This method controls recording of an audio clip in this dialogue box.
	 * 
	 * @param start
	 *            is a boolean that tells whether to start or stop recording
	 */
	private void onRecord(boolean start) {
		if (start) {
			audioClipFileLocation = getActivity().getApplicationContext()
					.getFilesDir().toString();
			// Creating the folder for WanderLog audio, if it doesn't already
			// exist:
			makeAudioLocation();
			// Creating a new MediaRecorder object
			audioRecorder = new MediaRecorder();
			// Choosing a source to record the audio:
			audioRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
			// Setting the type of the output format:
			audioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
			// Generating a new random name for this audio clip:
			audioClipFileLocation = audioClipFileLocation + "/"
					+ UUID.randomUUID() + ".3gp";
			// Where the audio clip is stored:
			audioRecorder.setOutputFile(audioClipFileLocation);
			// Setting how the audio is encoded:
			audioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

			try {
				audioRecorder.prepare();
			} catch (IOException e) {
				Log.e(LOG_TAG, "prepare() failed");
			}

			Log.e(LOG_TAG, audioClipFileLocation);

			audioRecorder.start();
		} else {
			audioRecorder.stop();
			audioRecorder.release();
			audioRecorder = null;
		}
	}

	/*
	 * Referenced from: Android Developer Guide
	 * http://developer.android.com/guide/topics/media/audio-capture.html
	 */
	/**
	 * This method controls playback for an audio clip.
	 * 
	 * @param start
	 *            is a boolean saying whether the audio is starting or stopping
	 */
	private void onPlay(boolean start) {
		if (start) {
			// Making a new MediaPlayer object
			audioPlayer = new MediaPlayer();
			// Listening for when the clip is done playing:
			audioPlayer.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					startPlaying = !startPlaying;
				}
			});
			try {
				// Locating the file and starting it
				audioPlayer.setDataSource(audioClipFileLocation);
				audioPlayer.prepare();
				audioPlayer.start();
			} catch (IOException e) {
				Log.e(LOG_TAG, "prepare() failed");
			}
		} else {
			// Stopping the audio clip from playing
			audioPlayer.release();
			audioPlayer = null;
		}
	}

	/**
	 * This method will create a folder specifically for Wander Log audio files.
	 */
	private void makeAudioLocation() {
		File folder = new File(audioClipFileLocation + "/WanderLogAudio");
		// If this doesn't exist yet, make it:
		if (!folder.exists()) {
			folder.mkdir();
		}
	}

	/**
	 * Referenced from: Android Developer Guide
	 * http://developer.android.com/guide/topics/media/audio-capture.html This
	 * method will release the audioRecorder or audioPlayer depending on which
	 * is being used.
	 */
	@Override
	public void onPause() {
		super.onPause();
		if (audioRecorder != null) {
			audioRecorder.release();
			audioRecorder = null;
		}

		if (audioPlayer != null) {
			audioPlayer.release();
			audioPlayer = null;
		}
	}

	/**
	 * When this fragment is stopped, we will make a backup of the current state
	 * of the database.
	 */
	@Override
	public void onStop() {
		super.onStop();
		Context context = getActivity().getApplicationContext();
		// Calling the "make backup" method to create a backup
		BackupManager.copyAndBackUpDatabase(context);
	}
}