package com.example.mqpapp.create_view_edit_entry;

import java.util.ArrayList;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

/**
 * This class is used to hold information related to entries, to avoid code
 * duplication.
 * 
 */
public class Entry {
	// Handles when an image is returned from the gallery:
	private static int RESULT_LOAD_IMAGE = 1;
	// Handles when a video is returned from the gallery
	private static int RESULT_LOAD_VIDEO = 2;

	// Path to your picture
	public Uri selectedImage;
	// Path to your picture
	Uri selectedVideo;

	// Controlling video
	MediaController mediaControls;

	// All the text views and edit views in an entry, for viewing:
	// view mode
	TextView title; // Title of the entry
	TextView description; // Written description of the entry
	TextView date; // Entry's date
	TextView location; // Entry's location
	TextView add_text; // Entry's option to add text
	TextView picture_caption; // Caption for a picture added to an entry

	// All the text views and edit views in an entry, for editing:
	EditText title_editmode; // Title of the entry
	EditText description_editmode; // Written description of the entry
	TextView date_editmode; // Entry's date
	EditText location_editmode; // Entry's location
	EditText add_text_editmode; // Entry's option to add text
	EditText new_picture_caption; // Caption for picture
	ImageView new_picture_editmode; // Entry's option to add picture
	VideoView new_video_editmode; // Entry's option to add video
	EditText new_text;
	VideoView new_video;
	ImageView new_picture;

	// Action bar menu items
	MenuItem edit;
	MenuItem cancel;
	MenuItem add;
	MenuItem save;

	public ArrayList<Uri> selectedImages = new ArrayList<Uri>();

	/**
	 * Getter for RESULT_LOAD_IMAGE
	 */
	public static int get_result_load_image() {
		return RESULT_LOAD_IMAGE;
	}

	/**
	 * Getter for RESULT_LOAD_VIDEO
	 */
	public static int get_result_load_video() {
		return RESULT_LOAD_VIDEO;
	}

	// Referenced from:
	// http://stackoverflow.com/questions/3401579/get-filename-and-path-from-uri-from-mediastore
	// (User Dextor, May 12th 2012)
	/**
	 * This method converts a URI to a true file path.
	 * @param context is the application's context
	 * @param uriString is the string version of a URI
	 * @return is the actual path of the image, not the URI
	 */
	public static String convertURIToTruePath(Context context, String uriString) {
		String actualPath = ""; // This will contain the actual path to the
								// media
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(context, Uri.parse(uriString),
				proj, null, null, null);
		Cursor cursor = loader.loadInBackground();
		if (cursor.getCount() > 0) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			actualPath = cursor.getString(column_index);
		}
		return actualPath;
	}

}
