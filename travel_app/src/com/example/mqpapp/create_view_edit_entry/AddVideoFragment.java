package com.example.mqpapp.create_view_edit_entry;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.mqpapp.BackupManager;
import com.example.mqpapp.CouchbaseManager;
import com.example.mqpapp.R;

/**
 * This class is used when the user wants to add a video clip to an entry.
 * 
 */
@SuppressLint("InflateParams")
public class AddVideoFragment extends DialogFragment {
	// image view that will display the selected video
	VideoView new_video_dialog;
	// invisibly text view to attach the selected image URI to the fragment
	TextView uri_text_view;
	FrameLayout frame_layout;
	EditText video_caption;
	ImageView video_preview_image;
	ImageView video_preview_image_play_button;
	// Location of the audio clip on the device, a WanderLog-specific folder:
	String videoClipFileLocation = null;
	Entry entry = new Entry();
	// a unique identifier for the entry being displayed / edited
	String entryUuid;
	// a manager for the database for getting documents and updating data
	CouchbaseManager manager;
	View V;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// get the intent that was used to call this activity
		Intent callingIntent = getActivity().getIntent();
		// get the uuid of the entry to display from the intent
		entryUuid = callingIntent.getStringExtra(CouchbaseManager.ENTRY_UUID);
		// get a manager for the database
		manager = CouchbaseManager.getInstance(this.getActivity()
				.getApplicationContext());
		// get document from the database w/ all the information for this entry

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		V = inflater.inflate(R.layout.add_video_dialog, null);

		builder.setView(V);

		// Videoview where the video is contained
		new_video_dialog = (VideoView) V
				.findViewById(R.id.add_new_video_dialog);
		// Moving the video to the top layer
		new_video_dialog.setZOrderOnTop(true);

		// Frame layout where the mediacontroller goes
		frame_layout = (FrameLayout) V.findViewById(R.id.frame_layout);
		// Hidden box containing URI
		uri_text_view = (TextView) V

		.findViewById(R.id.selected_video_uri);
		// This is the preview image for the video:
		video_preview_image = (ImageView) V
				.findViewById(R.id.list_item_video_preview);
		// This is the preview image play button for the video:
		video_preview_image_play_button = (ImageView) V
				.findViewById(R.id.imagef);
		// EditText where the user types a caption for the video
		video_caption = (EditText) V
				.findViewById(R.id.entry_selected_video_caption);

		builder.setMessage(R.string.add_video)
				.setPositiveButton("Add Video",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// Calling the method to update the database
								ViewAndEditEntryActivity callingActivity = (ViewAndEditEntryActivity) getActivity();
								// have the calling activity list scroll to the
								// bottom to view the new media
								callingActivity.setMediaAdded(true);
								// Add to the database the selected picture(s)
								// and potential caption
								callingActivity
										.addNewVideoToDatabase(video_caption
												.getText().toString());
							}
							// loadBitmap(picturePath, new_picture_dialog);
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// User cancelled the dialog
							}
						});
		// Create the AlertDialog object and return it
		return builder.create();

	}

	/**
	 * gets image view on add picture dialog to load picture through
	 * OnActivityForResult method in ViewAndEditEntry
	 * 
	 * @return the videoview
	 */
	public VideoView getVideoView() {
		return new_video_dialog;
	}

	/**
	 * gets (visibility=gone) text view on add picture dialog to save the
	 * picture URI and carry it over from OnActivityForResult to the fragment
	 * 
	 * @return the textview
	 */
	public TextView getTextView() {
		return uri_text_view;
	}

	/**
	 * Gets the frame layout that holds the media controller
	 * 
	 * @return FrameLayout holding media controller
	 */
	public FrameLayout getMediaControllerFrameLayout() {
		return frame_layout;
	}

	/**
	 * Gets the image preview for the video
	 * 
	 * @return the image preview for the video
	 */
	public ImageView getImagePreview() {
		return video_preview_image;
	}

	/**
	 * Gets the image preview's play button
	 * 
	 * @return the image preview's play button
	 */
	public ImageView getImagePreviewPlayButton() {
		return video_preview_image_play_button;
	}

	/**
	 * When this fragment is stopped, we will make a backup of the current state
	 * of the database.
	 */
	@Override
	public void onStop() {
		super.onStop();
		Context context = getActivity().getApplicationContext();
		// Calling the "make backup" method to create a backup
		BackupManager.copyAndBackUpDatabase(context);
	}

}
