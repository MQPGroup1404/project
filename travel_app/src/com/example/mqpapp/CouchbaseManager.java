package com.example.mqpapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Document.DocumentUpdater;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.Manager;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.Reducer;
import com.couchbase.lite.UnsavedRevision;
import com.couchbase.lite.android.AndroidContext;

/**
 * This class is used when creating and updating the couchbase database.
 *
 */
public class CouchbaseManager {
	private static final String TAG = CouchbaseManager.class.getSimpleName();
	private static CouchbaseManager instance;
	private static String DATABASE_NAME = "media_db";
	private Database database;
	private Manager manager;
	public static final String MIN_ORDER_NUM = "0000";
	public static final String MAX_ORDER_NUM = "9999";
	// the version for the views
	private static String INDEX_VERSION = "1.0.9";
	// the character that separates numbers in a stored data
	private static String DATE_DELIMINATOR = "-";

	// the names of the views
	public static String VIEW_PICTURE = "allPicturesView";
	public static String VIEW_PICTURE_WITH_TEXT = "allPicturesWithTextView";
	public static String VIEW_TIMELINE = "timelineView";
	public static String VIEW_ENTRY = "entryView";
	public static String VIEW_MEDIA = "contentView";

	/** the name of the keys for the various document types */

	// all keys available for all document types
	// the type partitions the database into several large sections
	public static String TYPE = "type";
	public static String TYPE_TIMELINE = "timelineType";
	public static String TYPE_ENTRY = "entryType";
	public static String TYPE_MEDIA = "mediaType";

	// all the keys available for a timeline type document
	public static String TIMELINE_TITLE = "timelineTitle";
	public static String TIMELINE_DESCRIPTION = "timelineDescription";
	public static String TIMELINE_LOCATION = "timelineLocation";
	public static String TIMELINE_START_DATE = "timelineStartDate";
	public static String TIMELINE_END_DATE = "timelineEndDate";
	public static String TIMELINE_ORDER = "timelineOrder";
	public static String TIMELINE_UUID = "timelineUuid";

	// all the keys available for an entry type document
	public static String ENTRY_TITLE = "entryTitle";
	public static String ENTRY_DESCRIPTION = "entryDescription";
	public static String ENTRY_LOCATION = "entryLocation";
	public static String ENTRY_DATE = "entryDate";
	public static String ENTRY_TIMELINE = "entryTimeline";
	public static String ENTRY_ORDER = "entryOrder";
	public static String ENTRY_UUID = "entryUuid";

	// a document of type media has several sub types that are
	// distinguishable by the media type value
	public static String MEDIA_TYPE = "mediaType";
	public static String MEDIA_TYPE_TEXT = "textMediaType";
	public static String MEDIA_TYPE_PICTURE = "pictureMediaType";
	public static String MEDIA_TYPE_PICTURE_WITH_TEXT = "pictureWithTextMediaType";
	public static String MEDIA_TYPE_AUDIO = "audioMediaType";
	public static String MEDIA_TYPE_VIDEO = "videoMediaType";

	// all media has these keys
	public static String MEDIA_ORDER = "mediaOrder";
	public static String MEDIA_DATE = "mediaDate";
	public static String MEDIA_ENTRY = "mediaEntry";

	// all the keys available for a text media type document
	public static String MEDIA_MESSAGE = "textMediaMessage";

	// all the keys available for a picture media type document
	public static String MEDIA_PICTURE_URI = "pictureMediaUri";
	public static String MEDIA_PICTURE_BYTES = "pictureMediaBytes";

	// all the keys available for an audio media type document
	public static String MEDIA_AUDIO_URI = "audioMediaUri";

	// all the keys available for an video media type document
	public static String MEDIA_VIDEO_URI = "videoMediaUri";

	// a standard name for the document uuid of a media document for
	public static String MEDIA_UUID = "mediaUuid";

	// the picture with text media type has access to all the keys of both a
	// picture
	// and a text type

	private CouchbaseManager() {
	}

	/**
	 * This method is a standard singleton implementation so that any activity
	 * can access the CouchbaseManager. In addition to creating an instance of
	 * this class, this method will initialize the database when the first
	 * instance is created
	 * 
	 * @param context
	 *            a context for initialization
	 * @return the singleton instance of this class
	 */
	public static synchronized CouchbaseManager getInstance(Context context) {
		if (instance == null) {
			instance = new CouchbaseManager();
			instance.chooseDatabase(getDatabaseName(), context);
			instance.initDbViews();
		}
		return instance;
	}

	/**
	 * Create a Couchbase database
	 * 
	 * @param dbName
	 *            the name of the database
	 * @param context
	 *            the current context
	 */
	public void chooseDatabase(String dbName, Context context) {
		try {
			manager = new Manager(new AndroidContext(context),
					Manager.DEFAULT_OPTIONS);
		} catch (IOException e) {
			Log.e("database", "could not create database manager");
		}

		// check the database name
		if (!Manager.isValidDatabaseName(dbName)) {
			Log.e("database", "invalid database name");
			return;
		}

		// get an existing database or create a new one if one
		// does not exist already
		try {
			database = manager.getDatabase(dbName);
		} catch (CouchbaseLiteException e) {
			Log.e("database", "error getting database");
		}
	}

	/**
	 * Close this database manager
	 */
	public void close() {
		if (manager != null) {
			manager.close();
		}
	}

	/*
	 * CRUD operations Create Retrieve Update Delete
	 */

	/**
	 * C-rud
	 * 
	 * @param docContent
	 *            content to add to the new document
	 * @return the id of the new document
	 */
	public String create(Map<String, Object> docContent) {
		// create an empty document
		Document doc = database.createDocument();

		// add content to document and write document to database
		try {
			doc.putProperties(docContent);
		} catch (CouchbaseLiteException e) {
			Log.e("database", "could not add content to document");
		}

		return doc.getId();
	}

	/**
	 * c-R-ud
	 * 
	 * @param docId
	 *            the document to retrieve from the database
	 * @return the properties of the given document
	 */
	public Map<String, Object> retrieve(String docId) {
		// retrieve the document from the database
		Document doc = database.getDocument(docId);

		// display the retrieved document
		return doc.getProperties();
	}

	/**
	 * cr-U-d
	 * 
	 * @param key
	 *            the key to look up in the document
	 * @param value
	 *            the value to set that key to
	 * @param docId
	 *            the document to update
	 * @return true if the update was successful, false otherwise
	 */
	public boolean update(final String key, final Object value, String docId) {
		try {
			Document doc = database.getDocument(docId);

			doc.update(new DocumentUpdater() {
				@Override
				public boolean update(UnsavedRevision newRevision) {
					Map<String, Object> properties = newRevision
							.getUserProperties();
					properties.put(key, value);
					newRevision.setUserProperties(properties);
					return true;
				}
			});
		} catch (CouchbaseLiteException e) {
			Log.e("database", "error updating document in database");
		}
		return true;
	}

	/**
	 * cru-D
	 * 
	 * @param docId
	 *            the id of the document to delete
	 * @return true if the document is deleted, false if it was not
	 */
	public boolean delete(String docId) {

		Document doc = null;

		try {
			// retrieve the document
			doc = database.getDocument(docId);

			// delete the document
			doc.delete();
			Log.i(TAG, "Deleted document " + docId);
		} catch (CouchbaseLiteException e) {
			Log.e("database", "could not delete document");
		}

		return doc.isDeleted();
	}

	/**
	 * This method initializes all the views for the database
	 */
	public void initDbViews() {
		// View for pictures with captions:
		com.couchbase.lite.View pictureWithTextEntryView = database
				.getView(VIEW_PICTURE_WITH_TEXT);
		pictureWithTextEntryView.setMap(new Mapper() {
			@Override
			public void map(Map<String, Object> document, Emitter emitter) {
				// only grab the documents of type picture
				if (document.get(TYPE).equals(TYPE_MEDIA)
						&& document.get(MEDIA_TYPE).equals(MEDIA_TYPE_PICTURE_WITH_TEXT)) {
					emitter.emit(document.get(MEDIA_PICTURE_URI), document);
				}
			}
		}, INDEX_VERSION);

		// Create a view for timelines
		com.couchbase.lite.View timelineView = database.getView(VIEW_TIMELINE);
		timelineView.setMapReduce(new Mapper() {
			@Override
			public void map(Map<String, Object> document, Emitter emitter) {
				// only get the documents that are of type timeline
				if (document.get(TYPE).equals(TYPE_TIMELINE)) {
					// emit this timeline document with the order as the key
					// so that the documents can be reordered
					emitter.emit(document.get(TIMELINE_ORDER), document);
				}
			}
		}, new Reducer() {
			@Override
			public Object reduce(List<Object> keys, List<Object> values,
					boolean rereduce) {
				return Integer.valueOf(values.size());
			}
		}, INDEX_VERSION);

		// Create a view for entries
		com.couchbase.lite.View entryView = database.getView(VIEW_ENTRY);
		entryView.setMapReduce(new Mapper() {
			@Override
			public void map(Map<String, Object> document, Emitter emitter) {
				// only get the documents that are of type timeline
				if (document.get(TYPE).equals(TYPE_ENTRY)) {
					// emit this entry's timeline with the order as the key so
					// that
					// the entries can be grouped by timeline and ordered as
					// well
					
					// take the current date formatting "mm-dd-yyyy" and convert
					// it to something that can be ordered properly "yyyy-mm-dd"
					// or "0000-00-00" if no date was given.
					String storedDate = (String) document.get(ENTRY_DATE);
					String newDate = "";
					String delims = "[" + DATE_DELIMINATOR + "]";
					String[] tokens = storedDate.split(delims);
					if (tokens.length == 3) {
						String month = tokens[0];
						String day = tokens[1];
						String year = tokens[2];
						// make sure that the month and day are always 2 numbers
						// to ensure proper ordering
						if (month.length() < 2) {
							month = "0" + month;
						}
						if (day.length() < 2) {
							day = "0" + day;
						}
						newDate = year + DATE_DELIMINATOR
								+ month + DATE_DELIMINATOR + day;
					}
					else {
						newDate = "0000-00-00";
					}
					
					Log.d(TAG, "The date = " + (String) document.get(ENTRY_DATE));
					Log.d(TAG, "The new date = " + newDate);
					
					Object[] key = new Object[] { document.get(ENTRY_TIMELINE),
							newDate};

					// emit the database document as the value
					emitter.emit(key, document);
				}
			}
		}, new Reducer() {
			@Override
			public Object reduce(List<Object> keys, List<Object> values,
					boolean rereduce) {
				return Integer.valueOf(values.size());
			}
		}, INDEX_VERSION);

		// Create a view for the entry contents (media)
		com.couchbase.lite.View contentView = database.getView(VIEW_MEDIA);
		contentView.setMapReduce(new Mapper() {
			@Override
			public void map(Map<String, Object> document, Emitter emitter) {
				// only get the documents that are of type timeline
				if (document.get(TYPE).equals(TYPE_MEDIA)) {
					// emit this media's entry with the order as the key so that
					// the medias can be grouped by entry and ordered as well
					Object[] key = new Object[] { document.get(MEDIA_ENTRY),
							document.get(MEDIA_ORDER) };

					// emit the database document as the value
					emitter.emit(key, document);
				}
			}
		}, new Reducer() {
			@Override
			public Object reduce(List<Object> keys, List<Object> values,
					boolean rereduce) {
				return Integer.valueOf(values.size());
			}
		}, INDEX_VERSION);
	}

	/**
	 * This method gets the database.
	 * @return The current database this manager is managing
	 */
	public Database getDatabase() {
		return database;
	}

	// ################################################################
	// ########### methods for working with the database ##############
	// ################################################################

	/**
	 * This method will query the database and count up all the results returned
	 * 
	 * @param view
	 *            the view to query
	 * @param key
	 *            an optional key to specify what the first part of the array
	 *            key should be. null will result in all the query results being
	 *            counted
	 * @return a string that will always be of length maxNum and represents how
	 *         many rows were returned by the query
	 */
	private String countDatabaseItems(String view, String key) {
		// create a query over the specified view
		Query query = getDatabase().getView(view).createQuery();
		query.setMapOnly(false);

		if (key != null) {
			// add a key range to limit the results to
			// all the items with the given key
			Object[] startKey = new Object[] { key, MIN_ORDER_NUM };
			Object[] endKey = new Object[] { key, MAX_ORDER_NUM };
			query.setStartKey(startKey);
			query.setEndKey(endKey);
		}

		QueryEnumerator queryResult;
		String result = "";
		try {
			// execute the query
			queryResult = query.run();
			// check to see if the query returned anything
			if (queryResult.hasNext()) {
				// the result is the count, so return that
				result = Integer.toString((Integer) queryResult.next()
						.getValue());
			}
		} catch (CouchbaseLiteException e) {
			Log.e(TAG, "could not run timeline query");
		}

		// add padding to the beginning of the number string
		result = MIN_ORDER_NUM.substring(0,
				MIN_ORDER_NUM.length() - result.length())
				+ result;

		return result;
	}

	/**
	 * This method will add a new timeline to the database
	 * 
	 * @param title
	 *            timeline title
	 * @param description
	 *            timeline description
	 * @param location
	 *            timeline location
	 * @param startDate
	 *            timeline start date
	 * @param endDate
	 *            timeline end date
	 */
	public void createTimeline(String title, String description,
			String location, String startDate, String endDate) {
		// create a map to insert into the database
		Map<String, Object> properties = new HashMap<String, Object>();

		// add values to this map
		properties.put(TYPE, TYPE_TIMELINE);
		properties.put(TIMELINE_TITLE, title);
		properties.put(TIMELINE_DESCRIPTION, description);
		properties.put(TIMELINE_LOCATION, location);
		properties.put(TIMELINE_START_DATE, startDate);
		properties.put(TIMELINE_END_DATE, endDate);

		// get the number of timelines already in the database and
		// give this timeline the next number
		properties.put(TIMELINE_ORDER, countDatabaseItems(VIEW_TIMELINE, null));

		Log.d(TAG,
				"adding timeline with order number = "
						+ properties.get(TIMELINE_ORDER));
		// add this information to the database
		create(properties);
	}

	/**
	 * This method will add a new entry to the database
	 * 
	 * @param title
	 *            entry title
	 * @param description
	 *            entry description
	 * @param location
	 *            entry location
	 * @param date
	 *            entry data
	 * @param timelineUuid
	 *            the timeline the entry belongs in
	 */
	public void createEntry(String title, String description, String location,
			String date, String timelineUuid) {

		// create a map to insert into the database
		Map<String, Object> properties = new HashMap<String, Object>();

		// add values to this map
		properties.put(TYPE, TYPE_ENTRY);
		properties.put(ENTRY_TITLE, title);
		properties.put(ENTRY_DESCRIPTION, description);
		properties.put(ENTRY_LOCATION, location);
		properties.put(ENTRY_DATE, date);
		properties.put(ENTRY_TIMELINE, timelineUuid);

		// get the number of entries already in the database and
		// give this entry the next number
		properties.put(ENTRY_ORDER,
				countDatabaseItems(VIEW_ENTRY, timelineUuid));

		Log.d(TAG,
				"adding entry with entry order number = "
						+ properties.get(ENTRY_ORDER));
		Log.d(TAG,
				"adding entry with timeline = "
						+ properties.get(ENTRY_TIMELINE));

		// add this information to the database
		create(properties);
	}

	/**
	 * This method will add a new piece of media to the database
	 * 
	 * @param mediaType
	 *            the type of media to add. Media types listed at the top of
	 *            this class in the public static final variables
	 * @param entryUuid
	 *            the entry that this piece of media belongs in
	 * @param message
	 *            a message to store into media types that require a message.
	 *            Use null if the media type does not require a message
	 * @param mediaUri
	 *            a uri to store into media types that require a uri. Use null
	 *            if the media type does not require a uri
	 */
	public void createMedia(String mediaType, String entryUuid, String message,
			String mediaUri) {
		// create a map to insert into the database
		Map<String, Object> properties = new HashMap<String, Object>();

		// add values to this map
		properties.put(TYPE, TYPE_MEDIA);
		properties.put(MEDIA_TYPE, mediaType);
		properties.put(MEDIA_ENTRY, entryUuid);

		// add media type specific values
		if (mediaType.equals(MEDIA_TYPE_TEXT)) {
			properties.put(MEDIA_MESSAGE, message);
		} else if (mediaType.equals(MEDIA_TYPE_PICTURE)) {
			properties.put(MEDIA_PICTURE_URI, mediaUri);
		} else if (mediaType.equals(MEDIA_TYPE_PICTURE_WITH_TEXT)) {
			properties.put(MEDIA_MESSAGE, message);
			properties.put(MEDIA_PICTURE_URI, mediaUri);
		} else if (mediaType.equals(MEDIA_TYPE_AUDIO)) {
			properties.put(MEDIA_MESSAGE, message);
			properties.put(MEDIA_AUDIO_URI, mediaUri);
		} else if (mediaType.equals(MEDIA_TYPE_VIDEO)) {
			properties.put(MEDIA_MESSAGE, message);
			properties.put(MEDIA_VIDEO_URI, mediaUri);
		}

		// get the number of media already in the database for
		// this entry and give this media the next number
		properties.put(MEDIA_ORDER, countDatabaseItems(VIEW_MEDIA, entryUuid));

		Log.d(TAG,
				"adding media with media order number = "
						+ properties.get(MEDIA_ORDER));

		// add this information to the database
		create(properties);
	}

	/**
	 * This method takes an array of documents and updates the order in the
	 * database to match the order of the array
	 * 
	 * @param itemList
	 *            The array to set the order in the database to.
	 */
	public void updateOrder(ArrayList<HashMap<String, String>> itemList) {
		HashMap<String, String> currentItem;
		String currentNum;

		// loop throught the array and set the order in the database for each
		// document
		// encountered in the array
		for (int i = 0; i < itemList.size(); i++) {
			// get the current document and order
			currentItem = itemList.get(i);
			currentNum = String.valueOf(i);
			currentNum = MIN_ORDER_NUM.substring(0, MIN_ORDER_NUM.length()
					- currentNum.length())
					+ currentNum;

			// update the database
			update(MEDIA_ORDER, currentNum, currentItem.get(MEDIA_UUID));
		}
	}

	/**
	 * This method will delete multiple items from the database all at once
	 * 
	 * @param docIds
	 *            a list of documents to delete from the database
	 */
	public void deleteMultipleMedia(List<String> docIds) {
		database.beginTransaction();
		for (String item : docIds) {
			delete(item);
		}
		database.endTransaction(true);
	}
	
	/**
	 * This method will delete multiple items from the database all at once
	 * 
	 * @param docIds
	 *            a list of documents to delete from the database
	 */
	public void deleteMultipleEntries(List<String> docIds) {
		for (String item : docIds) {
			deleteEntry(item);
		}
	}
	
	/**
	 * This method will delete an entry and all media documents associated with
	 * that entry
	 * 
	 * @param entryId
	 *            The unique identifier for the entry to delete
	 */
	public void deleteEntry(String entryUuid) {
		// create a query to get all of the media associated with
		// this entry
		Query query = database.getView(VIEW_MEDIA).createQuery();
		
		// the mapper is all that is needed to return the results needed
		query.setMapOnly(true);
		
		// set up the start and end keys to select all the media in this entry
		Object[] startKey = new Object[] { entryUuid, MIN_ORDER_NUM };
		Object[] endKey = new Object[] { entryUuid, MAX_ORDER_NUM };
		query.setStartKey(startKey);
		query.setEndKey(endKey);
		
		QueryEnumerator result = null;
		// get the results of running the query
		try {
			result = query.run();
		} catch (CouchbaseLiteException e) {
			Log.e(TAG, "error querying for media within entry to delete");
		}
		
		database.beginTransaction();
		
		// loop through all the the documents returned by the query
		// and delete all the media associated with this entry
		for (Iterator<QueryRow> iter = result; iter.hasNext();) {
			// get the current row in the query results
			QueryRow row = iter.next();
			
			// get the id of the current media document
			String mediaUuid = row.getDocumentId();
			
			// delete this piece of media associated with the given entry
			delete(mediaUuid);
		}
				
		// now that all of the media associated with this entry has been deleted,
		// the entry itself can now be deleted
		delete(entryUuid);
		
		database.endTransaction(true);
	}
	
	/**
	 * This method will delete a timeline, all the entries within that timeline,
	 * and all media documents within those entries that entry
	 * 
	 * @param timelineId
	 *            The unique identifier for the entry to delete
	 */
	public void deleteTimeline(String timelineUuid) {
		// create a query to get all of the media associated with
		// this entry
		Query query = database.getView(VIEW_ENTRY).createQuery();
		
		// the mapper is all that is needed to return the results needed
		query.setMapOnly(true);
		
		// set up the start and end keys to select all the media in this entry
		Object[] startKey = new Object[] { timelineUuid, MIN_ORDER_NUM };
		Object[] endKey = new Object[] { timelineUuid, MAX_ORDER_NUM };
		query.setStartKey(startKey);
		query.setEndKey(endKey);
		
		QueryEnumerator result = null;
		// get the results of running the query
		try {
			result = query.run();
		} catch (CouchbaseLiteException e) {
			Log.e(TAG, "error querying for entries within timeline to delete");
		}
				
		// loop through all the the documents returned by the query
		// and delete all the media associated with this entry
		for (Iterator<QueryRow> iter = result; iter.hasNext();) {
			// get the current row in the query results
			QueryRow row = iter.next();
			
			// get the id of the current media document
			String entryUuid = row.getDocumentId();
			
			// delete this piece of media associated with the given entry
			deleteEntry(entryUuid);
		}
				
		// now that all of the media associated with this entry has been deleted,
		// the entry itself can now be deleted
		delete(timelineUuid);
	}

	/**
	 * This method will return the name of the media database
	 * 
	 * @return the name of the media database
	 */
	public static String getDatabaseName() {
		return DATABASE_NAME;
	}

}
